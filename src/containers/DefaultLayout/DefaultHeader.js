import React, { Component } from "react";
// import { Link, NavLink } from 'react-router-dom';
import { withRouter } from "react-router-dom";
import {
  // Badge,
  UncontrolledDropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Nav
  // NavItem
} from "reactstrap";
// import { Button } from "react-bootstrap";
import PropTypes from "prop-types";

import {
  // AppAsideToggler,
  AppNavbarBrand,
  AppSidebarToggler
} from "@coreui/react";
import logo from "../../assets/img/brand/IT.png";
// import logo from "../../assets/img/brand/logo.svg";
// import sygnet from "../../assets/img/brand/sygnet.svg";

const propTypes = {
  children: PropTypes.node
};

const defaultProps = {};

class DefaultHeader extends Component {
  changePage = path => {
    return this.props.history.push(path);
  };
  render() {
    // eslint-disable-next-line
    const { children, ...attributes } = this.props;

    return (
      <React.Fragment>
        <AppSidebarToggler className="d-lg-none" display="md" mobile />
        <AppNavbarBrand
          full={{ src: logo, width: 89, height: 25, alt: "CoreUI Logo" }}
          // minimized={{ src: sygnet, width: 30, height: 30, alt: "CoreUI Logo" }}
        />
        <AppSidebarToggler className="d-md-down-none" display="lg" />

        {/* <Nav className="d-md-down-none" navbar> */}
        {/* <NavItem className="px-3">
            <NavLink to="/dashboard" className="nav-link" >Dashboard</NavLink>
          </NavItem>
          <NavItem className="px-3">
            <Link to="/users" className="nav-link">Users</Link>
          </NavItem>
          <NavItem className="px-3">
            <NavLink to="#" className="nav-link">Settings</NavLink>
          </NavItem> */}
        {/* </Nav> */}
        <Nav className="ml-auto" navbar>
          <label variant="dark">{sessionStorage.getItem("user_id")}</label>
          <UncontrolledDropdown nav direction="down">
            <DropdownToggle nav>
              <img
                src={`${process.env.PUBLIC_URL}/img/avatars/7.jpg`}
                className="img-avatar"
                alt=""
              />
            </DropdownToggle>

            <DropdownMenu right>
              <DropdownItem header tag="div" className="text-center">
                <strong>ตั้งค่า</strong>
              </DropdownItem>

              <DropdownItem
                // hidden={this.state.userLevel}
                onClick={() => this.changePage("/users")}
              >
                <i className="fa fa-user"></i> ข้อมูลผู้ใช้งาน
              </DropdownItem>
              <DropdownItem
                // hidden={this.state.userLevel}
                onClick={() => this.changePage("/files")}
              >
                <i className="fa fa-file"></i> ข้อมูลระบบงาน
              </DropdownItem>

              {/* <DropdownItem onClick={() => this.setModel(true)}> */}
              {/* <i className="fa fa-key"></i> เปลี่ยนรหัสผ่าน */}
              {/* </DropdownItem> */}
              {/* <DropdownItem><i className="fa fa-wrench"></i> Settings</DropdownItem> */}
              <DropdownItem onClick={e => this.props.onLogout(e)}>
                <i className="fa fa-lock"></i> ออกจากระบบ
              </DropdownItem>
            </DropdownMenu>
          </UncontrolledDropdown>
        </Nav>
        {/* <label variant="dark" onClick={e => this.props.onLogout(e)}>
            {sessionStorage.getItem("user_id")}
            <i className="fa fa-lock"></i>
          </label> */}
        {/* <AppAsideToggler className="d-md-down-none" /> */}
        {/*<AppAsideToggler className="d-lg-none" mobile />*/}
      </React.Fragment>
    );
  }
}

DefaultHeader.propTypes = propTypes;
DefaultHeader.defaultProps = defaultProps;

export default withRouter(DefaultHeader);
