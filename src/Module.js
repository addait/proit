
const dateFormat = date => {
    if (date !== null) {
      let dd = date.substring(8, 10);
      let mm = date.substring(5, 7);
      let yyyy = date.substring(0, 4);
      let hhmm = date.substring(11, 16);

      return dd + "/" + mm + "/" + yyyy + " " + hhmm;
    }
  };

// const checkPermiss = async (userID,fileID) => {
//   await Axios.get(RequestUrl+"checkpermiss/users/"+userID+"/fileid"+fileID,{
//     headers: { authorization: Cookies.get("CRPtoken") }
//   })
//   .then(res => {
//     if(res.data.success){
//       return res.data.data
//     }else{
//       this.props.history.push("/login");
//     }
//   })
// }

module.exports ={
  dateFormat:dateFormat
}