import React from "react";
import { Col, Row, Button } from "reactstrap";
import { Tabs, Tab, Collapse } from "react-bootstrap";
import Swal from "sweetalert2";
import { REQUEST_URL } from "../../config";
import Cookies from "js-cookie";
import DatePicker from "react-date-picker";
import moment from "moment";
import pic1 from "../../img/placeholder.png";
import Axios from "axios";
import ModalAsset from "./Modal/ModalAsset";
import ModalType from "./Modal/ModalType";
import ModalBrand from "./Modal/ModalBrand";
import ModalSeller from "./Modal/ModalSeller";
import ModalDept from "../Computer/Modal/ModalDept";
import ModalLocation from "./Modal/ModalLocation";
import ModalUser from "../Computer/Modal/ModalUser";

class EditMainSoftware extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      idtype: "",
      volume: "1",
      checkOS: false,
      cdkey: "",
      partnumber: "",
      serial: "",
      bath: "บาท",
      yyear: "ปี"
    };
  }

  componentDidUpdate = (prevProps, prevState) => {
    // if (prevState.idtype !== this.state.idtype) {
    //   Axios.post(
    //     REQUEST_URL + "/software/genidasset",
    //     {
    //       id: this.state.idtype
    //     },
    //     { headers: { authorization: Cookies.get("webcomputer") } }
    //   ).then(response => {
    //     if (response.data.status) {
    //       this.setState({
    //         barcode: response.data.data
    //       });
    //     } else {
    //     }
    //   });
    // }
  };

  componentDidMount = () => {
    this.DataSoftwareEdit();
  };

  DataSoftwareEdit = async () => {
    Axios.post(
      REQUEST_URL + "/software/datasoftwareedit",
      {
        id: this.props.match.params.id
      },
      {
        headers: { authorization: Cookies.get("webcomputer") }
      }
    ).then(response => {
      if (response.data.status) {
        this.setState({
          barcode: response.data.data[0].barcode,
          ascode: response.data.data[0].assetID,
          idtype: response.data.data[0].typeID,
          nametype: response.data.data[0].typename,
          idbrand: response.data.data[0].brandID,
          namebrand: response.data.data[0].brandName,
          partnumber: response.data.data[0].partnumber,
          serial: response.data.data[0].serial,
          volume: response.data.data[0].volumelicense,
          a_desc: response.data.data[0].name,
          startwarranty: response.data.data[0].startdate,
          warranty: response.data.data[0].warranty,
          endwarranty: response.data.data[0].startend,
          price: response.data.data[0].price,
          unit: response.data.data[0].unit,
          status: response.data.data[0].status,
          cdkey: response.data.data[0].cdkey,
          checkOS: response.data.data[0].isOS,
          idseller: response.data.data[0].supplierID,
          nameseller: response.data.data[0].supplierName,
          deptcode: response.data.data[0].depcode,
          deptname: response.data.data[0].desc,
          idlocation: response.data.data[0].locacode,
          namelocation: response.data.data[0].locadesc,
          pfs_date1: moment(response.data.data[0].pfs_date1).format(
            "YYYY-MM-DD"
          ),
          pfs_id1: response.data.data[0].pfs_id1,
          name1: response.data.data[0].name1,
          pfs_date2: moment(response.data.data[0].pfs_date2).format(
            "YYYY-MM-DD"
          ),
          pfs_id2: response.data.data[0].pfs_id2,
          name2: response.data.data[0].name2,
          refno: response.data.data[0].invno,
          hold: response.data.data[0].hold,
          preby: response.data.data[0].preby,
          predate: moment(response.data.data[0].predate).format("YYYY-MM-DD")
        });
      }
    });
  };

  getAsset = (
    ASCODE,
    a_desc,
    STARTDATE,
    YEAR,
    ENDDATE,
    UNITCOST,
    UNIT,
    sta_adt,
    pfs_id1,
    name1,
    pfs_date1,
    pfs_id2,
    name2,
    pfs_date2,
    refno,
    hold
  ) => {
    this.setState({
      ascode: ASCODE,
      a_desc: a_desc,
      startwarranty: moment(STARTDATE).format("YYYY-MM-DD"),
      warranty: YEAR,
      endwarranty: moment(ENDDATE).format("YYYY-MM-DD"),
      price: UNITCOST,
      unit: UNIT,
      status: sta_adt,
      pfs_id1: pfs_id1,
      name1: name1,
      pfs_date1: moment(pfs_date1).format("YYYY-MM-DD"),
      pfs_id2: pfs_id2,
      name2: name2,
      pfs_date2: moment(pfs_date2).format("YYYY-MM-DD"),
      refno: refno,
      hold: hold
    });
  };

  modalAsset = bool => {
    this.setState({
      modalasset: bool
    });
  };

  getType = (id, name) => {
    this.setState({
      idtype: id,
      nametype: name
    });
  };

  modalType = bool => {
    this.setState({
      modaltype: bool
    });
  };

  getBrand = (id, name) => {
    this.setState({
      idbrand: id,
      namebrand: name
    });
  };

  modalBrand = bool => {
    this.setState({
      modalbrand: bool
    });
  };

  getSeller = (id, name) => {
    this.setState({
      idseller: id,
      nameseller: name
    });
  };

  modalSeller = bool => {
    this.setState({
      modalseller: bool
    });
  };

  getSoftwareDept = (depcode, depname) => {
    this.setState({
      deptcode: depcode,
      deptname: depname
    });
  };

  modalDept = bool => {
    this.setState({
      modaldept: bool
    });
  };

  getLocation = (id, name) => {
    this.setState({
      idlocation: id,
      namelocation: name
    });
  };

  modalLocation = bool => {
    this.setState({
      modallocation: bool
    });
  };

  getSoftwareUser1 = (pfs_id, title, name, last_name) => {
    this.setState({
      pfs_id1: pfs_id,
      name1: title + name + " " + last_name
    });
  };

  getSoftwareUser2 = (pfs_id, title, name, last_name) => {
    this.setState({
      pfs_id2: pfs_id,
      name2: title + name + " " + last_name
    });
  };

  modalUser = bool => {
    this.setState({
      modaluser: bool
    });
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  sendData = e => {
    e.preventDefault();
    Swal.fire({
      title: "คุณต้องการแก้ไขข้อมูลใช่ไหม ?",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "ใช่ แก้ไขเลย",
      cancelButtonText: "ยกเลิก"
    }).then(result => {
      if (result.value) {
        Axios.post(
          REQUEST_URL + "/software/editasset",
          {
            barcode: this.props.match.params.id,
            ascode: this.state.ascode,
            idtype: this.state.idtype,
            nametype: this.state.nametype,
            idbrand: this.state.idbrand,
            namebrand: this.state.namebrand,
            partnumber: this.state.partnumber,
            serial: this.state.serial,
            volumelicense: this.state.volume,
            a_desc: this.state.a_desc,
            startwarranty: moment(this.state.startwarranty).format(
              "YYYY-MM-DD"
            ),
            warranty: this.state.warranty,
            endwarranty: moment(this.state.endwarranty).format("YYYY-MM-DD"),
            price: this.state.price,
            unit: this.state.unit,
            status: this.state.status,
            cdkey: this.state.cdkey,
            checkOS: this.state.checkOS,
            volume: this.state.volume,
            idseller: this.state.idseller,
            nameseller: this.state.nameseller,
            deptcode: this.state.deptcode,
            deptname: this.state.deptname,
            idlocation: this.state.idlocation,
            namelocation: this.state.namelocation,
            pfs_date1: this.state.pfs_date1,
            pfs_id1: this.state.pfs_id1,
            name1: this.state.name1,
            pfs_date2: this.state.pfs_date2,
            pfs_id2: this.state.pfs_id2,
            name2: this.state.name2,
            refno: this.state.refno,
            hold: this.state.hold,
            preby: this.state.preby,
            predate: this.state.predate,
            lastby: sessionStorage.getItem("user_id")
          },
          {
            headers: { authorization: Cookies.get("webcomputer") }
          }
        ).then(response => {
          if (response.data.status) {
            Swal.fire({
              type: "success",
              title: "เพิ่มข้อมูลสำเร็จ...",
              timer: 1200,
              showConfirmButton: false
            });
            this.props.history.push("/mainsoftware");
          } else {
            Swal.fire({
              type: "error",
              title: "เพิ่มข้อมูลไม่สำเร็จ...",
              timer: 1200,
              showConfirmButton: false
            });
          }
        });
      }
    });
  };

  render() {
    const { partnumber, serial, cdkey, volume } = this.state;
    return (
      <div>
        <div className="animated fadeIn">
          รายละเอียด
          <Row>
            <Col md="8">
              <Row style={{ margin: "2%" }}>
                <Col md="6">
                  <Row>
                    <Col md="12">
                      <label style={{ width: "110px", textAlign: "right" }}>
                        รหัส Barcode : 
                      </label>
                      <input
                        type="text"
                        value={this.state.barcode}
                        disabled
                        style={{ width: "270px" }}
                      />
                    </Col>
                    <Col md="12" style={{ marginTop: "3%" }}>
                      <label style={{ width: "110px", textAlign: "right" }}>
                        รหัสทรัพย์สิน : 
                      </label>
                      <input
                        type="text"
                        value={this.state.ascode}
                        disabled
                        style={{
                          width: "240px",
                          backgroundColor: "yellow",
                          fontSize: "18px",
                          textAlign: "center",
                          fontWeight: "bold"
                        }}
                      />
                      <button
                        type="button"
                        block
                        outline
                        color="secondary"
                        disabled
                        onClick={() => this.setState({ modalasset: true })}
                      >
                        <i className="fa fa-search"></i>
                      </button>
                    </Col>
                  </Row>
                </Col>
                <Col md="6"></Col>
              </Row>
              <Row style={{ margin: "2%" }}>
                <Col md="12">
                  <label style={{ width: "110px", textAlign: "right" }}>
                    ประเภท : 
                  </label>
                  <input
                    type="text"
                    value={this.state.idtype}
                    disabled
                    style={{
                      width: "80px",
                      backgroundColor: "black",
                      color: "white",
                      textAlign: "center",
                      fontSize: "18px",
                      fontWeight: "bold"
                    }}
                  />
                  <button
                    type="button"
                    block
                    outline
                    color="secondary"
                    disabled
                    onClick={() => this.setState({ modaltype: true })}
                  >
                    <i className="fa fa-search"></i>
                  </button>
                  <input
                    type="text"
                    value={this.state.nametype}
                    disabled
                    style={{ width: "300px" }}
                  />
                  <label style={{ marginLeft: "20px", width: "50px" }}>
                    ยี่ห้อ : 
                  </label>
                  <input
                    type="text"
                    value={this.state.namebrand}
                    disabled
                    style={{
                      width: "180px",
                      backgroundColor: "red",
                      color: "white",
                      textAlign: "center"
                    }}
                  />
                  <button
                    type="button"
                    block
                    outline
                    color="secondary"
                    onClick={() => this.setState({ modalbrand: true })}
                  >
                    <i className="fa fa-search"></i>
                  </button>
                </Col>
              </Row>
              <Row style={{ margin: "2%" }}>
                <Col md="12">
                  <label style={{ width: "110px", textAlign: "right" }}>
                    Part Number : 
                  </label>
                  <input
                    type="text"
                    name="partnumber"
                    values={partnumber}
                    value={this.state.partnumber}
                    onChange={this.handleChange}
                    style={{ width: "310px" }}
                  />
                  <label style={{ width: "80px", textAlign: "right" }}>
                    Serial : 
                  </label>
                  <input
                    type="text"
                    name="serial"
                    values={serial}
                    value={this.state.serial}
                    onChange={this.handleChange}
                    style={{ width: "300px" }}
                  />
                </Col>
              </Row>
              <Row style={{ margin: "2%" }}>
                <Col md="12">
                  <label style={{ width: "110px", textAlign: "right" }}>
                    รายละเอียด : 
                  </label>
                  <textarea
                    rows="2"
                    value={this.state.a_desc}
                    disabled
                    style={{ verticalAlign: "top", width: "690px" }}
                  />
                </Col>
              </Row>
              <Row style={{ margin: "2%" }}>
                <Col md="12">
                  <label style={{ width: "110px", textAlign: "right" }}>
                    วันที่รับเข้า : 
                  </label>
                  <input
                    disabled
                    value={moment(this.state.startwarranty).format(
                      "DD/MM/YYYY"
                    )}
                    style={{ width: "200px" }}
                  />
                  <label style={{ width: "80px", textAlign: "right" }}>
                    รับประกัน : 
                  </label>
                  <input
                    disabled
                    value={
                      this.state.warranty !== undefined
                        ? this.state.warranty + " " + this.state.yyear
                        : ""
                    }
                    style={{ width: "150px" }}
                  />
                  <label style={{ width: "90px", textAlign: "right" }}>
                    หมดประกัน : 
                  </label>
                  <input
                    disabled
                    value={moment(this.state.endwarranty).format("DD/MM/YYYY")}
                    style={{ width: "170px" }}
                  />
                </Col>
              </Row>
              <Row style={{ margin: "2%" }}>
                <Col md="12">
                  <label style={{ width: "110px", textAlign: "right" }}>
                    ราคา : 
                  </label>
                  <input
                    disabled
                    value={
                      this.state.price !== undefined
                        ? this.state.price + " " + this.state.bath
                        : ""
                    }
                    style={{ width: "200px" }}
                  />
                  <label style={{ width: "80px", textAlign: "right" }}>
                    หน่วย : 
                  </label>
                  <input
                    disabled
                    value={this.state.unit === "SET" ? "ชุด" : this.state.unit}
                    style={{ width: "150px" }}
                  />
                  <label style={{ width: "90px", textAlign: "right" }}>
                    สถานะ : 
                  </label>
                  {this.state.status === undefined ? (
                    <input
                      disabled
                      value={this.state.status === "0" ? "คงอยู่" : ""}
                      style={{ width: "170px" }}
                    />
                  ) : (
                    <input
                      disabled
                      value={this.state.status === "0" ? "คงอยู่" : ""}
                      style={{
                        width: "170px",
                        backgroundColor: "green",
                        color: "white",
                        textAlign: "center"
                      }}
                    />
                  )}
                </Col>
              </Row>
              <Row style={{ margin: "2%" }}>
                <Col md="12">
                  <label style={{ width: "110px", textAlign: "right" }}>
                    KEY : 
                  </label>
                  <input
                    name="cdkey"
                    values={cdkey}
                    value={this.state.cdkey}
                    onChange={this.handleChange}
                    style={{ width: "350px" }}
                  />
                  <input
                    type="checkbox"
                    id="OS"
                    name="OS"
                    checked={this.state.checkOS}
                    onClick={() =>
                      this.setState({
                        checkOS: !this.state.checkOS
                      })
                    }
                    style={{
                      marginLeft: "20px",
                      marginRight: "5px"
                    }}
                  />
                  <label
                    style={{
                      color: "blue",
                      fontWeight: "bold",
                      fontSize: "18px"
                    }}
                    onClick={() =>
                      this.setState({
                        checkOS: !this.state.checkOS
                      })
                    }
                  >
                    OS
                  </label>
                  <label style={{ width: "90px", textAlign: "right" }}>
                    Volume : 
                  </label>
                  <input
                    name="volume"
                    values={volume}
                    value={this.state.volume}
                    onChange={this.handleChange}
                    style={{ width: "110px" }}
                  />
                  <label style={{ marginLeft: "10px" }}>Lisence</label>
                </Col>
              </Row>
            </Col>
          </Row>
          <hr />
          <label>ส่วนทรัพย์สินผู้ครอบครอง</label>
          <Row style={{ margin: "1%" }}>
            <Col md="12">
              <label style={{ width: "100px", textAlign: "right" }}>
                ผู้ขาย : 
              </label>
              <input
                type="text"
                value={this.state.idseller}
                disabled
                style={{ width: "200px" }}
              />
              <button
                type="button"
                block
                outline
                color="secondary"
                onClick={() => this.setState({ modalseller: true })}
              >
                <i className="fa fa-search"></i>
              </button>
              <input
                type="text"
                value={this.state.nameseller}
                disabled
                style={{ width: "700px" }}
              />
            </Col>
            <Col md="12" style={{ marginTop: "1%" }}>
              <label style={{ width: "100px", textAlign: "right" }}>
                แผนก : 
              </label>
              <input
                type="text"
                value={this.state.deptcode}
                disabled
                style={{ width: "200px" }}
              />
              <button
                type="button"
                block
                outline
                color="secondary"
                onClick={() => this.setState({ modaldept: true })}
              >
                <i className="fa fa-search"></i>
              </button>
              <input
                type="text"
                value={this.state.deptname}
                disabled
                style={{ width: "700px" }}
              />
            </Col>
            <Col md="12" style={{ marginTop: "1%" }}>
              <label style={{ width: "100px", textAlign: "right" }}>
                สถานที่ตั้ง : 
              </label>
              <input
                type="text"
                value={this.state.idlocation}
                disabled
                style={{ width: "200px" }}
              />
              <button
                type="button"
                block
                outline
                color="secondary"
                onClick={() => this.setState({ modallocation: true })}
              >
                <i className="fa fa-search"></i>
              </button>
              <input
                type="text"
                value={this.state.namelocation}
                disabled
                style={{ width: "700px" }}
              />
            </Col>
            <Col md="12" style={{ marginTop: "1%" }}>
              <label style={{ width: "100px", textAlign: "right" }}>
                ผู้ครอบครอง : 
              </label>
              <input
                type="text"
                value={this.state.pfs_id1}
                disabled
                style={{ width: "200px" }}
              />
              <button
                type="button"
                block
                outline
                color="secondary"
                onClick={() =>
                  this.setState({
                    modaluser: true,
                    reqsoftwareuser: "Software1"
                  })
                }
              >
                <i className="fa fa-search"></i>
              </button>
              <input
                type="text"
                value={this.state.name1}
                disabled
                style={{ width: "700px" }}
              />
            </Col>
            <Col md="12" style={{ marginTop: "1%" }}>
              <label style={{ width: "100px", textAlign: "right" }}>
                ผู้ดูแล : 
              </label>
              <input
                type="text"
                value={this.state.pfs_id2}
                disabled
                style={{ width: "200px" }}
              />
              <button
                type="button"
                block
                outline
                color="secondary"
                onClick={() =>
                  this.setState({
                    modaluser: true,
                    reqsoftwareuser: "Software2"
                  })
                }
              >
                <i className="fa fa-search"></i>
              </button>
              <input
                type="text"
                value={this.state.name2}
                disabled
                style={{ width: "700px" }}
              />
            </Col>
          </Row>
          <Row>
            <Col
              md="12"
              style={{
                display: "flex",
                justifyContent: "flex-end",
                marginTop: "2%",
                marginBottom: "5%"
              }}
            >
              <Button
                color="success"
                onClick={this.sendData}
                style={{ marginRight: "1%" }}
              >
                บันทึกข้อมูล
              </Button>
              <Button
                color="danger"
                onClick={() => this.props.history.push("/mainsoftware")}
              >
                ยกเลิกการบันทึก
              </Button>
            </Col>
          </Row>
        </div>
        <ModalAsset
          Modal={this.state.modalasset}
          set={this.modalAsset}
          getAsset={this.getAsset}
        />
        <ModalType
          Modal={this.state.modaltype}
          set={this.modalType}
          getType={this.getType}
        />
        <ModalBrand
          Modal={this.state.modalbrand}
          set={this.modalBrand}
          getBrand={this.getBrand}
        />
        <ModalSeller
          Modal={this.state.modalseller}
          set={this.modalSeller}
          getSeller={this.getSeller}
        />
        <ModalDept
          Modal={this.state.modaldept}
          set={this.modalDept}
          REQ="Software"
          getSoftwareDept={this.getSoftwareDept}
        />
        <ModalLocation
          Modal={this.state.modallocation}
          set={this.modalLocation}
          getLocation={this.getLocation}
        />
        <ModalUser
          Modal={this.state.modaluser}
          set={this.modalUser}
          REQ={this.state.reqsoftwareuser}
          getSoftwareUser1={this.getSoftwareUser1}
          getSoftwareUser2={this.getSoftwareUser2}
        />
      </div>
    );
  }
}

export default EditMainSoftware;
