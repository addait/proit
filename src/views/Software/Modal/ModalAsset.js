import React from "react";
import {
  Modal,
  Container,
  Row,
  Col,
  Form,
  Button,
  Table,
  Tab,
  Tabs
} from "react-bootstrap";
import axios from "axios";
import { REQUEST_URL } from "../../../config";
import Cookies from "js-cookie";
import Swal from "sweetalert2";

class ModalAsset extends React.Component {
  state = {
    dataasset: [],
    inputsearchx: ""
  };

  componentDidMount = async () => {
    await this.Asset();
  };

  Asset = async () => {
    await axios
      .post(
        REQUEST_URL + "/software/dataasset",
        {},
        {
          headers: { authorization: Cookies.get("webcomputer") }
        }
      )
      .then(response => {
        if (response.data.status) {
          this.setState({
            dataasset: response.data.data
          });
        } else {
          this.props.history.push("/login");
        }
      });
  };

  modalClose = bool => e => {
    if (!bool) {
      this.setState({
        inputsearchx: ""
      });
    }
    this.props.set(bool);
  };

  searchHandler = e => {
    this.setState({ [e.target.name]: e.target.value });
    this.searchNoButton(e.target.value);
  };

  searchNoButton = txt => {
    let ggez = txt;
    var filtered = this.state.dataasset.filter(function(x) {
      return x.ASCODE.match(new RegExp("^" + ggez === null ? "" : ggez, "i"));
    });
    this.setState({ searchx: filtered.slice(0, 200) });
  };

  toMain = (
    ASCODE,
    a_desc,
    STARTDATE,
    YEAR,
    ENDDATE,
    UNITCOST,
    UNIT,
    sta_adt,
    pfs_id1,
    name1,
    pfs_date1,
    pfs_id2,
    name2,
    pfs_date2,
    refno,
    hold
  ) => e => {
    this.props.getAsset(
      ASCODE,
      a_desc,
      STARTDATE,
      YEAR,
      ENDDATE,
      UNITCOST,
      UNIT,
      sta_adt,
      pfs_id1,
      name1,
      pfs_date1,
      pfs_id2,
      name2,
      pfs_date2,
      refno,
      hold
    );
    this.props.set(false);
    this.setState({
      inputsearchx: ""
    });
  };

  render() {
    const { inputsearchx } = this.state;
    return (
      <div>
        <Modal
          size="xl"
          show={this.props.Modal}
          onHide={this.modalClose(false)}
        >
          <Modal.Header
            closeButton
            id="fontthai"
            style={{ fontSize: "22px" }}
          ></Modal.Header>
          <Modal.Body>
            <label>ค้นหา : </label>
            <input
              type="text"
              name="inputsearchx"
              values={inputsearchx}
              onChange={this.searchHandler}
              placeholder="รหัสทรัพย์สินเพื่อค้นหา"
              style={{ width: "500px", marginBottom: "2%" }}
            />
            <div style={{ display: "flex", justifyContent: "center" }}>
              <table
                //   width="100%"
                style={{
                  display: "block",
                  height: "350px",
                  overflowY: "scroll",
                  alignItems: "center"
                }}
              >
                <thead className="text-center table-active">
                  <tr>
                    <th id="tb" width="8%">
                      สถานะ
                    </th>
                    <th id="tb" width="12%">
                      ASCODE
                    </th>
                    <th id="tb" width="30%">
                      รายละเอียด
                    </th>
                    <th id="tb" width="5%">
                      กลุ่ม
                    </th>
                    <th id="tb" width="10%">
                      รายละเอียดกลุ่ม
                    </th>
                    <th id="tb" width="8%">
                      รหัสผู้ครอบครอง
                    </th>
                    <th id="tb" width="22%">
                      ชื่อผู้ครอบครอง
                    </th>
                    <th id="tb" width="5%">
                      #
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.inputsearchx === ""
                    ? this.state.dataasset.map((item, index) => {
                        return (
                          <tr key={index}>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.sta_adt}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.ASCODE}
                            </td>
                            <td id="tb">{item.a_desc}</td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.GROUP}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.GROUP === "FS" ? "โปรแกรมคอมพิวเตอร์" : ""}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.pfs_id1}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.name1}
                            </td>
                            <td
                              id="tb"
                              style={{
                                textAlign: "center",
                                cursor: "pointer",
                                color: "red"
                              }}
                              onClick={this.toMain(
                                item.ASCODE,
                                item.a_desc,
                                item.STARTDATE,
                                item.YEAR,
                                item.ENDDATE,
                                item.UNITCOST,
                                item.UNIT,
                                item.sta_adt,
                                item.pfs_id1,
                                item.name1,
                                item.pfs_date1,
                                item.pfs_id2,
                                item.name2,
                                item.pfs_date2,
                                item.REFNO,
                                item.HOLD
                              )}
                            >
                              เลือก
                            </td>
                          </tr>
                        );
                      })
                    : this.state.searchx.map((item, index) => {
                        return (
                          <tr key={index}>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.sta_adt}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.ASCODE}
                            </td>
                            <td id="tb">{item.a_desc}</td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.GROUP}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.GROUP === "FS" ? "โปรแกรมคอมพิวเตอร์" : ""}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.pfs_id1}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.name1}
                            </td>
                            <td
                              id="tb"
                              style={{
                                textAlign: "center",
                                cursor: "pointer",
                                color: "red"
                              }}
                              onClick={this.toMain(
                                item.ASCODE,
                                item.a_desc,
                                item.STARTDATE,
                                item.YEAR,
                                item.ENDDATE,
                                item.UNITCOST,
                                item.UNIT,
                                item.sta_adt,
                                item.pfs_id1,
                                item.name1,
                                item.pfs_date1,
                                item.pfs_id2,
                                item.name2,
                                item.pfs_date2,
                                item.REFNO,
                                item.HOLD
                              )}
                            >
                              เลือก
                            </td>
                          </tr>
                        );
                      })}
                </tbody>
              </table>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={this.modalClose(false)}
              id="btnthai"
            >
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default ModalAsset;
