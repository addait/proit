import React from "react";
import {
  Modal,
  Container,
  Row,
  Col,
  Form,
  Button,
  Table,
  Tab,
  Tabs
} from "react-bootstrap";
import axios from "axios";
import { REQUEST_URL } from "../../../config";
import Cookies from "js-cookie";
import Swal from "sweetalert2";

class ModalSeller extends React.Component {
  state = {
    dataseller: [],
    inputsearchx: ""
  };

  componentDidMount = async () => {
    await this.Seller();
  };

  Seller = async () => {
    await axios
      .post(
        REQUEST_URL + "/software/dataseller",
        {},
        {
          headers: { authorization: Cookies.get("webcomputer") }
        }
      )
      .then(response => {
        if (response.data.status) {
          this.setState({
            dataseller: response.data.data
          });
        } else {
          this.props.history.push("/login");
        }
      });
  };

  modalClose = bool => e => {
    if (!bool) {
      this.setState({
        inputsearchx: ""
      });
    }
    this.props.set(bool);
  };

  searchHandler = e => {
    this.setState({ [e.target.name]: e.target.value });
    this.searchNoButton(e.target.value);
  };

  searchNoButton = txt => {
    let ggez = txt;
    var filtered = this.state.dataseller.filter(function(x) {
      return (
        x.supplierID.match(new RegExp("^" + ggez === null ? "" : ggez, "i")) ||
        x.supplierName.match(
          new RegExp("^" + ggez === null ? "" : ggez, "i")
        ) ||
        x.supaddress.match(new RegExp("^" + ggez === null ? "" : ggez, "i"))
      );
    });
    this.setState({ searchx: filtered.slice(0, 200) });
  };

  toMain = (id, name) => e => {
    this.props.getSeller(id, name);
    this.props.set(false);
    this.setState({
      inputsearchx: ""
    });
  };

  render() {
    const { inputsearchx } = this.state;
    return (
      <div>
        <Modal
          size="xl"
          show={this.props.Modal}
          onHide={this.modalClose(false)}
        >
          <Modal.Header
            closeButton
            id="fontthai"
            style={{ fontSize: "22px" }}
          ></Modal.Header>
          <Modal.Body>
            <label>ค้นหา : </label>
            <input
              type="text"
              name="inputsearchx"
              values={inputsearchx}
              onChange={this.searchHandler}
              placeholder="รหัสผู้ขายเพื่อค้นหา"
              style={{ width: "500px", marginBottom: "2%" }}
            />
            <div style={{ display: "flex", justifyContent: "center" }}>
              <table
                //   width="100%"
                style={{
                  display: "block",
                  height: "350px",
                  overflowY: "scroll",
                  alignItems: "center"
                }}
              >
                <thead className="text-center table-active">
                  <tr>
                    <th id="tb" width="10%">
                      รหัส
                    </th>
                    <th id="tb" width="40%">
                      ผู้ขาย
                    </th>
                    <th id="tb" width="20%">
                      ผู้ติดต่อ
                    </th>
                    <th id="tb" width="25%">
                      จำหน่าย
                    </th>
                    <th id="tb" width="5%">
                      #
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.inputsearchx === ""
                    ? this.state.dataseller.map((item, index) => {
                        return (
                          <tr key={index}>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.supplierID}
                            </td>
                            <td id="tb">{item.supplierName}</td>
                            <td id="tb">{item.supcontact}</td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.supdetail}
                            </td>
                            <td
                              id="tb"
                              style={{
                                textAlign: "center",
                                cursor: "pointer",
                                color: "red"
                              }}
                              onClick={this.toMain(
                                item.supplierID,
                                item.supplierName
                              )}
                            >
                              เลือก
                            </td>
                          </tr>
                        );
                      })
                    : this.state.searchx.map((item, index) => {
                        return (
                          <tr key={index}>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.sta_adt}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.ASCODE}
                            </td>
                            <td id="tb">{item.a_desc}</td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.GROUP}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.GROUP === "FS" ? "โปรแกรมคอมพิวเตอร์" : ""}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.pfs_id1}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.name1}
                            </td>
                            <td
                              id="tb"
                              style={{
                                textAlign: "center",
                                cursor: "pointer",
                                color: "red"
                              }}
                              onClick={this.toMain(
                                item.supplierID,
                                item.supplierName
                              )}
                            >
                              เลือก
                            </td>
                          </tr>
                        );
                      })}
                </tbody>
              </table>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={this.modalClose(false)}
              id="btnthai"
            >
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default ModalSeller;
