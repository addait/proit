import React from "react";
import { withRouter } from "react-router-dom";
import {
  Card,
  CardBody,
  Col,
  Row,
  Button,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon
} from "reactstrap";
import Spinner from "react-spinkit";
import Axios from "axios";
import Cookies from "js-cookie";
import { REQUEST_URL } from "../../config";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import moment from "moment";
// import OrderDetail from "./OrderDetail";
import Swal from "sweetalert2";
import "../../index.css";

const customTotal = (from, to, size) => (
  <span
    className="react-bootstrap-table-pagination-total"
    style={{ marginLeft: "5%" }}
  >
    Showing {from} to {to} of {size} Results
  </span>
);

class MainSoftware extends React.Component {
  state = {
    data: [],
    filterData: [],
    modal: false,
    LoadData: false,
    inputsearch: "",
    selection: "",
    columns: [
      {
        dataField: "startdate",
        text: "วันที่รับเข้า",
        headerStyle: (colum, colIndex) => {
          return { width: "9%", textAlign: "center" };
        },
        formatter: (cell, row) => {
          return (
            <div style={{ textAlign: "center" }}>
              <label>{moment(row.startdate).format("DD/MM/YYYY")}</label>
            </div>
          );
        }
      },
      {
        dataField: "barcode",
        text: "รหัสบาร์โค้ด",
        headerStyle: (colum, colIndex) => {
          return { width: "12%", textAlign: "center" };
        },
        formatter: (cell, row) => {
          return <div style={{ textAlign: "center" }}>{row.barcode}</div>;
        }
      },

      {
        dataField: "brandName",
        text: "ยี่ห้อ",
        headerStyle: (colum, colIndex) => {
          return { width: "12%", textAlign: "center" };
        },
        formatter: (cell, row) => {
          return <div style={{ textAlign: "center" }}>{row.brandName}</div>;
        }
      },
      {
        dataField: "name",
        text: "รายละเอียด",
        headerStyle: (colum, colIndex) => {
          return { width: "28%", textAlign: "center" };
        }
      },
      {
        dataField: "unit",
        text: "หน่วย",
        headerStyle: (colum, colIndex) => {
          return { width: "7%", textAlign: "center" };
        },
        formatter: (cell, row) => {
          return <div style={{ textAlign: "center" }}>{row.unit}</div>;
        }
      },
      {
        dataField: "price",
        text: "ราคา",
        headerStyle: (colum, colIndex) => {
          return { width: "7%", textAlign: "center" };
        },
        formatter: (cell, row) => {
          return <div style={{ textAlign: "center" }}>{row.price}</div>;
        }
      },
      {
        dataField: "predate",
        text: "วันที่บันทึก",
        headerStyle: (colum, colIndex) => {
          return { width: "10%", textAlign: "center" };
        },
        formatter: (cell, row) => {
          return <div>{moment(row.predate).format("DD/MM/YYYY")}</div>;
        }
      },
      {
        dataField: "preby",
        text: "ผู้บันทึก",
        headerStyle: (colum, colIndex) => {
          return { width: "10%", textAlign: "center" };
        },
        formatter: (cell, row) => {
          return <div style={{ textAlign: "center" }}>{row.preby}</div>;
        }
      },
      {
        dataField: "lastdate",
        text: "วันที่แก้ไข",
        headerStyle: (colum, colIndex) => {
          return { width: "10%", textAlign: "center" };
        },
        formatter: (cell, row) => {
          return (
            <div>
              {row.lastdate !== null
                ? moment(row.lastdate).format("DD/MM/YYYY")
                : ""}
            </div>
          );
        }
      },
      {
        dataField: "lastby",
        text: "ผู้แก้ไข",
        headerStyle: (colum, colIndex) => {
          return { width: "10%", textAlign: "center" };
        },
        formatter: (cell, row) => {
          return <div style={{ textAlign: "center" }}>{row.lastby}</div>;
        }
      }
    ],
    selectRow: {
      mode: "radio",
      clickToSelect: true,
      bgColor: "#00BFFF",
      hideSelectAll: true,
      hideSelectColumn: true,
      onSelect: row => {
        this.setState({
          selection: row.barcode
        });
      }
    },
    options: {
      paginationSize: 5,
      pageStartIndex: 1,
      firstPageText: "First",
      prePageText: "Back",
      nextPageText: "Next",
      lastPageText: "Last",
      nextPageTitle: "First page",
      prePageTitle: "Pre page",
      firstPageTitle: "Next page",
      lastPageTitle: "Last page",
      showTotal: true,
      paginationTotalRenderer: customTotal,
      sizePerPageList: [
        {
          text: "30",
          value: 30
        },
        {
          text: "50",
          value: 50
        },
        {
          text: "100",
          value: 100
        }
      ]
    }
  };

  componentDidMount() {
    this.showData();
    // this.chkPermission();
  }

  chkPermission = async () => {
    await Axios.post(
      REQUEST_URL + "/users/chkpermission",
      {
        user: Cookies.get("webcomputer"),
        file: "F02"
      },
      {
        headers: { authorization: Cookies.get("webcomputer") }
      }
    ).then(res => {
      if (res.data.success) {
        this.setState({
          act_open: res.data.data[0].act_open,
          act_add: res.data.data[0].act_add,
          act_edit: res.data.data[0].act_edit,
          act_view: res.data.data[0].act_view,
          act_print: res.data.data[0].act_print
        });
      } else {
        this.props.history.push("/login");
      }
    });
  };

  showData = async () => {
    await Axios.get(REQUEST_URL + "/software/datasoftware", {
      headers: { authorization: Cookies.get("webcomputer") }
    }).then(response => {
      if (response.data.status) {
        this.setState({
          filterData: response.data.data
        });
      } else {
        this.props.history.push("/login");
      }
    });
  };

  searchHandle = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
    this.searchNoButton(e.target.value);
  };

  searchNoButton = txt => {
    let ggez = txt;
    var filtered = this.state.filterData.filter(function(x) {
      return (
        x.barcode.match(new RegExp("^" + ggez === null ? "" : ggez, "i")) ||
        x.assetID.match(new RegExp("^" + ggez === null ? "" : ggez, "i"))
      );
    });
    this.setState({ searchx: filtered.slice(0, 200) });
  };

  deleteData = () => {
    Swal.fire({
      title: "คุณต้องการลบข้อมูลใช่ไหม ?",
      text: this.state.selection,
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "ใช่",
      cancelButtonText: "ไม่ใช่"
    }).then(result => {
      if (result.value) {
        Axios.post(
          REQUEST_URL + "/software/deleteasset",
          { id: this.state.selection },
          {
            headers: { authorization: Cookies.get("webcomputer") }
          }
        ).then(response => {
          if (response.data.status) {
            Swal.fire({
              type: "success",
              title: "ลบข้อมูลสำเร็จ...",
              timer: 1200,
              showConfirmButton: false
            });
            this.showData();
            this.setState({
              inputsearch: ""
            });
          } else {
            Swal.fire({
              type: "error",
              title: "ลบข้อมูลไม่สำเร็จ...",
              timer: 1200,
              showConfirmButton: false
            });
          }
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire({
          type: "error",
          title: "ยกเลิกการลบข้อมูล",
          timer: 1200,
          showConfirmButton: false
        });
      }
    });
  };

  render() {
    const { inputsearch } = this.state;
    return this.state.filterData.length === 0 ? (
      <div className="animated fadeIn pt-5 text-center">
        <Spinner name="three-bounce" />{" "}
      </div>
    ) : (
      <div className="animated fadeIn">
        <Row>
          <Col xs={12}>
            <Card>
              <CardBody>
                <FormGroup row>
                  <Col md="3">
                    <InputGroup>
                      <Input
                        type="text"
                        name="inputsearch"
                        values={inputsearch}
                        placeholder="ค้นหา"
                        onChange={this.searchHandle}
                      />
                    </InputGroup>
                  </Col>
                  <div
                    style={{ marginRight: "1%" }}
                    onClick={() => this.props.history.push("/main/software/new")}
                  >
                    <Button color="success">เพิ่มข้อมูล</Button>
                  </div>
                  <div
                    style={{ marginRight: "1%" }}
                    onClick={() =>
                      this.state.selection === ""
                        ? Swal.fire({
                            type: "error",
                            title: "เลือกอย่างน้อย 1 รายการ",
                            timer: 1200
                          })
                        : this.props.history.push(
                            "/main/software/edit/" + this.state.selection
                          )
                    }
                  >
                    <Button color="warning">แก้ไขข้อมูล</Button>
                  </div>
                  <div
                    style={{ marginRight: "1%" }}
                    onClick={() =>
                      this.state.selection === ""
                        ? Swal.fire({
                            type: "error",
                            title: "เลือกอย่างน้อย 1 รายการ",
                            timer: 1200
                          })
                        : this.props.history.push(
                            "/main/software/view/" + this.state.selection
                          )
                    }
                  >
                    <Button color="info">มุมมอง</Button>
                  </div>
                  <div style={{ marginRight: "1%" }}>
                    <Button
                      color="danger"
                      onClick={() =>
                        this.state.selection === ""
                          ? Swal.fire({
                              type: "error",
                              title: "เลือกอย่างน้อย 1 รายการ",
                              timer: 1200
                            })
                          : this.deleteData()
                      }
                    >
                      ลบข้อมูล
                    </Button>
                  </div>
                </FormGroup>
                {this.state.inputsearch === "" ? (
                  <BootstrapTable
                    keyField="barcode"
                    data={this.state.filterData}
                    columns={this.state.columns}
                    pagination={paginationFactory(this.state.options)}
                    selectRow={this.state.selectRow}
                  />
                ) : (
                  <BootstrapTable
                    keyField="barcode"
                    data={this.state.searchx}
                    columns={this.state.columns}
                    pagination={paginationFactory(this.state.options)}
                    selectRow={this.state.selectRow}
                  />
                )}
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default withRouter(MainSoftware);
