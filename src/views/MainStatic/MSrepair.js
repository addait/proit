import React from 'react'
import {
    Card,
    CardBody,
    Col,
    Row,
    FormGroup,
    InputGroup,
    Input
  } from "reactstrap";
  import BootstrapTable from "react-bootstrap-table-next";
  import {  HorizontalBar } from "react-chartjs-2";

class MSrepair extends React.Component{
    state={
        dataHori: {
            labels: ['ฮาร์ดแวร์', 'ซอฟต์แวร์', 'Network', 'Program'],
            datasets: [
              {
                label: 'จำนวนงาน',
                backgroundColor: 'rgba(255,99,132,0.2)',
                borderColor: 'rgba(255,99,132,1)',
                borderWidth: 1,
                hoverBackgroundColor: 'rgba(255,99,132,0.4)',
                hoverBorderColor: 'rgba(255,99,132,1)',
                data: [36, 5, 5, 3]
              }
            ]
          },
          columns: [
            {
              dataField: "id",
              text: "Product ID",
              align: "center",
              headerAlign: "center"
            },
            {
              dataField: "name",
              text: "Product Name",
              align: "center",
              headerAlign: "center"
            },
            {
              dataField: "namereal",
              text: "Product Price",
              align: "center",
              headerAlign: "center"
            },
            {
              dataField: "prirate",
              text: "Product Price",
              align: "center",
              headerAlign: "center"
            },
            {
              dataField: "price",
              text: "Product Price",
              align: "center",
              headerAlign: "center"
            }
          ],
          datatable: [
            { id: "1", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
            { id: "2", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
            { id: "3", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
            { id: "4", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
            { id: "5", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
            { id: "6", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
            { id: "7", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
            { id: "8", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
            { id: "9", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
            { id: "10", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
            { id: "11", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
            { id: "12", name: "Smith", price: 199, namereal: "dew", prirate: 509 }
          ],
    }
    render(){
        return(
            <div className="animated fadeIn" >
<Row>
          <Col xs={6}>
            {/* <Card >
              <CardBody> */}
                {/* <FormGroup row style={{ padding: "5px" }}> */}
                <h5><b>พนักงานซ่อม</b></h5>
                <hr />
                <BootstrapTable
                keyField="id"
                data={this.state.datatable}
                columns={this.state.columns}
              />
              <div style={{display:'flex' , justifyContent:'flex-end'}}>
              <label style={{fontSize:'18px' , fontWeight:'bold' , color:'red'}}>รวมทั้งสิ้น (งาน) </label>
              </div>             
                {/* </FormGroup>
              </CardBody>
            </Card> */}
          </Col>
          <Col xs={6}>
                   <Card >
              <CardBody>
              <h5><b>รหัสพนักงาน</b></h5>
                <hr />
                <FormGroup row style={{ padding: "5px" }}>
                <HorizontalBar data={this.state.dataHori}/>
               </FormGroup>
              </CardBody>
            </Card>
          </Col>
          </Row>
            </div>
        )
    }
}
export default MSrepair