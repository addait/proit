import React from "react";
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from "reactstrap";
import Cmedit from "./CMedit";
import CMview from "./CMview";
import CMadd from "./CMadd";

class CameraModal extends React.Component {
  close = () => {
    this.props.setModal(false);
  };
  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.show}
          toggle={this.close}
          style={{ maxWidth: "1024px" }}
        >
          <ModalHeader toggle={this.close}>
            {this.props.text === 0 ? "แก้ไขข้อมูล" : this.props.text === 1 ? "มุมมองข้อมูล" : "เพิ่มข้อมูล"}
          </ModalHeader>
          <ModalBody>
            {this.props.text === 0 ? (
              <Cmedit
                data={this.props.data}
                close={this.close}
                refresh={this.props.refresh}
              />
            ) : (
              this.props.text === 1 ? <CMview data={this.props.data} close={this.close} /> :
              <CMadd close={this.close}  refresh={this.props.refresh} />
            )}
          </ModalBody>
        </Modal>
      </div>
    );
  }
}
export default CameraModal;
