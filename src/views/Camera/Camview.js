import React from "react";
import { Card, CardBody, Col, Row, Button, FormGroup } from "reactstrap";
import {
  Input,
  Upload,
  Icon,
  Radio,
  DatePicker,
  Checkbox,
  InputNumber,
  Select
} from "antd";
import "./Camera.css";
import Axios from 'axios'
import moment from "moment";
import Cookies from 'js-cookie'
import { REQUEST_URL } from "../../config";
import {Apicamera} from './Camconfig'


const InputGroup = Input.Group;
const { Option } = Select;
const { Search, TextArea } = Input;

class Camview extends React.Component {

 async componentDidMount(){
    await this.showdata()
    this.Modeldata()
 }

  state = {
    fileList: [],
    fileimg: [],
    date: '',
    ib1:'',
    ib2:'',
    type:'',
    checked:false,
    asCode:'',
    Model:'',
    serial:'',
    Firmware:'',
    local:'',
    ip:'',
    num:0,
    status:'',
    datewaran: '',
    remark:'',
    waranyear: "3",
    gennumber:'',
  };

  Modeldata = async () => {
    await Axios.post(
      REQUEST_URL + "/camera/cctv",
      {cctv:this.state.ib1},
      {
        headers: { authorization: Cookies.get("webcomputer") }
      }
    )
      .then(res => {
        // console.log(res.data.data[0].model)
        this.setState({ ib2: res.data.data[0].model });

      })
      .catch(err => {
        console.log(err);
      });
  }

  showdata = () => {
    return this.props.data.map((row,index) => {
           this.setState({
           type:parseInt(row.type),
           checked:row.notfity,
           serial:row.serialNo,
           asCode:row.asCode,
           Model:row.model,
           Firmware:row.firmware,
           local:row.location,
           ip:row.ip,
           num:row.price,
           status:parseInt(row.status),
           waranyear:row.waranty,
           remark:row.remark,
           fileList:row.picname === null || row.picname === '' ? [] :        
            [
              {
                uid: "-1",
                name:  row.picname,
                status: "done",
                url: Apicamera + row.picname 
              }
            ],
            ib1:row.cctvID,
          })
      })
  }

  render() {
    return (
      <div>
        <Row>
          <Col xs={8}>
            <Card>
              <CardBody>
                <FormGroup row>
                  <Col xs={6}>
                    <span>วันที่เริ่มใช้งาน : </span>
                    <br />
                    <DatePicker
                      defaultValue={moment(moment(this.props.data[0].startdate).format("DD/MM/YYYY"),"DD/MM/YYYY")}
                      format={"DD/MM/YYYY"}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>อยู่ในกล่อง : </span>
                    <br />
                    <InputGroup compact>
                      <Search
                        style={{ width: "30%" }}
                        value={this.state.ib1}
                        enterButton
                      />
                      <Input
                        style={{ width: "70%" }}
                        value={this.state.ib2}
                        
                      />
                    </InputGroup>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={8}>
                    <span>ประเภท : </span>
                    <br />
                    <Radio.Group                                          
                      value={this.state.type}
                    >
                      <Radio value={0} id='type'>Dome</Radio>
                      <Radio value={1} id='type'>Bullet</Radio>
                      <Radio value={2} id='type'>Other</Radio>
                    </Radio.Group>
                  </Col>
                  <Col xs={4}>
                    <span>แจ้งเตือน : </span>
                    <br />
                    <Checkbox
                    checked={this.state.checked}
                    onChange={this.onChangenum}
                    >                      
                      <label style={{ color: "red" }}>
                        แจ้งเตือนเมื่อกล้องดับ
                      </label>
                    </Checkbox>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>รหัสทรัพย์สิน :</span>
                    <Input
                      placeholder="รหัสทรัพย์สิน"
                      id="asCode"
                      value={this.state.asCode}                     
                      autoComplete={"off"}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>Model :</span>
                    <Input
                      placeholder="Model"
                      id="Model"
                      value={this.state.Model}                     
                      autoComplete={"off"}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>Serial No. :</span>
                    <Input
                      placeholder="Serial No."
                      id="serial"
                      value={this.state.serial}                     
                      autoComplete={"off"}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>Firmware Version :</span>
                    <Input
                      placeholder="Firmware Version"
                      id="Firmware"
                      value={this.state.Firmware}                     
                      autoComplete={"off"}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>สถานที่ :</span>
                    <Input
                      placeholder="สถานที่"
                      id="local"
                      value={this.state.local}                     
                      autoComplete={"off"}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={6}>
                    <span>IP Address :</span>
                    <Input
                      placeholder="IP Address"
                      id="ip"
                      value={this.state.ip}                     
                      autoComplete={"off"}
                    />
                  </Col>
                  <Col xs={6}>
                    <span>ราคา (บาท) :</span>
                    <br />
                    <InputNumber
                    // onChange={(e)=>this.onChangenum(e,'num')}      
                      value={this.state.num}
                      style={{ width: "50%" }}
                      defaultValue={0.0}
                      formatter={value =>
                        `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                      }
                      parser={value => value.replace(/\$\s?|(,*)/g, "")}
                    />
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
          <Col xs={4}>
            <Card>
              <CardBody>
                <FormGroup row>
                  <Upload
                    showUploadList={
                     { showDownloadIcon : false}
                    }
                    action={REQUEST_URL + "/api"}
                    accept="image/,.JPG"
                    listType="picture-card"
                    fileList={this.state.fileList}
                    disabled={true}
                  >
                    {this.state.fileList.length >= 1 ? null : (
                      <div>
                        <Icon type="plus" />
                        <div className="ant-upload-text">อัพโหลดรูปภาพ</div>
                      </div>
                    )}
                  </Upload>
                </FormGroup>
              </CardBody>
            </Card>
            <Card>
              <CardBody>
                <FormGroup row>
                  <Col xs={12}>
                    <span>วันที่เริ่มรับประกัน : </span>
                    <br />
                    <InputGroup compact>
                      <DatePicker
                        defaultValue={moment(
                          moment(this.props.data[0].startWaranty).format("DD/MM/YYYY"),
                          "DD/MM/YYYY"
                        )}
                        format={"DD/MM/YYYY"}
                      />
                      <Select                
                        value={this.state.waranyear}
                      >
                        <Option value="1">1 ปี</Option>
                        <Option value="2">2 ปี</Option>
                        <Option value="3">3 ปี</Option>
                        <Option value="4">4 ปี</Option>
                        <Option value="5">5 ปี</Option>
                      </Select>
                    </InputGroup>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>สถานะ : </span>
                    <br />
                    <Radio.Group
                      value={this.state.status}
                    >
                      <Radio value={0} id='status'>ใช้งานอยู๋</Radio>
                      <Radio value={1} id='status'>ชำรุด</Radio>
                      <Radio value={2} id='status'>ไม่ใช้งาน</Radio>
                    </Radio.Group>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>หมายเหตุ :</span>
                    <TextArea
                      placeholder="หมายเหตุ"
                      allowClear
                      id="remark"
                      value={this.state.remark}                     
                    />
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <hr />
        <Row
          style={{
            marginRight: "3%",
            display: "flex",
            justifyContent: "flex-end"
          }}
        >
          <Col xs={1}>
            <Button color="danger" onClick={this.props.close}>
              ยกเลิก
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}
export default Camview;
