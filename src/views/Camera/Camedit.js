import React from "react";
import { Card, CardBody, Col, Row, Button, FormGroup } from "reactstrap";
import {
  Input,
  Upload,
  Icon,
  Radio,
  DatePicker,
  Checkbox,
  InputNumber,
  Select
} from "antd";
import { REQUEST_URL } from "../../config";
import moment from "moment";
import "./Camera.css";
import "../../assets/img/brand/IT.png";
import Axios from "axios";
import Swal from "sweetalert2";
import Cookies from "js-cookie";
import {Apicamera} from './Camconfig'


const InputGroup = Input.Group;
const { Option } = Select;
const { Search, TextArea } = Input;

class Camedit extends React.Component {

 async componentDidMount(){
    await this.showdata()
    this.Modeldata()
  }



  state = {
    fileList: [],
    fileimg: [],
    camID:'',
    picname:'',
    date: moment().format("YYYY-MM-DD"),
    ib1:'',
    ib2:'',
    type:'',
    checked:false,
    asCode:'',
    Model:'',
    serial:'',
    Firmware:'',
    local:'',
    ip:'',
    num:0,
    status:'',
    datewaran: moment().format("YYYY-MM-DD"),
    remark:'',
    waranyear: "3",
    gennumber:'',
    date1:'',
    datewaran1:'',
  };

  Modeldata = async () => {
    await Axios.post(
      REQUEST_URL + "/camera/cctv",
      {cctv:this.state.ib1},
      {
        headers: { authorization: Cookies.get("webcomputer") }
      }
    )
      .then(res => {
        // console.log(res.data.data[0].model)
        this.setState({ ib2: res.data.data[0].model });

      })
      .catch(err => {
        console.log(err);
      });
  }

  showdata = () => {
    //   console.log(this.props.data)
    return this.props.data.map((row,index) => {
           this.setState({
           camID:row.cameraID,
           picname:row.picname,
           type:parseInt(row.type),
           checked:row.notfity,
           serial:row.serialNo,
           asCode:row.asCode,
           Model:row.model,
           Firmware:row.firmware,
           local:row.location,
           ip:row.ip,
           num:row.price,
           status:parseInt(row.status),
           waranyear:row.waranty,
           remark:row.remark,
           fileList:row.picname === null || row.picname === '' ? [] :        
            [
              {
                uid: "-1",
                name:  row.picname,
                status: "done",
                url: Apicamera + row.picname 
              }
            ],
            ib1:row.cctvID,
            datewaran:moment(row.startWaranty).format('YYYY-MM-DD'),
            date:moment(row.startdate).format('YYYY-MM-DD'),
            datewaran1:moment(row.startWaranty).format('YYYY-MM-DD'),
            date1:moment(row.startdate).format('YYYY-MM-DD'),

          })
      })
  }
  
  camUpdate = async (img) => {
    await Axios.post(REQUEST_URL + '/camera/cameraupdate' , {
          cameraID:this.state.camID,
          cctvID : this.props.ib1 === '' ? this.state.ib1 : this.props.ib1,
          status : this.state.status,
          asCode : this.state.asCode,
          model : this.state.Model,
          serialNo : this.state.serial,
          firmware : this.state.Firmware,
          type : this.state.type,
          ip : this.state.ip,
          location : this.state.local,
          startWaranty : this.state.datewaran === null ? this.state.datewaran1 : this.state.datewaran, 
          waranty : this.state.waranyear,
          picname : img === 'no data'  ? this.state.picname : img,
          remark : this.state.remark,
          notfity : this.state.checked,
          startdate : this.state.date === null ? this.state.date1 : this.state.date,  
          price : this.state.num,
          lastby : sessionStorage.getItem('user_id')
    } ,{
      headers: { authorization: Cookies.get("webcomputer") }
    })
    .then(res => {
      if(res.data.status){
        Swal.fire({
          type: 'success',
          title: 'เสร็จสิ้น !',
          text: 'การบันทึกข้อมูลเสร็จสิ้น !',
          confirmButtonText: 'ตกลง',
        })
      }
    })
    .catch(err => {
      console.log(err);
    });
  }

  deleteimg = async () => {
    await Axios.post(REQUEST_URL + "/camera/cameraimgdelete", {
      id:this.state.camID
    } , {
          
      headers: { authorization: Cookies.get("webcomputer") }
    })
    .then( async res => {
    //   console.log(res)
    })
    .catch(err => {
      console.log(err);
    });
  }

  save = async () => {

    if(this.state.fileimg === undefined){
        Swal.fire({
          type: 'error',
          title: 'ข้อผิดพลาด !',
          text: 'ไม่สามารถลบรูปภาพได้ กรุณาเพิ่มรูปภาพ',
          confirmButtonText:'ตกลง',
        })
    }else{
    if(this.state.fileimg.length === 0){
     await this.camUpdate('no data')
      this.props.refresh()
      this.props.close()
    }else{
      const data = new FormData();
      data.append("file", this.state.fileimg,moment().format('ss') + this.state.picname.substring(0,this.state.picname.length - 4));
      await Axios.post(REQUEST_URL + "/camera/cameraimg", data , {
        
        headers: { authorization: Cookies.get("webcomputer") }
      })
      .then( async res => {
        if(res.data.status){
          await this.deleteimg()
          await this.camUpdate(res.data.name + '.jpg')
        this.props.refresh()
          this.props.close()     
        }else{
          Swal.fire({
            type: 'error',
            title: 'ข้อผิดพลาด !',
            text: 'ไม่สามารถบันทึกไฟล์รูปภาพได้!',
            confirmButtonText: 'ตกลง',
          })
        }
      })
      .catch(err => {
        console.log(err);
      });
    }
    }
  }



  handleChange = e => {
    // console.log(e)
    this.setState({
      fileList: e.fileList,
      fileimg: e.file.originFileObj === undefined ? [] : e.file.originFileObj
    });
  };

  onChange = (e) => {
    // console.log(  [e.target.id]  + ':' +e.target.value)
    this.setState({
      [e.target.id] :e.target.value
    })
  }

  onChangenum = (e,s) => {
    // console.log( e,s)
    if(s == 'num'){
      this.setState({num:e})
    }else if (s == 'waranyear'){
      this.setState({waranyear:e})
    }else{
      this.setState({checked:!this.state.checked})
    }
  }

  open = () => {
    this.props.setModal(true)
    this.props.bg(true)
  }

  render() {
    return (
      <div>
        <Row>
          <Col xs={8}>
            <Card>
              <CardBody>
                <FormGroup row>
                  <Col xs={6}>
                    <span>วันที่เริ่มใช้งาน : </span>
                    <br />
                    <DatePicker
                      defaultValue={moment(
                        moment().format("DD/MM/YYYY"),
                        "DD/MM/YYYY"
                      )}
                      format={"DD/MM/YYYY"}
                      onChange={e =>
                        this.setState({
                          date:
                            e == null ? "" : moment(e._d).format("YYYY-MM-DD")
                        })
                      }
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>อยู่ในกล่อง : </span>
                    <br />
                    <InputGroup compact>
                      <Search
                        style={{ width: "30%" }}
                        value={this.props.ib1 === '' ? this.state.ib1 : this.props.ib1}
                        onSearch={this.open}
                        enterButton
                      />
                      <Input
                        style={{ width: "70%" }}
                        value={this.props.ib2 === '' ? this.state.ib2 : this.props.ib2}
                        
                      />
                    </InputGroup>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={8}>
                    <span>ประเภท : </span>
                    <br />
                    <Radio.Group                     
                      onChange={this.onChange}
                      value={this.state.type}
                    >
                      <Radio value={0} id='type'>Dome</Radio>
                      <Radio value={1} id='type'>Bullet</Radio>
                      <Radio value={2} id='type'>Other</Radio>
                    </Radio.Group>
                  </Col>
                  <Col xs={4}>
                    <span>แจ้งเตือน : </span>
                    <br />
                    <Checkbox
                    checked={this.state.checked}
                    onChange={this.onChangenum}
                    >                      
                      <label style={{ color: "red" }}>
                        แจ้งเตือนเมื่อกล้องดับ
                      </label>
                    </Checkbox>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>รหัสทรัพย์สิน :</span>
                    <Input
                      placeholder="รหัสทรัพย์สิน"
                      id="asCode"
                      value={this.state.asCode}
                      onChange={this.onChange}
                      autoComplete={"off"}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>Model :</span>
                    <Input
                      placeholder="Model"
                      id="Model"
                      value={this.state.Model}
                      onChange={this.onChange}
                      autoComplete={"off"}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>Serial No. :</span>
                    <Input
                      placeholder="Serial No."
                      id="serial"
                      value={this.state.serial}
                      onChange={this.onChange}
                      autoComplete={"off"}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>Firmware Version :</span>
                    <Input
                      placeholder="Firmware Version"
                      id="Firmware"
                      value={this.state.Firmware}
                      onChange={this.onChange}
                      autoComplete={"off"}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>สถานที่ :</span>
                    <Input
                      placeholder="สถานที่"
                      id="local"
                      value={this.state.local}
                      onChange={this.onChange}
                      autoComplete={"off"}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={6}>
                    <span>IP Address :</span>
                    <Input
                      placeholder="IP Address"
                      id="ip"
                      value={this.state.ip}
                      onChange={this.onChange}
                      autoComplete={"off"}
                    />
                  </Col>
                  <Col xs={6}>
                    <span>ราคา (บาท) :</span>
                    <br />
                    <InputNumber
                    onChange={(e)=>this.onChangenum(e,'num')}      
                      value={this.state.num}
                      style={{ width: "50%" }}
                      defaultValue={0.0}
                      formatter={value =>
                        `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                      }
                      parser={value => value.replace(/\$\s?|(,*)/g, "")}
                    />
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
          <Col xs={4}>
            <Card>
              <CardBody>
                <FormGroup row>
                  <Upload
                  showUploadList={
                    { showDownloadIcon : false}
                   }
                    className="uploadnone"
                    action={REQUEST_URL + "/api"}
                    accept="image/,.JPG"
                    listType="picture-card"
                    fileList={this.state.fileList}
                    onChange={this.handleChange}
                    onRemove={() => this.setState({ gennum: "" })}
                    disabled={false}
                  >
                    {this.state.fileList.length >= 1 ? null : (
                      <div>
                        <Icon type="plus" />
                        <div className="ant-upload-text">อัพโหลดรูปภาพ</div>
                      </div>
                    )}
                  </Upload>
                </FormGroup>
              </CardBody>
            </Card>
            <Card>
              <CardBody>
                <FormGroup row>
                  <Col xs={12}>
                    <span>วันที่เริ่มรับประกัน : </span>
                    <br />
                    <InputGroup compact>
                      <DatePicker
                        defaultValue={moment(
                          moment().format("DD/MM/YYYY"),
                          "DD/MM/YYYY"
                        )}
                        format={"DD/MM/YYYY"}
                        onChange={e =>
                          this.setState({
                            datewaran:
                              e == null ? "" : moment(e._d).format("YYYY-MM-DD")
                          })
                        }
                      />
                      <Select
                        onChange={(e)=>this.onChangenum(e,'waranyear')}                        
                        value={this.state.waranyear}
                      >
                        <Option value="1">1 ปี</Option>
                        <Option value="2">2 ปี</Option>
                        <Option value="3">3 ปี</Option>
                        <Option value="4">4 ปี</Option>
                        <Option value="5">5 ปี</Option>
                      </Select>
                    </InputGroup>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>สถานะ : </span>
                    <br />
                    <Radio.Group
                      onChange={this.onChange}
                      value={this.state.status}
                    >
                      <Radio value={0} id='status'>ใช้งานอยู๋</Radio>
                      <Radio value={1} id='status'>ชำรุด</Radio>
                      <Radio value={2} id='status'>ไม่ใช้งาน</Radio>
                    </Radio.Group>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>หมายเหตุ :</span>
                    <TextArea
                      placeholder="หมายเหตุ"
                      allowClear
                      id="remark"
                      value={this.state.remark}
                      onChange={this.onChange}
                    />
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <hr />
        <Row
          style={{
            marginRight: "3%",
            display: "flex",
            justifyContent: "flex-end"
          }}
        >
          <Col xs={1}>
            <Button color="primary" onClick={this.save}>
              บันทึก
            </Button>
          </Col>
          <Col xs={1}>
            <Button color="danger" onClick={this.props.close}>
              ยกเลิก
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}
export default Camedit;
