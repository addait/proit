import React from "react";
import { Card, CardBody, Col, Row, Button, FormGroup } from "reactstrap";
import {
  Input,
  Upload,
  Icon,
  Radio,
  DatePicker,
  Checkbox,
  InputNumber,
  Select
} from "antd";
import { REQUEST_URL } from "../../config";
import moment from "moment";
import "./Camera.css";
import "../../assets/img/brand/IT.png";
import Axios from "axios";
import Swal from "sweetalert2";
import Cookies from "js-cookie";
import { async } from "q";

const InputGroup = Input.Group;
const { Option } = Select;
const { Search, TextArea } = Input;

class Camadd extends React.Component {
  state = {
    fileList: [],
    fileimg: [],
    date: moment().format("YYYY-MM-DD"),
    ib1:'',
    ib2:'',
    type:'',
    checked:false,
    asCode:'',
    Model:'',
    serial:'',
    Firmware:'',
    local:'',
    ip:'',
    num:0,
    status:'',
    datewaran: moment().format("YYYY-MM-DD"),
    remark:'',
    waranyear: "3",
    gennumber:'',
  };

  gennumber = async () => {
    let data =   await Axios.post(REQUEST_URL + "/camera/gencamera", {}, {        
        headers: { authorization: Cookies.get("webcomputer") }
      })
      .then( async res => {
        return res.data.data
      })
      .catch(err => {
        console.log(err);
      });
      return data
    }
  
  CameraAdd = async () => {
    await Axios.post(REQUEST_URL + '/camera/camerainsert' , {
          cctvID : this.props.ib1,
          status : this.state.status,
          asCode : this.state.asCode,
          model : this.state.Model,
          serialNo : this.state.serial,
          firmware : this.state.Firmware,
          type : this.state.type,
          ip : this.state.ip,
          location : this.state.local,
          startWaranty : this.state.datewaran,
          waranty : this.state.waranyear,
          picname : this.state.fileList.length === 0 ? '' : this.state.gennumber + '.jpg',
          remark : this.state.remark,
          notfity : this.state.checked,
          startdate : this.state.date,
          price : this.state.num,
          preby : sessionStorage.getItem('user_id')
    } ,{
      headers: { authorization: Cookies.get("webcomputer") }
    })
    .then(res => {
      if(res.data.status){
        Swal.fire({
          type: 'success',
          title: 'เสร็จสิ้น !',
          text: 'การบันทึกข้อมูลเสร็จสิ้น !',
          confirmButtonText: 'ตกลง',
        })
      }
    })
    .catch(err => {
      console.log(err);
    });
  }

  save = async () => {
    let num = await this.gennumber()
    this.setState({gennumber:num})
    if(
      this.state.ip === '' ||
      this.state.type === '' ||
      this.state.Model === '' ||
      this.state.local === '' ||
      this.state.serial === '' ||
      this.state.asCode === '' ||
      this.state.status === '' ||
      this.state.Firmware === '' ||
      this.state.waranyear === '' ||
      this.props.ib1 === '' ||
      this.props.ib2 === '' 
    ){
      Swal.fire({
        type: 'error',
        title: 'ข้อผิดพลาด !',
        text: 'กรุณากรอกข้อมูลครบถ้วน!',
        confirmButtonText: 'ตกลง',
      })
    }else{
    if(this.state.fileList.length === 0){
     await this.CameraAdd()
      this.props.refresh()
      this.props.close()
    }else{
      const data = new FormData();
      data.append("file", this.state.fileimg,this.state.gennumber);
      await Axios.post(REQUEST_URL + "/camera/cameraimg", data , {
        
        headers: { authorization: Cookies.get("webcomputer") }
      })
      .then( async res => {
        // console.log(res)
        if(res.data.status){
          await this.CameraAdd()
        this.props.refresh()
          this.props.close()     
        }else{
          Swal.fire({
            type: 'error',
            title: 'ข้อผิดพลาด !',
            text: 'ไม่สามารถบันทึกไฟล์รูปภาพได้!',
            confirmButtonText: 'ตกลง',
          })
        }
      })
      .catch(err => {
        console.log(err);
      });
    }
    }
  }



  handleChange = e => {
    // console.log(e)
    this.setState({
      fileList: e.fileList,
      fileimg: e.file.originFileObj === undefined ? [] : e.file.originFileObj
    });
  };

  onChange = (e) => {
    // console.log(  [e.target.id]  + ':' +e.target.value)
    this.setState({
      [e.target.id] :e.target.value
    })
  }

  onChangenum = (e,s) => {
    // console.log( e,s)
    if(s == 'num'){
      this.setState({num:e})
    }else if (s == 'waranyear'){
      this.setState({waranyear:e})
    }else{
      this.setState({checked:!this.state.checked})
    }
  }

  open = () => {
    this.props.setModal(true)
    this.props.bg(true)
  }

  render() {
    return (
      <div>
        <Row>
          <Col xs={8}>
            <Card>
              <CardBody>
                <FormGroup row>
                  <Col xs={6}>
                    <span>วันที่เริ่มใช้งาน : </span>
                    <br />
                    <DatePicker
                      defaultValue={moment(
                        moment().format("DD/MM/YYYY"),
                        "DD/MM/YYYY"
                      )}
                      format={"DD/MM/YYYY"}
                      onChange={e =>
                        this.setState({
                          date:
                            e == null ? "" : moment(e._d).format("YYYY-MM-DD")
                        })
                      }
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>อยู่ในกล่อง : </span>
                    <br />
                    <InputGroup compact>
                      <Search
                        style={{ width: "30%" }}
                        value={this.props.ib1}
                        onSearch={this.open}
                        enterButton
                      />
                      <Input
                        style={{ width: "70%" }}
                        value={this.props.ib2}
                        
                      />
                    </InputGroup>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={8}>
                    <span>ประเภท : </span>
                    <br />
                    <Radio.Group                     
                      onChange={this.onChange}
                      value={this.state.type}
                    >
                      <Radio value={0} id='type'>Dome</Radio>
                      <Radio value={1} id='type'>Bullet</Radio>
                      <Radio value={2} id='type'>Other</Radio>
                    </Radio.Group>
                  </Col>
                  <Col xs={4}>
                    <span>แจ้งเตือน : </span>
                    <br />
                    <Checkbox
                    checked={this.state.checked}
                    onChange={this.onChangenum}
                    >                      
                      <label style={{ color: "red" }}>
                        แจ้งเตือนเมื่อกล้องดับ
                      </label>
                    </Checkbox>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>รหัสทรัพย์สิน :</span>
                    <Input
                      placeholder="รหัสทรัพย์สิน"
                      id="asCode"
                      value={this.state.asCode}
                      onChange={this.onChange}
                      autoComplete={"off"}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>Model :</span>
                    <Input
                      placeholder="Model"
                      id="Model"
                      value={this.state.Model}
                      onChange={this.onChange}
                      autoComplete={"off"}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>Serial No. :</span>
                    <Input
                      placeholder="Serial No."
                      id="serial"
                      value={this.state.serial}
                      onChange={this.onChange}
                      autoComplete={"off"}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>Firmware Version :</span>
                    <Input
                      placeholder="Firmware Version"
                      id="Firmware"
                      value={this.state.Firmware}
                      onChange={this.onChange}
                      autoComplete={"off"}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>สถานที่ :</span>
                    <Input
                      placeholder="สถานที่"
                      id="local"
                      value={this.state.local}
                      onChange={this.onChange}
                      autoComplete={"off"}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={6}>
                    <span>IP Address :</span>
                    <Input
                      placeholder="IP Address"
                      id="ip"
                      value={this.state.ip}
                      onChange={this.onChange}
                      autoComplete={"off"}
                    />
                  </Col>
                  <Col xs={6}>
                    <span>ราคา (บาท) :</span>
                    <br />
                    <InputNumber
                    onChange={(e)=>this.onChangenum(e,'num')}      
                      value={this.state.num}
                      style={{ width: "50%" }}
                      defaultValue={0.0}
                      formatter={value =>
                        `$ ${value}`.replace(/\B(?=(\d{3})+(?!\d))/g, ",")
                      }
                      parser={value => value.replace(/\$\s?|(,*)/g, "")}
                    />
                  </Col>
                </FormGroup>

                {/* <FormGroup row>
                  <Col xs={12}>
                    <span>หมายเหตุ :</span>
                    <TextArea placeholder="หมายเหตุ" allowClear id='remark' value={this.state.remark} />
                  </Col>                
                </FormGroup> */}
              </CardBody>
            </Card>
          </Col>
          <Col xs={4}>
            <Card>
              <CardBody>
                <FormGroup row>
                  <Upload
                    className="uploadnone"
                    action={REQUEST_URL + "/api"}
                    accept="image/,.JPG"
                    listType="picture-card"
                    fileList={this.state.fileList}
                    onChange={this.handleChange}
                    onRemove={() => this.setState({ gennum: "" })}
                    disabled={false}
                  >
                    {this.state.fileList.length >= 1 ? null : (
                      <div>
                        <Icon type="plus" />
                        <div className="ant-upload-text">อัพโหลดรูปภาพ</div>
                      </div>
                    )}
                  </Upload>
                </FormGroup>
              </CardBody>
            </Card>
            <Card>
              <CardBody>
                <FormGroup row>
                  <Col xs={12}>
                    <span>วันที่เริ่มรับประกัน : </span>
                    <br />
                    <InputGroup compact>
                      <DatePicker
                        defaultValue={moment(
                          moment().format("DD/MM/YYYY"),
                          "DD/MM/YYYY"
                        )}
                        format={"DD/MM/YYYY"}
                        onChange={e =>
                          this.setState({
                            datewaran:
                              e == null ? "" : moment(e._d).format("YYYY-MM-DD")
                          })
                        }
                      />
                      <Select
                        onChange={(e)=>this.onChangenum(e,'waranyear')}                        
                        value={this.state.waranyear}
                      >
                        <Option value="1">1 ปี</Option>
                        <Option value="2">2 ปี</Option>
                        <Option value="3">3 ปี</Option>
                        <Option value="4">4 ปี</Option>
                        <Option value="5">5 ปี</Option>
                      </Select>
                    </InputGroup>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>สถานะ : </span>
                    <br />
                    <Radio.Group
                      onChange={this.onChange}
                      value={this.state.status}
                    >
                      <Radio value={0} id='status'>ใช้งานอยู๋</Radio>
                      <Radio value={1} id='status'>ชำรุด</Radio>
                      <Radio value={2} id='status'>ไม่ใช้งาน</Radio>
                    </Radio.Group>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>หมายเหตุ :</span>
                    <TextArea
                      placeholder="หมายเหตุ"
                      allowClear
                      id="remark"
                      value={this.state.remark}
                      onChange={this.onChange}
                    />
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <hr />
        <Row
          style={{
            marginRight: "3%",
            display: "flex",
            justifyContent: "flex-end"
          }}
        >
          <Col xs={1}>
            <Button color="primary" onClick={this.save}>
              บันทึก
            </Button>
          </Col>
          <Col xs={1}>
            <Button color="danger" onClick={this.props.close}>
              ยกเลิก
            </Button>
          </Col>
        </Row>
      </div>
    );
  }
}
export default Camadd;
