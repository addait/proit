import React from "react";
import {Tooltip} from 'antd'
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import {  Modal, ModalHeader, ModalBody , Button } from "reactstrap";
import ToolkitProvider, { Search } from 'react-bootstrap-table2-toolkit';


const { SearchBar } = Search;

class CamModal extends React.Component {
       

    state={
        columns:[ {
            dataField: "cctvID",
            text: "รหัสกล่อง",
            sort: true,
            align: "center",
            headerAlign: "center"
          },{
            dataField: "model",
            text: "Model",
            sort: true,
            align: "center",
            headerAlign: "center"
          },{
            dataField: "serialNo",
            text: "Serial No.",
            sort: true,
            headerAlign: "center",
            formatter: (cell, row) => { return ( 
                <Tooltip placement="top" title={row.serialNo}>         
                  <div style={{
                whiteSpace: 'nowrap',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                // width:'100px',
                cursor:'pointer'
              }}>{row.serialNo}</div>
              </Tooltip>
              
              )
               },
          },{
            dataField: "ip",
            text: "IP Address",
            sort: true,
            align: "center",
            headerAlign: "center"
          },{
            dataField: "name",
            text: "Name Device",
            sort: true,
            headerAlign: "center",
            formatter: (cell, row) => { return ( 
                <Tooltip placement="top" title={row.name}>         
                  <div style={{
                whiteSpace: 'nowrap',
                overflow: 'hidden',
                textOverflow: 'ellipsis',
                // width:'100px',
                cursor:'pointer'
              }}>{row.name}</div>
              </Tooltip>
              
              )
               },
          },
          {
            text: "เลือก",
            align: "center",
            headerAlign: "center",
            formatter: (cell, row) =>{
                return(
                    <Button color="warning" onClick={this.setText(row.cctvID,row.model)}>เลือก</Button>
                )
            } 
          },
        ]
    }

    setText = (a,b) => e =>{
        this.props.text(a,b)
        this.close()
    }

    close = () => {
        this.props.setModal(false);
        this.props.bg(false);
      };

  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.show}
          toggle={this.close}
          style={ { maxWidth: "1024px" }}
        >
          <ModalHeader toggle={this.close}>
           ค้นหากล่อง
          </ModalHeader>
          <ModalBody>
          <ToolkitProvider
  keyField="cctvID"
  data={ this.props.data }
  columns={ this.state.columns }
  search
>
  {
    props => (
      <div>
        <SearchBar { ...props.searchProps }  placeholder="ค้นหา"/>
        <hr />
        <BootstrapTable
          { ...props.baseProps }
          pagination={ paginationFactory() }
        />
      </div>
    )
  }
</ToolkitProvider>
          </ModalBody>
        </Modal>
      </div>
    );
  }
}
export default CamModal;
