import React from "react";
import { Card, CardBody, Col, Row, FormGroup , Button } from "reactstrap";
import { Input, Upload, Icon, Radio, DatePicker, message } from "antd";
import { REQUEST_URL } from "../../config";
import moment from "moment";
import "./Camera.css";
import "../../assets/img/brand/IT.png";
import Axios from "axios";
import Swal from 'sweetalert2'
import Cookies from "js-cookie";



class CMadd extends React.Component {


  state = {
    fileList: [],
  fileimg:[],
    boxcode: "",
    date: moment().format('YYYY-MM-DD'),
    model: "",
    name: "",
    serial: "",
    Firmware: "",
    ip: "",
    waranty: "",
    remark: "",
    status: "",
    picname: "",
    gennum: "",
    chck_update:false,
    
  };

  onChangeRadio = e => {
    this.setState({
      status: e.target.value
    });
  };

  onChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  handleChange = e => {
    this.setState({ fileList: e.fileList ,fileimg: e.file.originFileObj === undefined ? [] : e.file.originFileObj  });

  };

  update = async () => {   
    await Axios.post(REQUEST_URL + "/camera/cctvinsert",  {
      status:this.state.status,
      model:this.state.model,
      serialNo:this.state.serial,
      firmware:this.state.Firmware,
      name:this.state.name,
      ip:this.state.ip,
      startdate:this.state.date,
      waranty:this.state.waranty,
      remark:this.state.remark,
      picname:this.state.fileimg === '' || this.state.fileimg === null  ? this.state.picname : this.state.fileimg === undefined || this.state.fileimg.length === 0  ? '' :this.state.gennum ,
      preby:sessionStorage.getItem('user_id')
    } , {
      headers: { authorization: Cookies.get("webcomputer") }
    })
      .then(res => {
        if(res.data.status){
          Swal.fire({
            type: 'success',
            title: 'เสร็จสิ้น !',
            text: 'การบันทึกข้อมูลเสร็จสิ้น !',
            confirmButtonText: 'ตกลง',
          })
        }
      })
      .catch(err => {
        console.log(err);
      });
  }

  gennumber = async () => {
  let data =   await Axios.post(REQUEST_URL + "/camera/gencctv", {}, {        
      headers: { authorization: Cookies.get("webcomputer") }
    })
    .then( async res => {
      return res.data.data
    })
    .catch(err => {
      console.log(err);
    });
    return data
  }

  

  save = async () => {
    let num = await this.gennumber()
    this.setState({gennum:num})
    if(
      this.state.model === '' ||
      this.state.name === '' ||
      this.state.serial === '' ||
      this.state.Firmware === '' ||
      this.state.ip === '' ||
      this.state.status === '' ||
      this.state.date === ''
    ){
      Swal.fire({
        type: 'error',
        title: 'ข้อผิดพลาด !',
        text: 'กรุณากรอกข้อมูลครบถ้วน!',
        confirmButtonText: 'ตกลง',
      })
    }else{
      if (this.state.fileimg.length === 0 ){
        await this.update()
        this.props.refresh()
        this.props.close()
      }else{
        // console.log(this.state.fileimg)
      const data = new FormData();
      data.append("file", this.state.fileimg,this.state.gennum);
      await Axios.post(REQUEST_URL + "/camera/cctvimg", data , {
        
        headers: { authorization: Cookies.get("webcomputer") }
      })
      .then( async res => {
        // console.log(res)
        if(res.data.status){
          await this.update()
        this.props.refresh()
          this.props.close()     
        }else{
          Swal.fire({
            type: 'error',
            title: 'ข้อผิดพลาด !',
            text: 'ไม่สามารถบันทึกไฟล์รูปภาพได้!',
            confirmButtonText: 'ตกลง',
          })
        }
      })
      .catch(err => {
        console.log(err);
      });
    }
     
    }
    
    //   if (this.state.fileimg.length === 0 ){
    //     await this.update()
    //   }else{
    //     const data = new FormData();
    //     data.append("file", this.state.fileimg,this.state.boxcode);
    //     await Axios.post(REQUEST_URL + "/camera/cctvimg",   data , {
          
    //       headers: { authorization: Cookies.get("webcomputer") }
    //     })
    //     .then( async res => {
    //       if(res.data.status){
    //         await this.update()
    //         this.setState({fileList:[]})
    //         window.location.reload(true)
    //       }
    //     })
    //     .catch(err => {
    //       console.log(err);
    //     });
    //   }
    // }
    };
    
    render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs={8}>
            <Card>
              <CardBody>
                <FormGroup row>                
                  <Col xs={6}>
                    <span>วันที่เริ่ม : </span>
                    <br />
                    <DatePicker
                      defaultValue={moment(
                        moment().format(
                          "DD/MM/YYYY"
                        ),
                        "DD/MM/YYYY"
                      )}
                      format={"DD/MM/YYYY"}
                      onChange={(e)=>
                        this.setState({
                        date:e == null ? '':moment(e._d).format('YYYY-MM-DD')
                      })
                    }
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={6}>
                    <span>Model :</span>
                    <Input
                      placeholder="Model"
                      id="model"
                      value={this.state.model}
                      onChange={this.onChange}
                      autoComplete={'off'}
                    />
                  </Col>
                  <Col xs={6}>
                    <span>Name Device :</span>
                    <Input
                      placeholder="Name Device"
                      id="name"
                      value={this.state.name}
                      onChange={this.onChange}
                      autoComplete={'off'}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>Serial No. :</span>
                    <Input
                      placeholder="Serial No."
                      id="serial"
                      value={this.state.serial}
                      onChange={this.onChange}
                      autoComplete={'off'}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>Firmware Ver.</span>
                    <Input
                      placeholder="Firmware Ver."
                      id="Firmware"
                      value={this.state.Firmware}
                      onChange={this.onChange}
                      autoComplete={'off'}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={6}>
                    <span>IP Adress :</span>
                    <Input
                      placeholder="IP Adress"
                      id="ip"
                      value={this.state.ip}
                      onChange={this.onChange}
                      autoComplete={'off'}
                    />
                  </Col>
                  <Col xs={6}>
                    <span>Waranty :</span>
                    <Input
                      placeholder="Waranty"
                      id="waranty"
                      value={
                        this.state.waranty === null ? "-" : this.state.waranty
                      }
                      onChange={this.onChange}
                      autoComplete={'off'}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>หมายเหตุ :</span>
                    <Input
                      placeholder="หมายเหตุ"
                      id="remark"
                      value={
                        this.state.remark === null ? "-" : this.state.remark
                      }
                      onChange={this.onChange}
                      autoComplete={'off'}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <Radio.Group
                      value={parseInt(this.state.status)}
                      onChange={this.onChangeRadio}
                    >
                      <Radio value={0}>ใช้งานอยู่</Radio>
                      <Radio value={1}>ชำรุด</Radio>
                      <Radio value={2}>ไม่ใช้งาน</Radio>
                    </Radio.Group>
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
          <Col xs={4}>
            <Card>
              <CardBody>
                <Upload
                  className="uploadnone"
                  action={REQUEST_URL + "/api"}
                  accept='image/,.JPG'
                  listType="picture-card"
                  fileList={this.state.fileList}
                  onChange={this.handleChange}
                  onRemove={()=>this.setState({gennum:''})}
                  disabled={false}
                >
                  
                  {this.state.fileList.length >= 1 ? null : (
                    <div>
                      <Icon type="plus" />
                      <div className="ant-upload-text">อัพโหลดรูปภาพ</div>
                    </div>
                  )}
                </Upload>  
                            
              </CardBody>
            </Card>
          </Col>
        </Row>
        <hr />
        <Row style={{marginRight:'3%' , display:'flex' , justifyContent:'flex-end'}}>
        <Col xs={1} >
          <Button color="primary" onClick={this.save}>บันทึก</Button>
        </Col>
        <Col xs={1} >
          <Button color="danger" onClick={this.props.close}>ยกเลิก</Button>
        </Col>
        </Row>
      </div>
    );
  }
}
export default CMadd;
