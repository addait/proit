import React from "react";
import {
  Card,
  CardBody,
  Col,
  Row,
  Button,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon,
} from "reactstrap";
import Spinner from "react-spinkit";
import Axios from "axios";
import Cookies from "js-cookie";
import { REQUEST_URL } from "../../config";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import moment from "moment";
import Swal from "sweetalert2";
import "./Camera.css";
import { Tooltip } from 'antd';
import 'antd/dist/antd.css'
import { FaPlus, FaPencilAlt, FaSearchPlus, FaSyncAlt, FaTimes } from "react-icons/fa";
import CameraModal from './CameraModal'
import { async } from "q";

const { SearchBar } = Search;
const options = {
    sizePerPageList: [ {
        text: '25', value: 25
      }, {
        text: '50', value: 50
      }, ]
  };

class CCTV extends React.Component {
  componentDidMount() {
    this.showdata();
  }

  state = {
    products: [],
    columns: [
      {
        dataField: "status",
        text: "สถานะ",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) => {
          return row.status === null || row.status == 0 ? (
            <i  className="fa fa-check-circle " style={{color:'green'}}></i>
          ) : (
            <i className="fa fa-times-circle " style={{color:'red'}}></i>
          );
        },
        style:{width:'50px'},
      },
      {
        dataField: "cctvID",
        text: "รหัสกล่อง",
        sort: true,
        align: "center",
        headerAlign: "center"
      },
      {
        dataField: "startdate",
        text: "วันที่เริ่มใช้งาน",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) => moment(row.startdate).format("DD/MM/YYYY")
      },
      {
        dataField: "model",
        text: "Model",
        sort: true,
        align: "center",
        headerAlign: "center"
      },
      {
        dataField: "serialNo",
        text: "Serial No.",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) => { return ( 
            <Tooltip placement="top" title={row.serialNo}>         
              <div style={{
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            // width:'100px',
            cursor:'pointer'
          }}>{row.serialNo}</div>
          </Tooltip>
          
          )
           },
      },
      {
        dataField: "firmware",
        text: "Firmware Version",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) => { return ( 
            <Tooltip placement="top" title={row.firmware}>         
              <div style={{
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            // width:'85px',
            cursor:'pointer'
          }}>{row.firmware}</div>
          </Tooltip>
        // row.firmware
          )
           },
        
      },
      {
        dataField: "ip",
        text: "IP Address",
        sort: true,
        align: "center",
        headerAlign: "center"
      },
      {
        dataField: "name",
        text: "Name Device",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) => { return ( 
            <Tooltip placement="top" title={row.name}>         
              <div style={{
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            // width:'100px',
            cursor:'pointer'
          }}>{row.name}</div>
          </Tooltip>
          
          )
           },
      },
      {
        dataField: "waranty",
        text: "Waranty",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) => {
          return row.waranty === null || row.waranty === "" ? "-" : row.waranty;
        }
      },
      {
        dataField: "predate",
        text: "วันที่บันทึก",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) =>
          moment(row.predate).format("DD/MM/YYYY")
      },
      {
        dataField: "preby",
        text: "ผู้บันทึก",
        sort: true,
        align: "center",
        headerAlign: "center"
      },
      {
        dataField: "lastdate",
        text: "วันที่แก้ไข",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) => {
          return row.lastdate === null
            ? "-"
            : moment(row.lastdate).format("DD/MM/YYYY");
        }
      },
      {
        dataField: "lastby",
        text: "ผู้แก้ไข",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) => {
          return row.lastby === null ? "-" : row.lastby;
        }
      }
    ],
    selectRow: {
      mode: "radio",
      clickToSelect: true,
      hideSelectColumn: true,
      bgColor: "#A3C68E",
      onSelect: (row, isSelect, rowIndex, e) => {
        this.setState({boxcode:row.cctvID})
      },
    },
    boxcode:'',
    boxtext:'',
    showModal:false,
    Modalboxcode:[]
  };

  setModal = (bool) => {
    this.setState({
      showModal:bool
    })
  }

  adddata = async () => {
    this.setState({ boxtext:2 });  
    this.setModal(true)
  }

  editdata = async () => {
    if(this.state.boxcode === ''){
      Swal.fire({
        type: 'error',
        title: 'เกิดข้อผิดพลาด !!',
        text: 'กรุณาเลือก รหัสกล่อง ก่อน !!',
        confirmButtonText:'ตกลง',
      })
    }else{
      await Axios.post(
        REQUEST_URL + "/camera/cctv",
        {cctv:this.state.boxcode},
        {
          headers: { authorization: Cookies.get("webcomputer") }
        }
      )
        .then(res => {
          this.setState({ Modalboxcode: res.data.data , boxtext:0 });  
          this.setModal(true)
  
        })
        .catch(err => {
          console.log(err);
        });
    }

  }

  viewdata = async () => {
    if(this.state.boxcode === ''){
      Swal.fire({
        type: 'error',
        title: 'เกิดข้อผิดพลาด !!',
        text: 'กรุณาเลือก รหัสกล่อง ก่อน !!',
        confirmButtonText:'ตกลง',
      })
    }else{ 
      await Axios.post(
        REQUEST_URL + "/camera/cctv",
        {cctv:this.state.boxcode},
        {
          headers: { authorization: Cookies.get("webcomputer") }
        }
      )
        .then(res => {
          this.setState({ Modalboxcode: res.data.data , boxtext:1 });  
          this.setModal(true)
  
        })
        .catch(err => {
          console.log(err);
        });
    }
  }

  deleteimg = async () => {

    await Axios.post(REQUEST_URL + "/camera/cctvimgdelete", {
      id:this.state.boxcode
    } , {
          
      headers: { authorization: Cookies.get("webcomputer") }
    })
    .then( async res => {
    })
    .catch(err => {
      console.log(err);
    });

  }


  deletedata = async () => {
    if(this.state.boxcode === ''){
      Swal.fire({
        type: 'error',
        title: 'เกิดข้อผิดพลาด !!',
        text: 'กรุณาเลือก รหัสกล่อง ก่อน !!',
        confirmButtonText:'ตกลง',
      })
    }else{
      Swal.fire({
        title: 'ต้องการลบข้อมูลนี้หรือไม่ ?',
        text: "ลบรายการ " + this.state.boxcode + ' !!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก'
      }).then(async (result) => {
        if (result.value) {
          await this.deleteimg()
          await Axios.post(
            REQUEST_URL + "/camera/cctvdelete",
            {id:this.state.boxcode},
            {
              headers: { authorization: Cookies.get("webcomputer") }
            }
          )
            .then( async res => {
              // console.log(res)
              if(res.data.status){
                 
                this.showdata();
                Swal.fire({
                  type: 'success',
                  title: 'เสร็จสิ้น !',
                  text: 'การลบรายการนี้สำเร็จ !!',
                  confirmButtonText:'ตกลง',
                })
          }
      
            })
            .catch(err => {
              console.log(err);
            });
        }
      })
    }
  }

  Refreshdata = async () => {
    await Axios.post(
      REQUEST_URL + "/camera/cctv",
      {},
      {
        headers: { authorization: Cookies.get("webcomputer") }
      }
    )
      .then(res => {
        if(res.data.data.length > 0){
          this.setState({ products: res.data.data ,boxcode:'' });
          Swal.fire({
            position: 'center',
            type: 'success',
            title: 'ฟื้นฟูเสร็จสิ้น !!',
            showConfirmButton: false,
            timer: 1500
          })
        }

      })
      .catch(err => {
        console.log(err);
      });
  };

  showdata = async () => {
    await Axios.post(
      REQUEST_URL + "/camera/cctv",
      {cctv:'%%'},
      {
        headers: { authorization: Cookies.get("webcomputer") }
      }
    )
      .then(res => {
        this.setState({ products: res.data.data });

      })
      .catch(err => {
        console.log(err);
      });
  };
  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs={12}>
            <Card>
              <CardBody>
                <ToolkitProvider
                  keyField="cctvID"
                  data={this.state.products}
                  columns={this.state.columns}
                  search
                >
                  {props => (
                    <div>
                      <FormGroup row>
                        <Col md="2">
                          <InputGroup>
                            <SearchBar
                              {...props.searchProps}
                              placeholder="ค้นหา"
                              autoComplete="off"
                            />
                          </InputGroup>
                        </Col>
                        <Col
                          md="4"
                          style={{
                            display: "flex",
                            justifyContent: "space-between"
                          }}
                        >
                          <Button color="success" onClick={this.adddata}> <FaPlus />{' '}เพิ่มข้อมูล</Button>
                          <Button color="warning" onClick={this.editdata}><FaPencilAlt />{' '}แก้ไขข้อมูล</Button>
                          <Button color="info" onClick={this.viewdata}><FaSearchPlus />{' '}มุมมอง</Button>
                          {/* <Button color="primary" onClick={this.Refreshdata}><FaSyncAlt />{' '}ฟื้นฟู</Button> */}
                          <Button color="danger" onClick={this.deletedata}><FaTimes />{' '}ลบข้อมูล</Button>
                        </Col>
                      </FormGroup>
                      <FormGroup row style={{ padding: "15px" }}>
                        <BootstrapTable
                          {...props.baseProps}
                          pagination={paginationFactory(options)}
                          selectRow={this.state.selectRow}
                          noDataIndication={"ไม่พบรายการ"}
                        />
                      </FormGroup>
                    </div>
                  )}
                </ToolkitProvider>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <CameraModal setModal={this.setModal} show={this.state.showModal} text={this.state.boxtext} data={this.state.Modalboxcode} refresh={this.showdata}/>
      </div>
    );
  }
}
export default CCTV;
