import React from "react";
import { Card, CardBody, Col, Row, FormGroup , Button } from "reactstrap";
import { Input, Upload, Icon, Radio, DatePicker, message } from "antd";
import { REQUEST_URL } from "../../config";
import moment from "moment";
import "./Camera.css";
import "../../assets/img/brand/IT.png";
import Axios from "axios";
import Swal from 'sweetalert2'
import Cookies from "js-cookie";
import {Apicctv} from './Camconfig'



class CMedit extends React.Component {
  componentDidMount() {
    this.showdata();
  }

  state = {
    fileList: [],
  fileimg:[],
    boxcode: "",
    date: "",
    model: "",
    name: "",
    serial: "",
    Firmware: "",
    ip: "",
    waranty: "",
    remark: "",
    status: "",
    picname: "",
    chck_update:false,
    
  };

  showdata = () => {
    return this.props.data.map((row, index) => {
      this.setState({
        boxcode: row.cctvID,
        date: moment(row.startdate).format("YYYY-MM-DD"),
        model: row.model,
        name: row.name,
        serial: row.serialNo,
        Firmware: row.firmware,
        ip: row.ip,
        waranty: row.waranty,
        remark: row.remark,
        status: row.status,
        picname: row.picname,
        fileList:row.picname === null || row.picname === '' ? [] :
        
        [
          {
            uid: "-1",
            name:  row.picname + ".jpg",
            status: "done",
            url: Apicctv + row.picname + `.jpg`
          }
        ],
      });
    });
  };

  onChangeRadio = e => {
    this.setState({
      status: e.target.value
    });
  };

  onChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  handleChange = e => {
    this.setState({ fileList: e.fileList ,fileimg: e.file.originFileObj === undefined ? [] : e.file.originFileObj  });
    // console.log(e.file.originFileObj);
  };

  update = async (img) => {
    // console.log(this.state.fileimg )
    await Axios.post(REQUEST_URL + "/camera/cctvupdate",  {
      status:this.state.status,
      model:this.state.model,
      serialNo:this.state.serial,
      firmware:this.state.Firmware,
      ip:this.state.ip,
      startdate:this.state.date,
      waranty:this.state.waranty,
      remark:this.state.remark,
      picname:img === 'no data'  ? this.state.picname : img ,
      cctvID:this.state.boxcode,
      lastby:sessionStorage.getItem('user_id')
    } , {

      headers: { authorization: Cookies.get("webcomputer") }
    })
      .then(res => {
        if(res.data.status){
         
          this.props.refresh()
          this.props.close()
          // this.setState({chck_update:true})
        }
        // this.setState({chck_update:true})
        // console.log(res.data.status)
      })
      .catch(err => {
        console.log(err);
      });
  }

  deleteimg = async () => {
    await Axios.post(REQUEST_URL + "/camera/cctvimgdelete", {
      id:this.state.boxcode
    } , {
          
      headers: { authorization: Cookies.get("webcomputer") }
    })
    .then( async res => {
      console.log(res)
    })
    .catch(err => {
      console.log(err);
    });
  }

  save = async () => {
    if(this.state.fileimg === undefined){
      Swal.fire({
        type: 'error',
        title: 'ข้อผิดพลาด !',
        text: 'ไม่สามารถลบรูปภาพได้ กรุณาเพิ่มรูปภาพ',
        confirmButtonText:'ตกลง',
      })
    }else{
      if (this.state.fileimg.length === 0 ){
        await this.update('no data')
      }else{
        // console.log(moment().format('DDMMYYYYHHmmss'))
        const data = new FormData();
        data.append("file", this.state.fileimg,moment().format('DDMMYYYYHHmmss') + this.state.boxcode);
        await Axios.post(REQUEST_URL + "/camera/cctvimg",   data , {
          
          headers: { authorization: Cookies.get("webcomputer") }
        })
        .then( async res => {
          if(res.data.status){
            // console.log('$$$$$$$$$$$$$$$$$$$')
             this.deleteimg()
            // console.log('###################')
            await this.update(res.data.name)
            this.showdata()
            // this.setState({fileList:[]})
            // window.location.reload(true)
          }
        })
        .catch(err => {
          console.log(err);
        });
      }
    }
    };
    
    render() {
      // console.log(this.state.fileList)
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs={8}>
            <Card>
              <CardBody>
                <FormGroup row>
                  <Col xs={6}>
                    <span>รหัสกล่อง :</span>
                    <Input
                      placeholder="รหัสกล่อง"
                      id="boxcode"
                      value={this.state.boxcode}
                      disabled
                    />
                  </Col>
                  <Col xs={6}>
                    <span>วันที่เริ่ม : </span>
                    <br />
                    <DatePicker
                      defaultValue={moment(
                        moment(this.props.data[0].startdate).format(
                          "DD/MM/YYYY"
                        ),
                        "DD/MM/YYYY"
                      )}
                      format={"DD/MM/YYYY"}
                      onChange={(e)=>
                        // console.log(e)
                        this.setState({
                        date:e == null ? '':moment(e._d).format('YYYY-MM-DD')
                      })
                    }
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={6}>
                    <span>Model :</span>
                    <Input
                      placeholder="Model"
                      id="model"
                      value={this.state.model}
                      onChange={this.onChange}
                    />
                  </Col>
                  <Col xs={6}>
                    <span>Name Device :</span>
                    <Input
                      placeholder="Name Device"
                      id="name"
                      value={this.state.name}
                      onChange={this.onChange}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>Serial No. :</span>
                    <Input
                      placeholder="Serial No."
                      id="serial"
                      value={this.state.serial}
                      onChange={this.onChange}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>Firmware Ver.</span>
                    <Input
                      placeholder="Firmware Ver."
                      id="Firmware"
                      value={this.state.Firmware}
                      onChange={this.onChange}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={6}>
                    <span>IP Adress :</span>
                    <Input
                      placeholder="IP Adress"
                      id="ip"
                      value={this.state.ip}
                      onChange={this.onChange}
                    />
                  </Col>
                  <Col xs={6}>
                    <span>Waranty :</span>
                    <Input
                      placeholder="Waranty"
                      id="waranty"
                      value={
                        this.state.waranty === null ? "-" : this.state.waranty
                      }
                      onChange={this.onChange}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>หมายเหตุ :</span>
                    <Input
                      placeholder="หมายเหตุ"
                      id="remark"
                      value={
                        this.state.remark === null ? "-" : this.state.remark
                      }
                      onChange={this.onChange}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <Radio.Group
                      value={parseInt(this.state.status)}
                      onChange={this.onChangeRadio}
                    >
                      <Radio value={0}>ใช้งานอยู่</Radio>
                      <Radio value={1}>ชำรุด</Radio>
                      <Radio value={2}>ไม่ใช้งาน</Radio>
                    </Radio.Group>
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
          <Col xs={4}>
            <Card>
              <CardBody>
                <Upload
                  className="uploadnone"
                  action={REQUEST_URL + "/api"}
                  accept='image/,.JPG'
                  listType="picture-card"
                  fileList={this.state.fileList}
                  onChange={this.handleChange}
                  disabled={false}
                  supportServerRender={true}
                  showUploadList={{showDownloadIcon :false}}

                >
                  
                  {this.state.fileList.length >= 1 ? null : (
                    <div>
                      <Icon type="plus" />
                      <div className="ant-upload-text">อัพโหลดรูปภาพ</div>
                    </div>
                  )}
                </Upload>  
                            
              </CardBody>
            </Card>
          </Col>
        </Row>
        <hr />
        <Row style={{marginRight:'3%' , display:'flex' , justifyContent:'flex-end'}}>
        <Col xs={1} >
          <Button color="primary" onClick={this.save}>บันทึก</Button>
        </Col>
        <Col xs={1} >
          <Button color="danger" onClick={this.props.close}>ยกเลิก</Button>
        </Col>
        </Row>
      </div>
    );
  }
}
export default CMedit;
