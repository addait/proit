import React from "react";
import {
  Card,
  CardBody,
  Col,
  Row,
  Button,
  FormGroup,
  InputGroup,
} from "reactstrap";
import Axios from "axios";
import Cookies from "js-cookie";
import { REQUEST_URL } from "../../config";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import BootstrapTable from "react-bootstrap-table-next";
import paginationFactory from "react-bootstrap-table2-paginator";
import ToolkitProvider, { Search } from "react-bootstrap-table2-toolkit";
import moment from "moment";
import Swal from "sweetalert2";
import { FaPlus, FaPencilAlt, FaSearchPlus, FaSyncAlt, FaTimes } from "react-icons/fa";
import "./Camera.css";
import { Tooltip } from 'antd';
import 'antd/dist/antd.css'
import CamModal from './CamModal'
import { async } from "q";

const { SearchBar } = Search;
const options = {
    sizePerPageList: [ {
        text: '25', value: 25
      }, {
        text: '50', value: 50
      }, ]
  };

class Camera extends React.Component {
  componentDidMount() {
    this.showdata();
  }

  setModal = (bool) => {
    this.setState({
      showModal:bool
    })
  }

  adddata = async () => {
    this.setState({ boxtext:2 });  
    this.setModal(true)
  }

  editdata = async () => {
    if(this.state.boxcode === ''){
      Swal.fire({
        type: 'error',
        title: 'เกิดข้อผิดพลาด !!',
        text: 'กรุณาเลือก รหัสกล่อง ก่อน !!',
        confirmButtonText:'ตกลง',
      })
    }else{
      await Axios.post(
        REQUEST_URL + "/camera/camera",
        {camera:this.state.boxcode},
        {
          headers: { authorization: Cookies.get("webcomputer") }
        }
      )
        .then(res => {
          this.setState({ Modalboxcode: res.data.data , boxtext:0 });  
          this.setModal(true)
  
        })
        .catch(err => {
          console.log(err);
        });
    }

  }

  viewdata = async () => {
    if(this.state.boxcode === ''){
      Swal.fire({
        type: 'error',
        title: 'เกิดข้อผิดพลาด !!',
        text: 'กรุณาเลือก รหัสกล่อง ก่อน !!',
        confirmButtonText:'ตกลง',
      })
    }else{ 
      await Axios.post(
        REQUEST_URL + "/camera/camera",
        {camera:this.state.boxcode},
        {
          headers: { authorization: Cookies.get("webcomputer") }
        }
      )
        .then(res => {
          // console.log(res.data)
          this.setState({ Modalboxcode: res.data.data , boxtext:1 });  
          this.setModal(true)
  
        })
        .catch(err => {
          console.log(err);
        });
    }
  }

  deleteimg = async () => {

    await Axios.post(REQUEST_URL + "/camera/cameraimgdelete", {
      id:this.state.boxcode
    } , {
          
      headers: { authorization: Cookies.get("webcomputer") }
    })
    .then( async res => {
    })
    .catch(err => {
      console.log(err);
    });

  }


  deletedata = async () => {
    if(this.state.boxcode === ''){
      Swal.fire({
        type: 'error',
        title: 'เกิดข้อผิดพลาด !!',
        text: 'กรุณาเลือก รหัสกล่อง ก่อน !!',
        confirmButtonText:'ตกลง',
      })
    }else{
      Swal.fire({
        title: 'ต้องการลบข้อมูลนี้หรือไม่ ?',
        text: "ลบรายการ " + this.state.boxcode + ' !!',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก'
      }).then(async (result) => {
        if (result.value) {
          await this.deleteimg()
          await Axios.post(
            REQUEST_URL + "/camera/cameradelete",
            {id:this.state.boxcode},
            {
              headers: { authorization: Cookies.get("webcomputer") }
            }
          )
            .then( async res => {
              // console.log(res)
              if(res.data.status){
                 
                this.showdata();
                Swal.fire({
                  type: 'success',
                  title: 'เสร็จสิ้น !',
                  text: 'การลบรายการนี้สำเร็จ !!',
                  confirmButtonText:'ตกลง',
                })
          }
      
            })
            .catch(err => {
              console.log(err);
            });
        }
      })
    }
  }

   calculate = (f,t) => {
    const fromDate = f;
    const toDate = t;
  
    try {  
      const result = this.getDateDifference(new Date(fromDate), new Date(toDate));
      if (result && !isNaN(result.years)) {
        
        return( result.years + 'ปี ' +
          result.months + 'เดือน ' +
          result.days + 'วัน') 
      }
    } catch (e) {
      console.error(e);
    }
  }
  
   getDateDifference = (startDate, endDate)  =>{
    if (startDate > endDate) {
      console.error('Start date must be before end date');
      return null;
    }
    const startYear = startDate.getFullYear();
    const startMonth = startDate.getMonth();
    const startDay = startDate.getDate();
  
    const endYear = endDate.getFullYear();
    const endMonth = endDate.getMonth();
    const endDay = endDate.getDate();
  
    // We calculate February based on end year as it might be a leep year which might influence the number of days.
    const february = (endYear % 4 == 0 && endYear % 100 != 0) || endYear % 400 == 0 ? 29 : 28;
    const daysOfMonth = [31, february, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  
    const startDateNotPassedInEndYear = (endMonth < startMonth) || endMonth == startMonth && endDay < startDay;
    const years = endYear - startYear - (startDateNotPassedInEndYear ? 1 : 0);
  
    const months = (12 + endMonth - startMonth - (endDay < startDay ? 1 : 0)) % 12;
  
    // (12 + ...) % 12 makes sure index is always between 0 and 11
    const days = startDay <= endDay ? endDay - startDay : daysOfMonth[(12 + endMonth - 1) % 12] - startDay + endDay;
  
    return {
      years: years,
      months: months,
      days: days
    };
  }

  state = {
    boxcode:'',
    showModal:false,
    products: [],
    columns: [
      {
        dataField: "status",
        text: "สถานะ",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) => {
          return row.status === null || row.status == 0 ? (
            <i i className="fa fa-check-circle " style={{color:'green'}}></i>
          ) : (
            <i className="fa fa-times-circle " style={{color:'red'}}></i>
          );
        },
        style:{width:'50px'},
      },
      {
        dataField: "cameraID",
        text: "รหัสกล้อง",
        sort: true,
        align: "center",
        headerAlign: "center"
      },
      {
        dataField: "cctvID",
        text: "อยู่ในกล่อง",
        sort: true,
        align: "center",
        headerAlign: "center"
      },
      {
        dataField: "type",
        text: "ประเภท",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) => row.type == 0 ? 'Dome' : row.type == 1 ? 'Bullet' : 'Other'
      },
      {
        dataField: "startdate",
        text: "วันที่เริ่มใช้งาน",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) => moment(row.startdate).format("DD/MM/YYYY")
      },
      {
        dataField: "asCode",
        text: "รหัสทรัพย์สิน",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) => { return ( 
          <Tooltip placement="top" title={row.asCode}>         
            <div style={{
          whiteSpace: 'nowrap',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
          cursor:'pointer'
        }}>{row.asCode}</div>
        </Tooltip>
        )
         },
      },
      {
        dataField: "model",
        text: "Model",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) => { return ( 
          <Tooltip placement="top" title={row.model}>         
            <div style={{
          whiteSpace: 'nowrap',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
          cursor:'pointer'
        }}>{row.model}</div>
        </Tooltip>
        )
         },
      },
      {
        dataField: "serialNo",
        text: "Serial No.",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) => { return ( 
            <Tooltip placement="top" title={row.serialNo}>         
              <div style={{
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            // width:'100px',
            cursor:'pointer'
          }}>{row.serialNo}</div>
          </Tooltip>
          
          )
           },
      },
      {
        dataField: "firmware",
        text: "Firmware Version",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) => { return ( 
            <Tooltip placement="top" title={row.firmware}>         
              <div style={{
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            cursor:'pointer'
          }}>{row.firmware}</div>
          </Tooltip>
          )
           },
        
      },
      {
        dataField: "ip",
        text: "IP Address",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) => { return ( 
          <Tooltip placement="top" title={row.ip}>         
            <div style={{
          whiteSpace: 'nowrap',
          overflow: 'hidden',
          textOverflow: 'ellipsis',
          cursor:'pointer'
        }}>{row.ip}</div>
        </Tooltip>
        )
         },
      },
      {
        dataField: "location",
        text: "สถานที่",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) => { return ( 
            <Tooltip placement="top" title={row.location}>         
              <div style={{
            whiteSpace: 'nowrap',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            // width:'100px',
            cursor:'pointer'
          }}>{row.location}</div>
          </Tooltip>
          
          )
           },
      },
      {
        dataField: "startWaranty",
        text: "เริ่มรับประกัน",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) =>
          moment(row.startWaranty).format("DD/MM/YYYY")
      },
      {
        dataField: "waranty",
        text: "รับประกัน (ปี)",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) => {
          return row.waranty === null || row.waranty === "" ? "-" : row.waranty;
        }
      },
      {
        text: "เหลือเวลาหมดประกัน",
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) =>{
       let a =  this.calculate(moment().format('MM/DD/YYYY'),moment(moment(row.startWaranty).add(row.waranty, 'year').calendar()).format('MM/DD/YYYY')) 
        return <span style={ a === undefined ? {fontWeight:'bold' , color:'red'}:{fontWeight:'bold' , color:'green'} } >{a === undefined ? 'หมดประกัน' : a}</span> 
        }
      },
      {
        dataField: "predate",
        text: "วันที่บันทึก",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) =>
          moment(row.predate).format("DD/MM/YYYY")
      },
      {
        dataField: "preby",
        text: "ผู้บันทึก",
        sort: true,
        align: "center",
        headerAlign: "center"
      },
      {
        dataField: "lastdate",
        text: "วันที่แก้ไข",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) => {
          return row.lastdate === null
            ? "-"
            : moment(row.lastdate).format("DD/MM/YYYY");
        }
      },
      {
        dataField: "lastby",
        text: "ผู้แก้ไข",
        sort: true,
        align: "center",
        headerAlign: "center",
        formatter: (cell, row) => {
          return row.lastby === null ? "-" : row.lastby;
        }
      }
    ],
    selectRow: {
      mode: "radio",
      clickToSelect: true,
      hideSelectColumn: true,
      bgColor: "#A3C68E",
      onSelect: (row, isSelect, rowIndex, e) => {
        this.setState({boxcode:row.cameraID})
      },
    },
  };

  showdata = async () => {
    await Axios.post(
      REQUEST_URL + "/camera/camera",
      {camera : '%%'},
      {
        headers: { authorization: Cookies.get("webcomputer") }
      }
    )
      .then(res => {
        this.setState({ products: res.data.data });
        // console.log(res.data.data)
      })
      .catch(err => {
        console.log(err);
      });
  };

 refresh = async () => {
   await this.showdata()
 }


  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs={12}>
            <Card>
              <CardBody>
                <ToolkitProvider
                  keyField="cameraID"
                  data={this.state.products}
                  columns={this.state.columns}
                  search
                >
                  {props => (
                    <div>
                      <FormGroup row>
                        <Col md="2">
                          <InputGroup>
                            <SearchBar
                              {...props.searchProps}
                              placeholder="ค้นหา"
                              autoComplete="off"
                            />
                          </InputGroup>
                        </Col>
                        <Col
                          md="4"
                          style={{
                            display: "flex",
                            justifyContent: "space-between"
                          }}
                        >
                         <Button color="success" onClick={this.adddata}> <FaPlus />{' '}เพิ่มข้อมูล</Button>
                          <Button color="warning" onClick={this.editdata}><FaPencilAlt />{' '}แก้ไขข้อมูล</Button>
                          <Button color="info" onClick={this.viewdata}><FaSearchPlus />{' '}มุมมอง</Button>
                          {/* <Button color="primary" onClick={this.Refreshdata}><FaSyncAlt />{' '}ฟื้นฟู</Button> */}
                          <Button color="danger" onClick={this.deletedata}><FaTimes />{' '}ลบข้อมูล</Button>
                        </Col>
                      </FormGroup>
                      <FormGroup row style={{ padding: "15px" }}>
                        <BootstrapTable
                          {...props.baseProps}
                          pagination={paginationFactory(options)}
                          selectRow={this.state.selectRow}
                          noDataIndication={"ไม่พบรายการ"}
                        />
                      </FormGroup>
                    </div>
                  )}
                </ToolkitProvider>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <CamModal show={this.state.showModal} setModal={this.setModal} text={this.state.boxtext} data={this.state.Modalboxcode} refresh={this.refresh}/>
      </div>
    );
  }
}
export default Camera;
