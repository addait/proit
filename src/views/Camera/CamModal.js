import React from "react";
import Camadd from "./Camadd";
import Camedit from "./Camedit";
import Camview from "./Camview";
import { Modal, ModalHeader, ModalBody } from "reactstrap";
import ModalBox from "./ModalBox";
import { REQUEST_URL } from "../../config";
import Axios from "axios";
import Cookies from "js-cookie";

class CamModal extends React.Component {
  state = {
    showbox: false,
    visible: false,
    Modalboxcode: [],
    ib1: "",
    ib2: ""
  };

  close = () => {
    this.setState({
      ib1: "",
      ib2: ""
    });
    this.props.setModal(false);
  };

  bg = bool => {
    this.setState({ visible: bool });
  };

  setModal = async bool => {
    if (bool) {
      await this.showBoxModal();
      this.setState({ showbox: bool });
    } else {
      this.setState({ showbox: bool });
    }
  };

  setText = (a, b) => {
    this.setState({ ib1: a, ib2: b });
  };

  showBoxModal = async () => {
    await Axios.post(
      REQUEST_URL + "/camera/cctv",
      { cctv: "%%" },
      {
        headers: { authorization: Cookies.get("webcomputer") }
      }
    )
      .then(res => {
        this.setState({ Modalboxcode: res.data.data });
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    return (
      <div>
        <Modal
          isOpen={this.props.show}
          toggle={this.props.text === 1 ? this.close : null}
          style={
            this.state.visible
              ? { maxWidth: "1024px", opacity: "0" }
              : { maxWidth: "1024px", opacity: "1" }
          }
        >
          <ModalHeader toggle={this.close}>
            {this.props.text === 0
              ? "แก้ไขข้อมูล"
              : this.props.text === 1
              ? "มุมมองข้อมูล"
              : "เพิ่มข้อมูล"}
          </ModalHeader>
          <ModalBody>
            {this.props.text === 0 ? (
              <Camedit
                data={this.props.data}
                close={this.close}
                refresh={this.props.refresh}
                setModal={this.setModal}
                bg={this.bg}
                ib1={this.state.ib1}
                ib2={this.state.ib2}
              />
            ) : this.props.text === 1 ? (
              <Camview data={this.props.data} close={this.close} />
            ) : (
              <Camadd
                close={this.close}
                refresh={this.props.refresh}
                setModal={this.setModal}
                bg={this.bg}
                ib1={this.state.ib1}
                ib2={this.state.ib2}
              />
            )}
          </ModalBody>
        </Modal>
        <ModalBox
          show={this.state.showbox}
          setModal={this.setModal}
          bg={this.bg}
          data={this.state.Modalboxcode}
          text={this.setText}
        />
      </div>
    );
  }
}
export default CamModal;
