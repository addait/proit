import React from "react";
import { Card, CardBody, Col, Row, FormGroup , Button } from "reactstrap";
import { Input, Upload, Icon, Radio, DatePicker } from "antd";
import moment from "moment";
import "./Camera.css";
import {Apicctv} from './Camconfig'

class CMview extends React.Component {
  componentDidMount() {
    this.showdata()
    // console.log(this.props.data)
  }

  state = {
    fileList: [],
    boxcode: "",
    date: "",
    model: "",
    name: "",
    serial: "",
    Firmware: "",
    ip: "",
    waranty: "",
    remark: "",
    status: "",
    picname: ""
  };

  showdata = () => {
   return this.props.data.map((row, index) => {
      this.setState({
        boxcode: row.cctvID,
        date: row.startdate,
        model: row.model,
        name: row.name,
        serial: row.serialNo,
        Firmware: row.firmware,
        ip: row.ip,
        waranty: row.waranty,
        remark: row.remark,
        status: row.status,
        picname: row.picname,
        fileList:row.picname === null || row.picname === '' ? [] :
        
        [
          {
            uid: "-1",
            name:  row.picname + ".jpg",
            status: "done",
            url: Apicctv + row.picname + `.jpg`
          }
        ],
      });
    });
  };

  handleChange = e => {
    this.setState({ fileList: e.fileList });
    console.log(e);
  };
  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs={8}>
            <Card>
              <CardBody>
                <FormGroup row>
                  <Col xs={6}>
                    <span>รหัสกล่อง :</span>
                    <Input placeholder="รหัสกล่อง"  value={this.state.boxcode}/>
                  </Col>
                  <Col xs={6}>
                    <span>วันที่เริ่ม : </span>
                    <br />
                    <Input placeholder="รหัสกล่อง"
                    value={moment(this.state.date).format('DD/MM/YYYY')}
                      format={"DD/MM/YYYY"}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={6}>
                    <span>Model :</span>
                    <Input placeholder="Model" value={this.state.model} />
                  </Col>
                  <Col xs={6}>
                    <span>Name Device :</span>
                    <Input placeholder="Name Device"  value={this.state.name} />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>Serial No. :</span>
                    <Input placeholder="Serial No."  value={this.state.serial}  />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>Firmware Ver.</span>
                    <Input placeholder="Firmware Ver."  value={this.state.Firmware}  />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={6}>
                    <span>IP Adress :</span>
                    <Input placeholder="IP Adress" value={this.state.ip} />
                  </Col>
                  <Col xs={6}>
                    <span>Waranty :</span>
                    <Input placeholder="Waranty" value={this.state.waranty === null || this.state.waranty === '' ? '-' : this.state.waranty} />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <span>หมายเหตุ :</span>
                    <Input placeholder="หมายเหตุ"  value={this.state.remark === null || this.state.remark === '' ? '-' : this.state.remark}/>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col xs={12}>
                    <Radio.Group value={parseInt(this.state.status)}>
                      <Radio value={0}>ใช้งานอยู่</Radio>
                      <Radio value={1}>ชำรุด</Radio>
                      <Radio value={2}>ไม่ใช้งาน</Radio>
                    </Radio.Group>
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
          <Col xs={4}>
            <Card>
              <CardBody>
                <Upload
                className='uploadview'
                  listType="picture-card"
                  fileList={this.state.fileList}
                  onChange={this.handleChange}
                  disabled={true}
                  showUploadList={{showDownloadIcon :false}}
                >
                  {this.state.fileList.length >= 1 ? null : (
                    <div>
                      <Icon type="plus" />
                      <div className="ant-upload-text">อัพโหลดรูปภาพ</div>
                    </div>
                  )}
                </Upload>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <hr />
        <Row style={{marginRight:'3%' , display:'flex' , justifyContent:'flex-end'}}>
        <Col xs={1} >
          <Button color="danger" onClick={this.props.close}>ยกเลิก</Button>
        </Col>
        </Row>
      </div>
    );
  }
}
export default CMview;
