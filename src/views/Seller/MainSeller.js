import React from "react";
import { withRouter } from "react-router-dom";
import {
  Card,
  CardBody,
  Col,
  Row,
  Button,
  FormGroup,
  Input,
  InputGroup
} from "reactstrap";
import Spinner from "react-spinkit";
import Axios from "axios";
import Cookies from "js-cookie";
import { REQUEST_URL } from "../../config";
import moment from "moment";
import Swal from "sweetalert2";
import "../../index.css";

class MainSeller extends React.Component {
  state = {
    data: [],
    filterData: [],
    modal: false,
    LoadData: false,
    inputsearch: "",
    selection: "",
    searchx: [],
    Active: 1,
    AllData: 0,
    Page: 200,
    PageCount: []
  };

  componentDidUpdate = (prevProps, prevState) => {
    let offset = (this.state.Active - 1) * this.state.Page;
    let perpage = this.state.Active * this.state.Page;
    if (prevState.Active !== this.state.Active) {
      const P = this.state.DataAll.slice(offset, perpage);
      this.setState({
        filterData: P
      });
    }
  };

  componentDidMount() {
    this.showData();
    // this.chkPermission();
  }

  chkPermission = async () => {
    await Axios.post(
      REQUEST_URL + "/users/chkpermission",
      {
        user: Cookies.get("webcomputer"),
        file: "F02"
      },
      {
        headers: { authorization: Cookies.get("webcomputer") }
      }
    ).then(res => {
      if (res.data.success) {
        this.setState({
          act_open: res.data.data[0].act_open,
          act_add: res.data.data[0].act_add,
          act_edit: res.data.data[0].act_edit,
          act_view: res.data.data[0].act_view,
          act_print: res.data.data[0].act_print
        });
      } else {
        this.props.history.push("/login");
      }
    });
  };

  showData = async () => {
    let offset = (this.state.Active - 1) * this.state.Page;
    let perpage = this.state.Active * this.state.Page;
    await Axios.post(
      REQUEST_URL + "/supplier/datasupplier",
      {},
      {
        headers: { authorization: Cookies.get("webcomputer") }
      }
    ).then(response => {
      if (response.data.status) {
        this.setState({
          DataAll: response.data.data,
          AllData: response.data.data.length
        });
      } else {
        this.props.history.push("/login");
      }
    });
    const PageCount = Math.ceil(this.state.AllData / this.state.Page);
    for (var x = 1; x < PageCount + 1; x++) {
      this.state.PageCount.push(x);
    }
    const P = this.state.DataAll.slice(offset, perpage);
    this.setState({
      filterData: P
    });
  };

  searchHandle = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
    this.searchNoButton(e.target.value);
  };

  searchNoButton = txt => {
    let ggez = txt;
    var filtered = this.state.DataAll.filter(function(x) {
      return (
        x.supplierID.match(new RegExp("^" + ggez === null ? "" : ggez, "i")) ||
        x.supplierName.match(new RegExp("^" + ggez === null ? "" : ggez, "i"))
      );
    });
    this.setState({ searchx: filtered.slice(0, 200) });
  };

  modalAdd = bool => {
    if (!bool) {
      this.showData();
    }
    this.setState({
      modaladd: bool
    });
  };

  modalEdit = bool => {
    if (!bool) {
      this.showData();
    }
    this.setState({
      modaledit: bool
    });
  };

  modalView = bool => {
    if (!bool) {
      this.showData();
    }
    this.setState({
      modalview: bool
    });
  };

  selectsupplierID = supplierid => {
    if (supplierid === this.state.selection) {
      this.setState({
        selection: ""
      });
    } else {
      this.setState({
        selection: supplierid
      });
    }
  };

  Previous = () => () => {
    if (this.state.Active !== 1) {
      this.setState({
        Active: this.state.Active - 1
      });
    }
  };

  Next = () => () => {
    if (this.state.Active < this.state.PageCount.length) {
      this.setState({
        Active: this.state.Active + 1
      });
    }
  };

  Pagination = page => () => {
    this.setState({
      Active: page
    });
  };

  render() {
    const { inputsearch } = this.state;
    return this.state.filterData.length === 0 ? (
      <div className="animated fadeIn pt-5 text-center">
        <Spinner name="three-bounce" />{" "}
      </div>
    ) : (
      <div className="animated fadeIn">
        <Row>
          <Col xs={12}>
            <Card>
              <CardBody>
                <FormGroup row>
                  <Col md="3">
                    <InputGroup>
                      <Input
                        type="text"
                        name="inputsearch"
                        values={inputsearch}
                        placeholder="ค้นหา"
                        onChange={this.searchHandle}
                      />
                    </InputGroup>
                  </Col>
                </FormGroup>
                <table>
                  <thead>
                    <tr style={{ textAlign: "center" }}>
                      <th id="tb">รหัสผู้ขาย</th>
                      <th id="tb">ชื่อ</th>
                      <th id="tb">ที่อยู่</th>
                      <th id="tb" width="15%">
                        ติดต่อ
                      </th>
                      <th id="tb" width="15%">
                        โทร
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.inputsearch === ""
                      ? this.state.filterData.map((item, index) => {
                          return (
                            <tr
                              className={
                                this.state.selection === item.supplierID
                                  ? "table-primary text-center"
                                  : ""
                              }
                              key={index}
                              onClick={() =>
                                this.selectsupplierID(item.supplierID)
                              }
                            >
                              <td id="tb" style={{ textAlign: "center" }}>
                                {item.supplierID}
                              </td>
                              <td id="tb">{item.supplierName}</td>
                              <td id="tb">{item.supaddress}</td>
                              <td id="tb" style={{ textAlign: "center" }}>
                                {item.supcontact}
                              </td>
                              <td id="tb" style={{ textAlign: "center" }}>
                                {item.tel}
                              </td>
                            </tr>
                          );
                        })
                      : this.state.searchx.map((item, index) => {
                          return (
                            <tr
                              className={
                                this.state.selection === item.supplierID
                                  ? "table-primary text-center"
                                  : ""
                              }
                              key={index}
                              onClick={() =>
                                this.selectsupplierID(item.supplierID)
                              }
                            >
                              <td id="tb" style={{ textAlign: "center" }}>
                                {item.supplierID}
                              </td>
                              <td id="tb">{item.supplierName}</td>
                              <td id="tb">{item.supaddress}</td>
                              <td id="tb" style={{ textAlign: "center" }}>
                                {item.supcontact}
                              </td>
                              <td id="tb" style={{ textAlign: "center" }}>
                                {item.tel}
                              </td>
                            </tr>
                          );
                        })}
                  </tbody>
                </table>
              </CardBody>
            </Card>
          </Col>
        </Row>
        {this.state.PageCount.length > 1 ? (
          <Row style={{ marginTop: "5%", marginBottom: "5%" }}>
            <Col md="12" sm="12" xs="12">
              <nav
                aria-label="..."
                style={{ display: "flex", justifyContent: "center" }}
              >
                <ul class="pagination">
                  <li class="page-item" onClick={this.Previous()}>
                    <a
                      href="#totop"
                      class="page-link"
                      style={{ cursor: "pointer", color: "black" }}
                    >
                      Previous
                    </a>
                  </li>

                  {this.state.PageCount.map(item => {
                    return (
                      <li class="page-item" onClick={this.Pagination(item)}>
                        <a
                          href="#totop"
                          class={
                            this.state.Active === item
                              ? "page-link activepage"
                              : "page-link"
                          }
                          style={{ cursor: "pointer", color: "black" }}
                        >
                          {item}
                        </a>
                      </li>
                    );
                  })}

                  <li class="page-item" onClick={this.Next()}>
                    <a
                      href="#totop"
                      class="page-link"
                      style={{ cursor: "pointer", color: "black" }}
                    >
                      Next
                    </a>
                  </li>
                </ul>
              </nav>
            </Col>
          </Row>
        ) : (
          ""
        )}
      </div>
    );
  }
}

export default withRouter(MainSeller);
