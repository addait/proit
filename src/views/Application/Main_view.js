import React from "react";
import { Col, Row, Button } from "reactstrap";
import Swal from "sweetalert2";
import { REQUEST_URL } from "../../config";
import Cookies from "js-cookie";
import moment from "moment";
import Axios from "axios";

class ViewMainApplication extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount = () => {
    this.dataEdit();
  };

  dataEdit = () => {
    Axios.post(
      REQUEST_URL + "/application/dataappedit",
      { appID: this.props.match.params.id },
      { headers: { authorization: Cookies.get("webcomputer") } }
    ).then(response => {
      if (response.data.status) {
        this.setState({
          appID: response.data.data[0].appID,
          nameapp: response.data.data[0].appName,
          nameappeng: response.data.data[0].appNameEng,
          depuse: response.data.data[0].depUse,
          teldepuse: response.data.data[0].telDepUse,
          pfs_id: response.data.data[0].pfs_id,
          programlanguage: response.data.data[0].programLanguage,
          developer: response.data.data[0].developer,
          teldeveloper: response.data.data[0].telDeveloper,
          scope: response.data.data[0].scop,
          remark: response.data.data[0].reMark,
          preby: response.data.data[0].preby,
          predate: response.data.data[0].predate
        });
      } else {
        this.props.history.push("/mainapplication");
      }
    });
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    const {
      nameapp,
      nameappeng,
      depuse,
      teldepuse,
      pfs_id,
      programlanguage,
      developer,
      teldeveloper,
      scope,
      remark
    } = this.state;
    return (
      <div>
        <div className="animated fadeIn">
          รายละเอียด
          <Row>
            <Col md="12">
              <Row style={{ margin: "1%" }}>
                <Col md="12">
                  <label style={{ width: "12%", textAlign: "right" }}>
                    รหัสโปรแกรม : 
                  </label>
                  <input
                    value={this.state.appID}
                    disabled
                    style={{
                      width: "15%",
                      fontWeight: "bold",
                      textAlign: "center"
                    }}
                  />
                </Col>
              </Row>
              <Row style={{ margin: "1%" }}>
                <Col md="12">
                  <label style={{ width: "12%", textAlign: "right" }}>
                    ชื่อโปรแกรม : 
                  </label>
                  <input
                    name="nameapp"
                    value={nameapp}
                    values={this.state.nameapp}
                    onChange={this.handleChange}
                    disabled
                    style={{ width: "30%" }}
                  />
                  <label style={{ width: "12%", textAlign: "right" }}>
                    แผนกที่ใช้งาน : 
                  </label>
                  <input
                    name="depuse"
                    value={depuse}
                    values={this.state.depuse}
                    onChange={this.handleChange}
                    disabled
                    style={{ width: "30%" }}
                  />
                </Col>
              </Row>
              <Row style={{ margin: "1%" }}>
                <Col md="12">
                  <label style={{ width: "12%", textAlign: "right" }}>
                    ชื่อโปรแกรม(ENG) : 
                  </label>
                  <input
                    name="nameappeng"
                    value={nameappeng}
                    values={this.state.nameappeng}
                    onChange={this.handleChange}
                    disabled
                    style={{ width: "30%" }}
                  />
                  <label style={{ width: "12%", textAlign: "right" }}>
                    เบอร์ติดต่อ : 
                  </label>
                  <input
                    name="teldepuse"
                    value={teldepuse}
                    values={this.state.teldepuse}
                    onChange={this.handleChange}
                    disabled
                    style={{ width: "30%" }}
                  />
                </Col>
              </Row>
              <Row style={{ margin: "1%" }}>
                <Col md="12">
                  <label style={{ width: "12%", textAlign: "right" }}>
                    รหัสพนักงาน : 
                  </label>
                  <input
                    name="pfs_id"
                    value={pfs_id}
                    values={this.state.pfs_id}
                    onChange={this.handleChange}
                    disabled
                    style={{ width: "30%" }}
                  />
                  <label style={{ width: "12%", textAlign: "right" }}>
                    ภาษาที่ใช้พัฒนา : 
                  </label>
                  <input
                    name="programlanguage"
                    value={programlanguage}
                    values={this.state.programlanguage}
                    onChange={this.handleChange}
                    disabled
                    style={{ width: "30%" }}
                  />
                </Col>
              </Row>
              <Row style={{ margin: "1%" }}>
                <Col md="12">
                  <label style={{ width: "12%", textAlign: "right" }}>
                    ผู้พัฒนาโปรแกรม : 
                  </label>
                  <input
                    name="developer"
                    value={developer}
                    values={this.state.developer}
                    onChange={this.handleChange}
                    disabled
                    style={{ width: "30%" }}
                  />
                </Col>
              </Row>
              <Row style={{ margin: "1%" }}>
                <Col md="12">
                  <label style={{ width: "12%", textAlign: "right" }}>
                    เบอร์ติดต่อ : 
                  </label>
                  <input
                    name="teldeveloper"
                    value={teldeveloper}
                    values={this.state.teldeveloper}
                    onChange={this.handleChange}
                    disabled
                    style={{ width: "30%" }}
                  />
                </Col>
              </Row>
              <Row style={{ margin: "1%" }}>
                <Col md="12">
                  <label style={{ width: "12%" }}></label>
                  <label style={{ marginBottom: "1%" }}>
                    ขอบเขตการทำงานของโปรแกรม
                  </label>
                </Col>
                <Col md="12">
                  <label style={{ width: "12%" }}></label>
                  <textarea
                    name="scope"
                    value={scope}
                    values={this.state.scope}
                    onChange={this.handleChange}
                    disabled
                    style={{ width: "60%" }}
                    rows="7"
                  />
                </Col>
              </Row>
              <Row style={{ margin: "1%" }}>
                <Col md="12">
                  <label style={{ width: "12%", textAlign: "right" }}>
                    หมายเหตุ : 
                  </label>
                  <input
                    name="remark"
                    value={remark}
                    values={this.state.remark}
                    onChange={this.handleChange}
                    disabled
                    style={{ width: "30%" }}
                  />
                </Col>
              </Row>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default ViewMainApplication;
