import React from "react";
import {
  Modal,
  Container,
  Row,
  Col,
  Form,
  Button,
  Table,
  Tab,
  Tabs
} from "react-bootstrap";
import axios from "axios";
import { REQUEST_URL } from "../../../config";
import Cookies from "js-cookie";
import Swal from "sweetalert2";

class ModalAdd extends React.Component {
  state = {
    datasparepart: [],
    inputsearchx: ""
  };

  componentDidMount = async () => {
    await this.Sparepart();
  };

  Sparepart = async () => {
    await axios
      .post(
        REQUEST_URL + "/suppliesmrp/datasparepartmrp",
        {},
        {
          headers: { authorization: Cookies.get("webcomputer") }
        }
      )
      .then(response => {
        if (response.data.status) {
          this.setState({
            datasparepart: response.data.data
          });
        } else {
          this.props.history.push("/login");
        }
      });
  };

  modalClose = bool => e => {
    if (!bool) {
      this.setState({
        inputsearchx: ""
      });
    }
    this.props.set(bool);
  };

  searchHandler = e => {
    this.setState({ [e.target.name]: e.target.value });
    this.searchNoButton(e.target.value);
  };

  searchNoButton = txt => {
    let ggez = txt;
    var filtered = this.state.datasparepart.filter(function(x) {
      return (
        x.group.match(new RegExp("^" + ggez === null ? "" : ggez, "i")) ||
        x.prodcode.match(new RegExp("^" + ggez === null ? "" : ggez, "i")) ||
        x.name.match(new RegExp("^" + ggez === null ? "" : ggez, "i"))
      );
    });
    this.setState({ searchx: filtered.slice(0, 200) });
  };

  toMain = (
    group,
    prodcode,
    name,
    unit,
    stdprice,
    lastprice,
    vend_code,
    glcode,
    glcode2,
    rw_stock,
    maxQty,
    minQty,
    picname
  ) => e => {
    for (let x = 0; x < this.props.dataMRP.length; x++) {
      if (this.props.dataMRP[x].prodcode === prodcode) {
        Swal.fire({
          type: "error",
          title: "มี Prodcode " + prodcode + " อยู่แล้ว",
          timer: 1200,
          showConfirmButton: false
        });
      } else {
        console.log(
          group,
          prodcode,
          name,
          unit,
          stdprice,
          lastprice,
          vend_code,
          glcode,
          glcode2,
          rw_stock,
          maxQty,
          minQty,
          picname
        );
      }
    }
  };

  render() {
    const { inputsearchx } = this.state;
    return (
      <div>
        <Modal
          size="lg"
          show={this.props.Modal}
          onHide={this.modalClose(false)}
        >
          <Modal.Header
            closeButton
            id="fontthai"
            style={{ fontSize: "22px" }}
          ></Modal.Header>
          <Modal.Body>
            <label>ค้นหา : </label>
            <input
              type="text"
              name="inputsearchx"
              values={inputsearchx}
              onChange={this.searchHandler}
              placeholder="รหัสบาร์โค้ดหรือชื่อซอฟต์แวร์เพื่อค้นหา"
              style={{ width: "500px", marginBottom: "2%" }}
            />
            <div style={{ display: "flex", justifyContent: "center" }}>
              <table
                //   width="100%"
                style={{
                  display: "block",
                  height: "350px",
                  overflowY: "scroll",
                  alignItems: "center"
                }}
              >
                <thead className="text-center table-active">
                  <tr>
                    <th id="tb" width="20%">
                      กลุ่ม
                    </th>
                    <th id="tb" width="20%">
                      รหัส
                    </th>
                    <th id="tb" width="35%">
                      รายการ
                    </th>
                    <th id="tb" width="15%">
                      หน่วย
                    </th>
                    <th id="tb" width="10%">
                      บันทึก
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.inputsearchx === ""
                    ? this.state.datasparepart.map((item, index) => {
                        return (
                          <tr key={index}>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.group}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.prodcode}
                            </td>
                            <td id="tb">{item.name}</td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.unit}
                            </td>
                            <td
                              id="tb"
                              style={{
                                textAlign: "center",
                                cursor: "pointer",
                                color: "red"
                              }}
                              onClick={this.toMain(
                                item.group,
                                item.prodcode,
                                item.name,
                                item.unit,
                                item.stdprice,
                                item.lastprice,
                                item.vend_code,
                                item.glcode,
                                item.glcode2,
                                item.rw_stock,
                                item.maxQty,
                                item.minQty,
                                item.picname
                              )}
                            >
                              บันทึก
                            </td>
                          </tr>
                        );
                      })
                    : this.state.searchx.map((item, index) => {
                        return (
                          <tr key={index}>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.group}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.prodcode}
                            </td>
                            <td id="tb">{item.name}</td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.unit}
                            </td>
                            <td
                              id="tb"
                              style={{
                                textAlign: "center",
                                cursor: "pointer",
                                color: "red"
                              }}
                              onClick={this.toMain(
                                item.group,
                                item.prodcode,
                                item.name,
                                item.unit,
                                item.stdprice,
                                item.lastprice,
                                item.vend_code,
                                item.glcode,
                                item.glcode2,
                                item.rw_stock,
                                item.maxQty,
                                item.minQty,
                                item.picname
                              )}
                            >
                              บันทึก
                            </td>
                          </tr>
                        );
                      })}
                </tbody>
              </table>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={this.modalClose(false)}
              id="btnthai"
            >
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default ModalAdd;
