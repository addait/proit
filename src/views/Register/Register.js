import React from "react";
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Alert
} from "reactstrap";
import axios from "axios";
import { REQUEST_URL } from "../../config";
import "../style.css";
import Swal from "sweetalert2";

class Login extends React.Component {
  state = {
    pfsID: "",
    userID: "",
    pass: ""
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onSubmit = e => {
    e.preventDefault();
    axios
      .post(REQUEST_URL + "/register", {
        userID: this.state.userID.toUpperCase(),
        password: this.state.pass.toUpperCase()
      })
      .then(response => {
        if (response.data.status) {
          Swal.fire({
            type: "success",
            title: "เพิ่มข้อมูลสำเร็จ",
            timer: 1200
          });
          setTimeout(() => {
            this.props.history.push("/login");
          }, 1400);
        } else {
          Swal.fire({
            type: "error",
            title: "เพิ่มข้อมูลไม่สำเร็จ",
            timer: 1200
          });
        }
      });
  };

  render() {
    return (
      <div style={{ width: "100%", height: "100%" }}>
        <div className="registerbackground" style={{ height: "100vh" }}>
          <Row style={{ margin: "0px" }}>
            <Col md="12"></Col>
          </Row>
          <Row
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              margin: "0px",
              height: "100vh"
            }}
          >
            <Col md="4">
              <CardGroup>
                <Card className="p-4" style={{ backgroundColor: "#e4e5e6" }}>
                  <CardBody>
                    <Form onSubmit={e => this.onSubmit(e)}>
                      <h1>สมัครสมาชิก</h1>
                      <p></p>
                      {this.state.status === false ? (
                        <Alert
                          className="alert alert-danger fade show"
                          variant="danger"
                        >
                          Username หรือ Password ถูกใช้แล้ว
                        </Alert>
                      ) : (
                        ""
                      )}

                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-pencil" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="text"
                          name="userID"
                          placeholder="Username"
                          autoComplete="username"
                          required
                          onChange={e => this.handleChange(e)}
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="password"
                          name="pass"
                          placeholder="Password"
                          autoComplete="current-password"
                          required
                          onChange={e => this.handleChange(e)}
                        />
                      </InputGroup>
                      <Row>
                        <Col md="6" xs="6">
                          <Button color="primary" className="px-4">
                            ยืนยัน
                          </Button>
                        </Col>
                        <Col
                          md="6"
                          xs="6"
                          style={{
                            display: "flex",
                            justifyContent: "flex-end"
                          }}
                        >
                          <p
                            onClick={() => this.props.history.push("/login")}
                            style={{
                              color: "black",
                              fontSize: "18px",
                              textDecoration: "underline",
                              cursor:'pointer',
                            }}
                          >
                            เข้าสู่ระบบ
                          </p>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
          <Row style={{ margin: "0px" }}>
            <Col md="12"></Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default Login;
