import React from "react";
import {
  Modal,
  Container,
  Row,
  Col,
  Form,
  Button,
  Table,
  Tab,
  Tabs
} from "react-bootstrap";
import axios from "axios";
import { REQUEST_URL } from "../../../config";
import Cookies from "js-cookie";
import Axios from "axios";
import Swal from "sweetalert2";

class ModalAdd extends React.Component {
  state = {
    brandID: "",
    brandName: ""
  };

  componentDidMount = async () => {
    Axios.post(
      REQUEST_URL + "/brand/genidbrand",
      {},
      { headers: { authorization: Cookies.get("webcomputer") } }
    ).then(response => {
      if (response.data.status) {
        this.setState({
          brandID: response.data.data
        });
      } else {
        this.props.history.push("/login");
      }
    });
  };

  modalClose = bool => e => {
    if (!bool) {
      this.setState({
        inputsearchx: ""
      });
    }
    this.props.set(bool);
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  sendData = () => {
    if (this.state.typeID === "" || this.state.typeName === "") {
      Swal.fire({
        type: "error",
        title: "กรอกข้อมูลให้ครบถ้วน",
        timer: 1200
      });
    } else {
      Axios.post(
        REQUEST_URL + "/brand/adddatabrand",
        {
          brandID: this.state.brandID,
          brandName: this.state.brandName,
          preby: sessionStorage.getItem("user_id")
        },
        { headers: { authorization: Cookies.get("webcomputer") } }
      ).then(response => {
        if (response.data.status) {
          Swal.fire({
            type: "success",
            title: "เพิ่มข้อมูลสำเร็จ...",
            timer: 1200,
            showConfirmButton: false
          });
          this.props.set(false);
        } else {
          Swal.fire({
            type: "error",
            title: "เพิ่มข้อมูลไม่สำเร็จ...",
            timer: 1200,
            showConfirmButton: false
          });
        }
      });
    }
  };

  render() {
    const { brandID, brandName } = this.state;
    return (
      <div>
        <Modal
          size="lg"
          show={this.props.Modal}
          onHide={this.modalClose(false)}
        >
          <Modal.Header closeButton id="fontthai" style={{ fontSize: "22px" }}>
            เพิ่มข้อมูล
          </Modal.Header>
          <Modal.Body>
            <div style={{ display: "flex", justifyContent: "flex-start" }}>
              <Row>
                <Col md="12">
                  <label style={{ width: "100px" }}>รหัสแบรนด์ : </label>
                  <input
                    name="brandID"
                    value={this.state.brandID}
                    values={brandID}
                    onChange={this.handleChange}
                    disabled
                  />
                </Col>
                <Col md="12" style={{ marginTop: "2%" }}>
                  <label style={{ width: "100px" }}>ชื่อแบรนด์ : </label>
                  <input
                    name="brandName"
                    value={this.state.brandName}
                    values={brandName}
                    onChange={this.handleChange}
                    style={{ width: "80%" }}
                  />
                </Col>
              </Row>
            </div>
          </Modal.Body>
          <Modal.Footer style={{ display: "flex", justifyContent: "center" }}>
            <Button variant="success" onClick={this.sendData} id="btnthai">
              บันทึกข้อมูล
            </Button>
            <Button
              variant="secondary"
              onClick={this.modalClose(false)}
              id="btnthai"
            >
              ปิดหน้านี้
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default ModalAdd;
