import React from "react";
import {
  Card,
  CardBody,
  Col,
  Row,
  Button,
  FormGroup,
  Input,
  InputGroup
} from "reactstrap";
import {  Statistic, Icon } from "antd";
import BootstrapTable from "react-bootstrap-table-next";
import { HorizontalBar } from "react-chartjs-2";

class MainComputer extends React.Component {
  state = {
    data: {
      labels: [
        "Microsoft Windows 8.1 Pro",
        "Microsoft Windows 7 Ultimate",
        "Microsoft Windows 10 Pro",
        "Microsoft Windows XP Professional",
        "Microsoft Windows 7 Professionnal"
      ],
      datasets: [
        {
          backgroundColor: "rgba(255,99,132,0.2)",
          borderColor: "rgba(255,99,132,1)",
          borderWidth: 1,
          hoverBackgroundColor: "#94CCEC",
          hoverBorderColor: "#0007FF",
          data: [1, 1, 2, 48, 57],
          label: "จำนวนเครื่อง"
        }
      ]
    },
    columns : [{
      dataField: 'id',
      text: 'Product ID',
      align:'center',
      headerAlign:'center',
    }, {
      dataField: 'name',
      text: 'Product Name',
      align:'center',
      headerAlign:'center',
    }, {
      dataField: 'price',
      text: 'Product Price',
      align:'center',
      headerAlign:'center',
    }],
    datatable: [{ "id": "1",
    "name": "Smith",
    "price": 199},
    { "id": "2",
    "name": "Smith",
    "price": 199},
    { "id": "3",
    "name": "Smith",
    "price": 199},
    { "id": "4",
    "name": "Smith",
    "price": 199},
    { "id": "5",
    "name": "Smith",
    "price": 199},
  ]
  };
  render() {
    return (
      <div className="animated fadeIn" style={{ padding: "20px" }}>
        <Row>
          <Col xs={3}>
            <Card>
              <CardBody>
                <FormGroup row style={{ padding: "5px" }}>
                  <Statistic
                    title="Online"
                    value={29}
                    valueStyle={{ color: "#3f8600" }}
                    prefix={<Icon type="user" />}
                  />
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
          <Col xs={3}>
            <Card>
              <CardBody>
                <FormGroup row style={{ padding: "5px" }}>
                  <Statistic
                    title="Offline"
                    value={79}
                    valueStyle={{ color: "#FF0000" }}
                    prefix={<Icon type="user" />}
                  />
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
          <Col xs={3}>
            <Card>
              <CardBody>
                <FormGroup row style={{ padding: "5px" }}>
                  <Statistic
                    title="Personal Computer (Total)"
                    value={491}
                    // precision={2}
                    valueStyle={{ color: "#001FFF" }}
                    prefix={<Icon type="desktop" />}
                  />
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
          <Col xs={3}>
            <Card>
              <CardBody>
                <FormGroup row style={{ padding: "5px" }}>
                  <Statistic
                    title="Notebook (Total)"
                    value={8}
                    // precision={2}
                    valueStyle={{ color: "#429AC0" }}
                    prefix={<Icon type="laptop" />}
                  />
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            <Card>
              <CardBody>
                <FormGroup row style={{ padding: "5px" }}>
                <h2>Windows</h2>
                <BootstrapTable keyField='id' data={ this.state.datatable } columns={ this.state.columns } />                   
                <label style={{color:'red'}}>** ไม่ได้ติดตั้งตัวรับข้อมูลจำนวน 439 เครื่อง</label>             
                </FormGroup>
                <label style={{color:'blue'}}><b>รวมทั้งสิ้น (เครื่อง) : 109</b></label>     
              </CardBody>
            </Card>
          </Col>
          <Col xs={6}>
            <Card>
              <CardBody>
                <FormGroup row style={{ padding: "5px" }}>
                  <h2>Windows</h2>
                  <HorizontalBar data={this.state.data} />
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}
export default MainComputer;
