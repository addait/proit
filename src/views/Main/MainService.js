import React from "react";
import {
  Card,
  CardBody,
  Col,
  Row,
  FormGroup,
  InputGroup,
  Input
} from "reactstrap";
import BootstrapTable from "react-bootstrap-table-next";
import { HorizontalBar } from "react-chartjs-2";
import { Tabs, Button, Badge } from "antd";
import { FaNetworkWired } from "react-icons/fa";
import { GoCircuitBoard, GoBug, GoTools } from "react-icons/go";

const { TabPane } = Tabs;

class MainService extends React.Component {
  state = {
    columns: [
      {
        dataField: "id",
        text: "Product ID",
        align: "center",
        headerAlign: "center"
      },
      {
        dataField: "name",
        text: "Product Name",
        align: "center",
        headerAlign: "center"
      },
      {
        dataField: "namereal",
        text: "Product Price",
        align: "center",
        headerAlign: "center"
      },
      {
        dataField: "prirate",
        text: "Product Price",
        align: "center",
        headerAlign: "center"
      },
      {
        dataField: "price",
        text: "Product Price",
        align: "center",
        headerAlign: "center"
      }
    ],
    datatable: [
      { id: "1", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "2", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "3", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "4", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "5", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "6", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "7", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "8", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "9", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "10", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "11", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "12", name: "Smith", price: 199, namereal: "dew", prirate: 509 }
    ]
  };
  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs={8}>
            <h5>
              <b>รายการจำนวนงาน</b>
            </h5>        
            <hr />
            <FormGroup row style={{ padding: "5px" }}>
              <BootstrapTable
                keyField="id"
                data={this.state.datatable}
                columns={this.state.columns}
              />
            </FormGroup>
            {/* <FormGroup row > */}

            <h5>
              <b>รายการค้างซ่อม</b>
            </h5>
            <Button.Group style={{display:'flex' , justifyContent:'flex-start'}}>
            <Button type="primary" icon="file-text">พิมพ์ใบแจ้งซ่อม</Button>
            <Button type="greenmary" icon="sync">ย้ายหน่วยงาน</Button>
            <Button type="danger" icon="form">ผู้ดำเนินการ</Button>
            </Button.Group>
            {/* </FormGroup> */}
            <hr />
            <FormGroup row style={{ padding: "5px" }}>
              <Tabs defaultActiveKey="1">
                <TabPane
                  tab={
                      <Badge count={100}>
                    <span>
                      <i className="fa fa-microchip "></i>

                      &nbsp; Technical Support
                    </span>
                      </Badge>
                  }
                  key="1"
                >
                     <BootstrapTable
                keyField="id"
                data={this.state.datatable}
                columns={this.state.columns}
              />
                </TabPane>
                <TabPane
                  tab={
                    <Badge count={100}>

                    <span>
                      <GoCircuitBoard />
                      &nbsp; System Support
                    </span>
                    </Badge>
                  }
                  key="2"
                ></TabPane>
                <TabPane
                  tab={
                      <span>
                      <FaNetworkWired />
                      <Badge count={100}>
                      &nbsp; Network
                      </Badge>
                    </span>
                  }
                  key="3"
                ></TabPane>
                <TabPane
                  tab={
                    <span>
                      <GoBug />
                      <Badge count={100}>
                      &nbsp; Develop Program
                      </Badge>
                    </span>
                  }
                  key="4"
                ></TabPane>
              </Tabs>
            </FormGroup>
          </Col>

          <Col xs={4}>
            <h5>
              <b>รายการจำนวนงาน</b>
            </h5>
            <hr />
            <BootstrapTable
              keyField="id"
              data={this.state.datatable}
              columns={this.state.columns}
            />
          </Col>
        </Row>
      </div>
    );
  }
}
export default MainService;
