import React from "react";
import {
  Card,
  CardBody,
  Col,
  Row,
  FormGroup,
  InputGroup,
  Input
} from "reactstrap";
import { Tabs, Icon, Badge} from "antd";
import "antd/dist/antd.css";
import MainComputer from './MainComputer'
import MainPay from './MainPay'
import MainStatic from './MainStatic'
import MainService from './MainService'

const { TabPane } = Tabs;

class Maindash extends React.Component {
  render() {
    return (
      <div className="animated fadeIn" style={{ padding: "20px" }}>
        <Row>
          <Col xs={12}>
            <Card>
              <CardBody>
                <Tabs defaultActiveKey="1">
                  <TabPane
                    tab={                     
                      <span>
                        <i className="fa fa-hand-paper-o "></i>
                        &nbsp; 
                        ใบแจ้งขอบริการ
                      </span>                      
                    }
                    key="1"
                  >
                    <MainService />
                  </TabPane>
                  <TabPane
                    tab={
                      <span>
                         <i className="fa fa-file-archive-o fa-lg mt-4"></i>
                        &nbsp; 
                        ใบจองวัสดุสิ้นเปลือง
                      </span>
                    }
                    key="2"
                  ></TabPane>
                  <TabPane tab={
                    <span>
                       <i className="fa fa-hand-pointer-o fa-lg mt-4"></i>
                       <Badge count={3}>
                        &nbsp; 
                        อนุมัติรับงาน
                      </Badge>
                      </span>
                    } key="3"></TabPane>
                  <TabPane tab={
                      <span>
                        <i className="fa fa-desktop fa-lg mt-4"></i>
                        &nbsp; 
                        คอมพิวเตอร์
                      </span>
                    } key="4">
                       <MainComputer />
                    </TabPane>
                  <TabPane tab={
                      <span>
                        <i className="fa fa-pie-chart fa-lg mt-4"></i>
                        &nbsp; 
                        สถิติ
                      </span>
                    } key="5">
                        <MainStatic />
                    </TabPane>
                  <TabPane tab={
                      <span>
                        <i className="fa fa-money fa-lg mt-4"></i>
                        &nbsp; 
                        สรุปค่าใช้จ่าย
                      </span>
                    } key="6">
                      <MainPay />
                    </TabPane>
                  <TabPane tab={
                    <span>
                         <i className="fa fa-list-alt fa-lg mt-4"></i>
                        <Badge count={1}>
                        &nbsp; 
                        รอเปลี่ยนชื่อ
                      </Badge>
                      </span>
                    } key="7"></TabPane>
                </Tabs>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}
export default Maindash;
