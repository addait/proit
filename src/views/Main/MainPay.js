import React from "react";
import {
  Card,
  CardBody,
  Col,
  Row,
  FormGroup,
  Input,
  InputGroup
} from "reactstrap";
import { Statistic, Icon, Cascader, Button } from "antd";
import BootstrapTable from "react-bootstrap-table-next";
import { Bar, Pie } from "react-chartjs-2";

class MainPay extends React.Component {
  state = {
    combo: [
      {
        value: "2018",
        label: "ปี 2018"
      },
      {
        value: "2019",
        label: "ปี 2019"
      }
    ],
    data: {
      labels: [
        "มกราคม",
        "กุมภาพันธ์",
        "มีนาคม",
        "เมษายน",
        "พฤษภาคม",
        "มิถุนายน",
        "กรกฎาคม",
        "สิงหาคม",
        "กันยายน",
        "ตุลาคม",
        "พฤศจิกายน",
        "ธันวาคม"
      ],
      datasets: [
        {
          label: "งานซ่อม",
          backgroundColor: "#AFFF9C",
          borderColor: "#369100",
          borderWidth: 1,
          hoverBackgroundColor: "#85D372",
          hoverBorderColor: "#369100",
          data: [65, 59, 80, 81, 56, 55, 40, 83, 84, 55, 56, 47]
        },
        {
          label: "วัสดุสิ้นเปลือง",
          backgroundColor: "#FFCD7C",
          borderColor: "#FF7000",
          borderWidth: 1,
          hoverBackgroundColor: "#FFA209",
          hoverBorderColor: "#FF7000",
          data: [61, 52, 83, 84, 55, 56, 47, 23, 24, 25, 26, 27]
        },
        {
          label: "เครื่องถ่ายเอกสาร",
          backgroundColor: "rgba(255,99,132,0.2)",
          borderColor: "rgba(255,99,132,1)",
          borderWidth: 1,
          hoverBackgroundColor: "rgba(255,99,132,0.4)",
          hoverBorderColor: "rgba(255,99,132,1)",
          data: [21, 22, 23, 24, 25, 26, 27, 59, 80, 81, 56, 55]
        }
      ]
    },
    columns: [
      {
        dataField: "id",
        text: "Product ID",
        align: "center",
        headerAlign: "center"
      },
      {
        dataField: "name",
        text: "Product Name",
        align: "center",
        headerAlign: "center"
      },
      {
        dataField: "namereal",
        text: "Product Price",
        align: "center",
        headerAlign: "center"
      },
      {
        dataField: "prirate",
        text: "Product Price",
        align: "center",
        headerAlign: "center"
      },
      {
        dataField: "price",
        text: "Product Price",
        align: "center",
        headerAlign: "center"
      }
    ],
    datatable: [
      { id: "1", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "2", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "3", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "4", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "5", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "6", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "7", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "8", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "9", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "10", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "11", name: "Smith", price: 199, namereal: "dew", prirate: 509 },
      { id: "12", name: "Smith", price: 199, namereal: "dew", prirate: 509 }
    ],
    datapie: {
      labels: [
        "รวมงานซ่อมทั้งหมด",
        "รวมวัสดุสิ้นเปลืองทั้งหมด",
        "รวมเครื่องถ่ายเอกสารทั้งหมด"
      ],
      datasets: [
        {
          data: [868293.0, 618526.32, 995755.26],
          backgroundColor: ["#36A2EB", "#FFCE56", "#FF6384"],
          hoverBackgroundColor: ["#36A2EB", "#FFCE56", "#FF6384"]
        }
      ]
    }
  };
  render() {
    return (
      <div className="animated fadeIn" style={{ padding: "20px" }}>
        <Row>
          <Col xs={12}>
            <Card>
              <CardBody>
                <FormGroup row style={{ padding: "5px" }}>
                  {/* <Col xs={4}>
                    <h2>ปีค่าใช้จ่าย</h2>
                    <Cascader
                      // defaultValue={['2019']}
                      placeholder="โปรดเลือก ปีค่าใช้จ่าย "
                      options={this.state.combo}
                    />
                    <Button
                      type="primary"
                      shape="round"
                      icon="download"
                      size={'large'}
                    >
                      Download
                    </Button>
                  </Col> */}
                  <Col xs={8}>
                    <h2>ค่าใช้จ่าย</h2>
                    <Cascader
                      defaultValue={['2019']}
                      placeholder="โปรดเลือก ปีค่าใช้จ่าย "
                      options={this.state.combo}
                    />     
                    {' '}            
                    <Button
                      type="primary"
                      shape="round"
                      icon="sync"
                    //   size={'large'}
                    >
                      ประมวล
                    </Button>
                    <Bar data={this.state.data} />
                  </Col>
                </FormGroup>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <Row>
          <Col xs={6}>
            {/* <Card>
              <CardBody> */}
            <FormGroup row style={{ padding: "5px" }}>
              <BootstrapTable
                keyField="id"
                data={this.state.datatable}
                columns={this.state.columns}
              />
            </FormGroup>
            {/* </CardBody>
            </Card> */}
          </Col>
          <Col xs={6}>
            {/* <Card>
              <CardBody> */}
            <FormGroup row>
              <Col xs={6}>
                <Card>
                  <CardBody>
                    <Statistic
                      title="รวมงานซ่อมทั้งหมด (บาท)"
                      value={868293}
                      precision={2}
                      valueStyle={{ color: "#001FFF" }}
                      prefix={<Icon type="dollar" />}
                    />
                  </CardBody>
                </Card>
              </Col>
              <Col xs={6}>
                <Card>
                  <CardBody>
                    <Statistic
                      title="รวมวัสดุสิ้นเปลืองทั้งหมด (บาท)"
                      value={618526.32}
                      precision={2}
                      valueStyle={{ color: "#FFB500" }}
                      prefix={<Icon type="dollar" />}
                    />
                  </CardBody>
                </Card>
              </Col>
            </FormGroup>
            <FormGroup row>
              <Col xs={6}>
                <Card>
                  <CardBody>
                    <Statistic
                      title="รวมเครื่องถ่ายเอกสารทั้งหมด (บาท)"
                      value={995755.26}
                      precision={2}
                      valueStyle={{ color: "red" }}
                      prefix={<Icon type="dollar" />}
                    />
                  </CardBody>
                </Card>
              </Col>
              <Col xs={6}>
                <Card>
                  <CardBody>
                    <Statistic
                      title="รวมค่าใช้จ่ายทั้งหมด (บาท)"
                      value={2482574.58}
                      precision={2}
                      valueStyle={{ color: "#4EA627" }}
                      prefix={<Icon type="dollar" />}
                    />
                  </CardBody>
                </Card>
              </Col>
            </FormGroup>
            {/* </CardBody>
            </Card> */}
            <FormGroup row>
              <Col xs={12}>
                <Card>
                  <CardBody>
                    <Pie data={this.state.datapie} />
                  </CardBody>
                </Card>
              </Col>
            </FormGroup>
          </Col>
        </Row>
      </div>
    );
  }
}
export default MainPay;
