import React from "react";
import MSproduct from '../MainStatic/MSproduct'
import MSservice from '../MainStatic/MService'
import MSrepair from '../MainStatic/MSrepair'
import MSdep from '../MainStatic/MSdep'
import { Tabs, Icon, DatePicker,Button  } from "antd";

const { TabPane } = Tabs;
const { RangePicker } = DatePicker;

class MainStatic extends React.Component {
    state = {
        mode: ['month', 'month'],
        value: [],
      };

      handlePanelChange = (value, mode) => {
        this.setState({
          value,
          mode: [mode[0] === 'date' ? 'month' : mode[0], mode[1] === 'date' ? 'month' : mode[1]],
        });
      };
    
      handleChange = value => {
        this.setState({ value });
      };
  render() {
    const { value, mode } = this.state
   
    return (
      <div className="animated fadeIn" >
          <div style={{display:'flex' , justifyContent:'center'}}>
          <RangePicker
        placeholder={['เริ่มเดือน', 'สิ้นสุดเดือน']}
        format="YYYY-MM"
        value={value}
        mode={mode}
        onChange={this.handleChange}
        onPanelChange={this.handlePanelChange}
      />
       <Button
                      type="primary"
                      icon="sync"
                    >
                      ประมวล
                    </Button>
                    </div>
        <Tabs defaultActiveKey="2">
          <TabPane
            tab={
              <span>
                <Icon type="bar-chart" />
                สถิติขอรับบริการ
              </span>
            }
            key="1"
          >
            <MSservice />
          </TabPane>
          <TabPane
            tab={
              <span>
                <Icon type="bar-chart" />
                สถิติพนักงานซ่อม
              </span>
            }
            key="2"
          >
            <MSrepair />
          </TabPane>
          <TabPane
            tab={
              <span>
                <Icon type="bar-chart" />
                10 อันดับสินค้า
              </span>
            }
            key="3"
          >
           <MSproduct />
          </TabPane>
          <TabPane
            tab={
              <span>
                <Icon type="bar-chart" />
                10 อันดับแผนก
              </span>
            }
            key="4"
          >
           <MSdep />
          </TabPane>        
        </Tabs>
      </div>
    );
  }
}
export default MainStatic;
