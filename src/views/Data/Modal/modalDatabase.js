import React from "react";
import {
  Modal,
  Form,
  Row,
  Col,
  Button,
  FormGroup,
  Dropdown
} from "react-bootstrap";
import DropdownToggle from "react-bootstrap/DropdownToggle";
import axios from "axios";
import { REQUEST_URL } from "../../../config";
// import cookie from "js-cookie";
import Swal from "sweetalert2";

class modalDatabase extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      server: "",
      data: []
    };
  }

  componentDidMount = () => {
    this.loadDatabase();
  };

  loadDatabase = () => {
    axios
      .post(
        REQUEST_URL + "/database",
        {}
        // ,
        // {
        //   headers: { authorization: cookie.get("webcomputer") }
        // }
      )
      .then(response => {
        this.setState({
          data: response.data.data
        });
      });
  };

  modalClose = bool => e => {
    this.props.set(bool);
  };

  onChangeDrop = e => {
    this.setState({
      value: e.currentTarget.textContent
    });
  };

  onChangeIP = e => {
    this.setState({
      server: e.target.value
    });
  };

  saveBasename = e => {
    e.preventDefault();
    axios
      .post(
        REQUEST_URL + "/addbasename",
        {
          basename: this.state.value,
          server: this.state.server,
          preby: sessionStorage.getItem("user_id")
        }
        // ,
        // {
        //   headers: { authorization: cookie.get("webcomputer") }
        // }
      )
      .then(response => {
        if (response.data.status) {
          Swal.fire({
            type: "success",
            title: "เพิ่มดาต้าเบลสสำเร็จ ...",
            timer: 1200
          });
          this.props.set(false);
        } else {
          Swal.fire({
            type: "error",
            title: "เพิ่มดาต้าเบลสไม่สำเร็จ ...",
            timer: 1200
          });
        }
      });
  };

  render() {
    return (
      <div>
        <Modal
          size="lg"
          show={this.props.Modal}
          onHide={this.modalClose(false)}
          id="fontthai"
        >
          <Form onSubmit={this.saveBasename}>
            <Modal.Header closeButton>
              <Modal.Title>
                เลือก Database ที่จะจัดทำ Data Dictionary
              </Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <FormGroup>
                <Row>
                  <Col md="12">
                    <Dropdown
                      style={{ display: "flex", justifyContent: "center" }}
                      onChange={this.onChangeDrop.bind(this)}
                    >
                      <DropdownToggle
                        variant="success"
                        value={this.state.value}
                      >
                        {this.state.value === ""
                          ? "เลือก Database"
                          : this.state.value}
                      </DropdownToggle>
                      <Dropdown.Menu style={{ overflowY: "scroll" , height:"500px" }}>
                        {this.state.data.map(item => {
                          return (
                            <Dropdown.Item>
                              <div onClick={this.onChangeDrop}>{item.name}</div>
                            </Dropdown.Item>
                          );
                        })}
                      </Dropdown.Menu>
                    </Dropdown>
                  </Col>
                  <Col
                    md="12"
                    style={{
                      display: "flex",
                      justifyContent: "center",
                      marginTop: "5%"
                    }}
                  >
                    <label>IP Database : </label>
                    <input
                      required
                      name="ip"
                      placeholder="ตัวอย่าง 10.32.0.14"
                      onChange={this.onChangeIP}
                    />
                  </Col>
                </Row>
              </FormGroup>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={this.modalClose(false)}>
                Close
              </Button>
              <Button variant="primary" type="submit" value="submit">
                Save Changes
              </Button>
            </Modal.Footer>
          </Form>
        </Modal>
      </div>
    );
  }
}

export default modalDatabase;
