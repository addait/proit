import React from "react";
import { Row, Col, Button, Dropdown } from "react-bootstrap";
import DropdownToggle from "react-bootstrap/DropdownToggle";
// import cookie from "js-cookie";
import axios from "axios";
import { REQUEST_URL } from "../../config";
import Swal from "sweetalert2";

class CreateDictionary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      TableDrop: [],
      selection: "",
      value: "",
      valueth: "",
      descriptiontb: "",
      description: ""
    };
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (prevState.value !== this.state.value) {
      this.loadDetailsTable();
    }
  };

  componentDidMount = () => {
    this.loadTable();
  };

  loadTable = () => {
    axios
      .post(
        REQUEST_URL + "/loadtable",
        {
          database: this.props.match.params.database
        }
        // ,
        // {
        //   headers: { authorization: cookie.get("webcomputer") }
        // }
      )
      .then(response => {
        if (response.data.status) {
          this.setState({
            TableDrop: response.data.data
          });
        }
      });
  };

  loadDetailsTable = () => {
    axios
      .post(
        REQUEST_URL + "/detailstable",
        {
          database: this.props.match.params.database,
          table: this.state.value
        }
        // ,
        // {
        //   headers: { authorization: cookie.get("webcomputer") }
        // }
      )
      .then(response => {
        if (response.data.status) {
          let databouk = response.data.data.map(item => {
            item.description = "";
            item.key = "";
            item.ref = "";
            return item;
          });
          this.setState({
            data: databouk,
            valueth: "",
            descriptiontb: "",
            description: ""
          });
        }
      });
  };

  onChangeDrop = e => {
    this.setState({
      value: e.currentTarget.textContent
    });
  };

  onChangeTxt = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onChangeDescription = index => e => {
    const newarr = this.state.data[index];
    newarr.description = e.target.value;
    const arr = [...this.state.data];
    arr.splice(index, 1, newarr);
    this.setState({
      data: arr
    });
  };

  onChangeKey = index => e => {
    const newarr = this.state.data[index];
    newarr.key = e.target.value;
    const arr = [...this.state.data];
    arr.splice(index, 1, newarr);
    this.setState({
      data: arr
    });
  };

  onChangeRef = index => e => {
    const newarr = this.state.data[index];
    newarr.ref = e.target.value;
    const arr = [...this.state.data];
    arr.splice(index, 1, newarr);
    this.setState({
      data: arr
    });
  };

  sendData = () => {
    if (this.state.valueth !== "" && this.state.descriptiontb !== "") {
      Swal.fire({
        title: "คุณต้องการเพิ่มข้อมูล ?",
        // text: "คุณไม่สามารถกู้คืนข้อมูลนี้กลับได้น่ะ",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, Add Now !!",
        cancelButtonText: "No, Cancel !!",
        // reverseButtons: true
      }).then(result => {
        if (result.value) {
          axios
            .post(
              REQUEST_URL + "/adddatadic",
              {
                data: this.state.data,
                dataid: this.props.match.params.id,
                table: this.state.value,
                tableth: this.state.valueth,
                descriptiontb: this.state.descriptiontb,
                preby: sessionStorage.getItem("user_id")
              }
              // ,
              // {
              //   headers: { authorization: cookie.get("webcomputer") }
              // }
            )
            .then(response => {
              if (response.data.status) {
                Swal.fire({
                  type: "success",
                  title: "เพิ่มข้อมูลสำเร็จ ...",
                  timer: 1200
                });
                setTimeout(() => {
                  this.props.history.push(
                    "/data/database/table/" +
                      this.props.match.params.database +
                      "/" +
                      this.props.match.params.id
                  );
                }, 1400);
              } else {
                Swal.fire({
                  type: "error",
                  title: "เพิ่มข้อมูลไม่สำเร็จ ...",
                  timer: 1200
                });
              }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire({
            type: "error",
            title: "Cancelled ...",
            timer: 1200
          });
        }
      });
    } else {
      Swal.fire({
        type: "error",
        title: "กรอกข้อมูลให้ครบถ้วน",
        timer: 1200
      });
    }
  };

  render() {
    const { valueth, descriptiontb } = this.state;
    return (
      <div>
        <h1>DATABASE : {this.props.match.params.database}</h1>
        <Row>
          <Col md="2">
            <Dropdown
              style={{
                display: "flex",
                justifyContent: "flex-start",
                marginTop: "1%"
              }}
              onChange={this.onChangeDrop.bind(this)}
            >
              <DropdownToggle
                variant="success"
                value={this.state.value}
                style={{ fontSize: "26px" }}
              >
                {this.state.value === "" ? "เลือก Table" : this.state.value}
              </DropdownToggle>
              <Dropdown.Menu style={{ overflowY: "auto", height: "500px" }}>
                {this.state.TableDrop.map(item => {
                  return (
                    <Dropdown.Item>
                      <div onClick={this.onChangeDrop}>{item.TABLE_NAME}</div>
                    </Dropdown.Item>
                  );
                })}
              </Dropdown.Menu>
            </Dropdown>
          </Col>
        </Row>
        {this.state.data.length !== 0 ? (
          <Row>
            <Col md="12" style={{ marginTop: "2%" }}>
              <label>ชื่อตาราง (ภาษาไทย) : </label>
              <input
                required
                name="valueth"
                values={valueth}
                value={this.state.valueth}
                style={{ width: "50%" }}
                onChange={this.onChangeTxt}
              />
            </Col>
            <Col md="12" style={{ marginTop: "2%" }}>
              <label>คำอธิบาย (Description) : </label>
              <input
                required
                name="descriptiontb"
                values={descriptiontb}
                value={this.state.descriptiontb}
                type="text"
                style={{ width: "50%" }}
                onChange={this.onChangeTxt}
              />
            </Col>
          </Row>
        ) : (
          ""
        )}
        {this.state.data.length !== 0 ? (
          <table width="100%" style={{ marginTop: "2%" }}>
            <thead className="text-center table-active">
              <tr>
                <th id="tb">Attribute</th>
                <th id="tb">Description</th>
                <th id="tb">Data Type</th>
                <th id="tb">Length</th>
                <th id="tb">Key</th>
                <th id="tb">Reference</th>
                <th id="tb">Original_Position</th>
              </tr>
            </thead>

            <tbody>
              {this.state.data.map((item, index) => {
                return (
                  <tr>
                    <td id="tb">{item.COLUMN_NAME}</td>
                    <td id="tb">
                      <input
                        required
                        name="description"
                        value={item.description}
                        onChange={this.onChangeDescription(index)}
                        // placeholder="รายละเอียด (ต้องใส่)"
                        style={{ width: "100%" }}
                      />
                    </td>
                    <td id="tb" className="text-center">
                      {item.DATA_TYPE}
                    </td>
                    <td id="tb" className="text-center">
                      {item.DATA_TYPE === "numeric"
                        ? item.NUMERIC_PRECISION + "," + item.NUMERIC_SCALE
                        : item.CHARACTER_MAXIMUM_LENGTH}
                    </td>
                    <td id="tb">
                      <input
                        name="key"
                        value={item.key}
                        onChange={this.onChangeKey(index)}
                        // placeholder="PK,FK (เว้นว่างได้)"
                        style={{ width: "100%" }}
                      />
                    </td>
                    <td id="tb">
                      <input
                        name="ref"
                        value={item.ref}
                        onChange={this.onChangeRef(index)}
                        // placeholder="Ref. (เว้นว่างได้)"
                        style={{ width: "100%" }}
                      />
                    </td>
                    <td id="tb" className="text-center">
                      {item.ORDINAL_POSITION}
                    </td>
                  </tr>
                );
              })}
            </tbody>
            <Button
              variant="danger"
              style={{ marginTop: "10%" }}
              onClick={this.sendData}
            >
              Save Data
            </Button>
          </table>
        ) : (
          ""
        )}
      </div>
    );
  }
}

export default CreateDictionary;
