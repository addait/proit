import React from "react";
import { Row, Col, Button } from "react-bootstrap";
// import cookie from "js-cookie";
import axios from "axios";
import { REQUEST_URL } from "../../config";
import Swal from "sweetalert2";

class Edit extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      value: "",
      valueth: "",
      descriptiontb: "",
      description: ""
    };
  }

  componentDidMount = () => {
    this.loadTable();
  };

  loadTable = () => {
    axios
      .post(
        REQUEST_URL + "/loaddataedit",
        {
          id: this.props.match.params.table
        }
        // ,
        // {
        //   headers: { authorization: cookie.get("webcomputer") }
        // }
      )
      .then(response => {
        if (response.data.status) {
          this.setState({
            value: response.data.data[0].tablename,
            valueth: response.data.data[0].tablenameth,
            descriptiontb: response.data.data[0].descriptiontable,
            data: response.data.data
          });
        } else {
          this.props.history.push("/login");
        }
      });
  };

  onChangeTxt = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  onChangeDescription = index => e => {
    const newarr = this.state.data[index];
    newarr.description = e.target.value;
    const arr = [...this.state.data];
    arr.splice(index, 1, newarr);
    this.setState({
      data: arr
    });
  };

  onChangeKey = index => e => {
    const newarr = this.state.data[index];
    newarr.key = e.target.value;
    const arr = [...this.state.data];
    arr.splice(index, 1, newarr);
    this.setState({
      data: arr
    });
  };

  onChangeRef = index => e => {
    const newarr = this.state.data[index];
    newarr.ref = e.target.value;
    const arr = [...this.state.data];
    arr.splice(index, 1, newarr);
    this.setState({
      data: arr
    });
  };

  sendData = () => {
    if (this.state.valueth !== "" && this.state.descriptiontb !== "") {
      Swal.fire({
        title: "คุณต้องการแก้ไขข้อมูล ?",
        // text: "คุณไม่สามารถกู้คืนข้อมูลนี้กลับได้น่ะ",
        type: "warning",
        showCancelButton: true,
        confirmButtonText: "Yes, Edit Now !!",
        cancelButtonText: "No, Cancel !!",
        // reverseButtons: true
      }).then(result => {
        if (result.value) {
          axios
            .post(
              REQUEST_URL + "/editdatadic",
              {
                data: this.state.data,
                id: this.props.match.params.table,
                table: this.state.value,
                tableth: this.state.valueth,
                descriptiontb: this.state.descriptiontb,
                lastby: sessionStorage.getItem("user_id")
              }
              // ,
              // {
              //   headers: { authorization: cookie.get("webcomputer") }
              // }
            )
            .then(response => {
              if (response.data.status) {
                Swal.fire({
                  type: "success",
                  title: "แก้ไขข้อมูลสำเร็จ ...",
                  timer: 1200
                });
                setTimeout(() => {
                  this.props.history.push(
                    "/data/database/table/" +
                      this.props.match.params.database +
                      "/" +
                      this.props.match.params.id
                  );
                }, 1400);
              } else {
                Swal.fire({
                  type: "error",
                  title: "แก้ไขข้อมูลไม่สำเร็จ ...",
                  timer: 1200
                });
              }
            });
        } else if (result.dismiss === Swal.DismissReason.cancel) {
          Swal.fire({
            type: "error",
            title: "Cancelled ...",
            timer: 1200
          });
        }
      });
    } else {
      Swal.fire({
        type: "error",
        title: "กรอกข้อมูลให้ครบถ้วน",
        timer: 1200
      });
    }
  };

  render() {
    const { valueth, descriptiontb } = this.state;
    return (
      <div>
        <h1>DATABASE : {this.props.match.params.database}</h1>
        <h1>TABLE : {this.state.value}</h1>
        <Row>
          <Col md="12" style={{ marginTop: "2%" }}>
            <label>ชื่อตาราง (ภาษาไทย) : </label>
            <input
              required
              name="valueth"
              values={valueth}
              value={this.state.valueth}
              style={{ width: "50%" }}
              onChange={this.onChangeTxt}
            />
          </Col>
          <Col md="12" style={{ marginTop: "2%" }}>
            <label>คำอธิบาย (Description) : </label>
            <input
              required
              name="descriptiontb"
              values={descriptiontb}
              value={this.state.descriptiontb}
              type="text"
              style={{ width: "50%" }}
              onChange={this.onChangeTxt}
            />
          </Col>
        </Row>
        <table width="100%" style={{ marginTop: "2%" }}>
          <thead className="text-center table-active">
            <tr>
              <th id="tb">Attribute</th>
              <th id="tb">Description</th>
              <th id="tb">Data Type</th>
              <th id="tb">Length</th>
              <th id="tb">Key</th>
              <th id="tb">Reference</th>
              <th id="tb">Original_Position</th>
            </tr>
          </thead>

          <tbody>
            {this.state.data.map((item, index) => {
              return (
                <tr>
                  <td id="tb">{item.attribute}</td>
                  <td id="tb">
                    <input
                      required
                      name="description"
                      value={item.description}
                      onChange={this.onChangeDescription(index)}
                      // placeholder="รายละเอียด"
                      style={{ width: "100%" }}
                    />
                  </td>
                  <td id="tb" className="text-center">
                    {item.datatype}
                  </td>
                  <td id="tb" className="text-center">
                    {item.length}
                  </td>
                  <td id="tb">
                    <input
                      name="key"
                      value={item.key}
                      onChange={this.onChangeKey(index)}
                      // placeholder="PK,FK (เว้นว่างได้)"
                      style={{ width: "100%" }}
                    />
                  </td>
                  <td id="tb">
                    <input
                      name="ref"
                      value={item.ref}
                      onChange={this.onChangeRef(index)}
                      // placeholder="Ref. (เว้นว่างได้)"
                      style={{ width: "100%" }}
                    />
                  </td>
                  <td id="tb" className="text-center">
                    {item.original_position}
                  </td>
                </tr>
              );
            })}
          </tbody>
          <Button
            variant="danger"
            style={{ marginTop: "10%" }}
            onClick={this.sendData}
          >
            Save Data
          </Button>
        </table>
      </div>
    );
  }
}

export default Edit;
