import React from "react";
import { Button } from "react-bootstrap";
// import cookie from "js-cookie";
import axios from "axios";
import { REQUEST_URL } from "../../config";
import moment from "moment";
import Swal from "sweetalert2";

class Table extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      selection: "",
      selectiondb: "",
      test: this.props.match.params.database
    };
  }

  componentDidMount = () => {
    this.loadDataTable();
  };

  loadDataTable = () => {
    axios
      .post(
        REQUEST_URL + "/tableshow",
        {
          database: this.props.match.params.database,
          id: this.props.match.params.id
        }
        // ,
        // {
        //   headers: { authorization: cookie.get("webcomputer") }
        // }
      )
      .then(response => {
        if (response.data.status) {
          this.setState({
            data: response.data.data
          });
        }
      });
  };

  createDataDictionary = () => {
    this.props.history.push(
      "/data/database/table/" +
        this.props.match.params.database +
        "/" +
        this.props.match.params.id +
        "/createdictionary/" +
        this.props.match.params.database +
        "/" +
        this.props.match.params.id
    );
  };

  DeleteTB = id => e => {
    e.preventDefault();
    Swal.fire({
      title: "คุณต้องการลบข้อมูล ?",
      // text: "คุณไม่สามารถกู้คืนข้อมูลนี้กลับได้น่ะ",
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "Yes, Delete Now !!",
      cancelButtonText: "No, Cancel !!",
      // reverseButtons: true
    }).then(result => {
      if (result.value) {
        axios
          .post(
            REQUEST_URL + "/deletedatadic",
            {
              id: id
            }
            // ,
            // {
            //   headers: { authorization: cookie.get("webcomputer") }
            // }
          )
          .then(response => {
            if (response.data.status) {
              Swal.fire({
                type: "success",
                title: "ลบข้อมูลสำเร็จ ...",
                timer: 1200
              });
              this.loadDataTable();
            } else {
              Swal.fire({
                type: "error",
                title: "ลบข้อมูลไม่สำเร็จ ...",
                timer: 1200
              });
            }
          });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire({
          type: "error",
          title: "Cancelled ...",
          timer: 1200
        });
      }
    });
  };

  render() {
    return (
      <div>
        <h1>DATABASE : {this.props.match.params.database}</h1>
        <Button
          variant="success"
          style={{ margin: "20px" }}
          onClick={this.createDataDictionary}
        >
          Create Data Dictionary
        </Button>
        <table width="100%">
          <thead className="text-center table-active">
            <tr>
              <th id="tb">Docno</th>
              <th id="tb">No</th>
              <th id="tb">Table Name TH</th>
              <th id="tb">Table Name</th>
              <th id="tb">Description Table</th>
              <th id="tb">Remark</th>
              <th id="tb">Preby</th>
              <th id="tb">Predate</th>
              <th id="tb">Lastby</th>
              <th id="tb">Lastdate</th>
              <th id="tb">#</th>
              <th id="tb">#</th>
              <th id="tb">#</th>
            </tr>
          </thead>
          <tbody>
            {this.state.data.map(item => {
              return (
                <tr className="text-center">
                  <td id="tb">{item.docno}</td>
                  <td id="tb">{item.num}</td>
                  <td id="tb">{item.tablenameth}</td>
                  <td id="tb">{item.tablename}</td>
                  <td id="tb">{item.descriptiontable}</td>
                  <td id="tb">{item.remark}</td>
                  <td id="tb">{item.preby}</td>
                  <td id="tb">
                    {moment(item.predate)
                      .utc()
                      .format("DD/MM/YYYY HH:mm")}
                  </td>
                  <td id="tb">{item.lastby}</td>
                  <td id="tb">
                    {item.lastdate === null
                      ? ""
                      : moment(item.lastdate)
                          .utc()
                          .format("DD/MM/YYYY HH:mm")}
                  </td>
                  <td id="tb">
                    <Button
                      onClick={() =>
                        this.props.history.push(
                          "/data/database/table/" +
                            this.props.match.params.database +
                            "/" +
                            this.props.match.params.id +
                            "/edit/" +
                            this.props.match.params.database +
                            "/" +
                            item.docno
                        )
                      }
                    >
                      แก้ไข
                    </Button>
                  </td>
                  <td id="tb">
                    <Button
                      onClick={() =>
                        this.props.history.push(
                          "/data/database/table/" +
                            this.props.match.params.database +
                            "/" +
                            this.props.match.params.id +
                            "/view/" +
                            this.props.match.params.database +
                            "/" +
                            item.docno
                        )
                      }
                    >
                      ดู
                    </Button>
                  </td>
                  <td id="tb">
                    <Button onClick={this.DeleteTB(item.docno)}>ลบ</Button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Table;
