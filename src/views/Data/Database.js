import React from "react";
import { Button } from "react-bootstrap";
// import cookie from "js-cookie";
import axios from "axios";
import { REQUEST_URL } from "../../config";
import Swal from "sweetalert2";
import ModalDatabase from "./Modal/modalDatabase";
import moment from "moment";

class Database extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      selection: "",
      id: ""
    };
  }

  componentDidMount = () => {
    this.loadDatabase();
  };

  loadDatabase = () => {
    axios
      .post(
        REQUEST_URL + "/basename",
        {}
        // ,
        // { headers: { authorization: cookie.get("webcomputer") } }
      )
      .then(response => {
        if (response.data.status) {
          this.setState({
            data: response.data.data
          });
        } else {
          this.props.history.push("/login");
        }
      });
  };

  selectDatabase = (cursor, id) => {
    if (cursor === this.state.selection) {
      this.setState({
        selection: ""
      });
    } else {
      this.setState({
        selection: cursor,
        id: id
      });
    }
  };

  modalDatabaseShow = bool => {
    if (bool === false) {
      this.loadDatabase();
    }
    this.setState({
      modalDatabase: bool
    });
  };

  Table = () => {
    if (this.state.selection !== "") {
      this.props.history.push(
        "/data/database/table/" + this.state.selection + "/" + this.state.id
      );
    } else {
      Swal.fire({
        type: "error",
        title: "เลือกอย่างน้อย 1 รายการ",
        timer: 1200
      });
    }
  };

  render() {
    return (
      <div>
        <input width="100%" placeholder="search" />
        <Button
          variant="danger"
          style={{ margin: "20px" }}
          onClick={() =>
            this.setState({
              modalDatabase: true
            })
          }
        >
          Select Database
        </Button>
        <Button variant="dark" style={{ margin: "20px" }} onClick={this.Table}>
          Select Table
        </Button>
        <table width="100%">
          <thead className="text-center table-active">
            <tr>
              <th id="tb">Database Name</th>
              <th id="tb">Server</th>
              <th id="tb">Preby</th>
              <th id="tb">PreDate</th>
            </tr>
          </thead>
          <tbody>
            {this.state.data.map(item => {
              return (
                <tr
                  className={
                    this.state.selection === item.dbname
                      ? "table-danger text-center"
                      : "text-center"
                  }
                  onClick={() => this.selectDatabase(item.dbname, item.dataid)}
                >
                  <td id="tb">{item.dbname}</td>
                  <td id="tb">{item.ipserver}</td>
                  <td id="tb">{item.preby}</td>
                  <td id="tb">
                    {moment(item.predate)
                      .utc()
                      .format("DD/MM/YYYY HH:mm")}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <ModalDatabase
          Modal={this.state.modalDatabase}
          set={this.modalDatabaseShow}
        />
      </div>
    );
  }
}

export default Database;
