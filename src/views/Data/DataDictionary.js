import React from "react";
import { Row, Col, Dropdown } from "react-bootstrap";
import DropdownToggle from "react-bootstrap/DropdownToggle";
// import cookie from "js-cookie";
import axios from "axios";
import { REQUEST_URL } from "../../config";

class DataDictionary extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      DatabaseDrop: [],
      data: [],
      dataB: [],
      value: "",
      id: ""
    };
  }

  componentDidUpdate = (prevProps, prevState) => {
    if (prevState.value !== this.state.value) {
      this.loadDataDic();
    }
  };

  componentDidMount = () => {
    this.loadDatabase();
  };

  loadDatabase = () => {
    axios
      .post(
        REQUEST_URL + "/loadbasename",
        {}
        // ,
        // {
        //   headers: { authorization: cookie.get("webcomputer") }
        // }
      )
      .then(response => {
        if (response.data.status) {
          this.setState({
            DatabaseDrop: response.data.data
          });
        } else {
          this.props.history.push("/login");
        }
      });
  };

  loadDataDic = () => {
    axios
      .post(
        REQUEST_URL + "/loaddatadictionaryshow",
        {
          database: this.state.value,
          id: this.state.id
        }
        // ,
        // {
        //   headers: { authorization: cookie.get("webcomputer") }
        // }
      )
      .then(response => {
        if (response.data.status) {
          this.setState({
            data: [response.data.data],
            dataB: response.data.data
          });

          // var groupBy = function(xs, res) {
          //   return item.reduce(function(rv, x) {
          //     (rv[x[res]] = rv[x[res]] || []).push(x);
          //     return rv;
          //   }, {});
          // };

          // console.log(groupBy([], "docno"));

          // console.log(response.data.data);
          // for(var y=0; y<response.data.dataH.length; y++){
          //   for (var x = 0; x < response.data.data.length; x++) {
          //     if (response.data.dataH[y].docno === response.data.data[x].docno) {
          //       console.log(response.data.data[x]);
          //     }
          //   }
          // }
        } else {
          this.props.history.push("/login");
        }
      });
  };

  onChangeDrop = id => e => {
    this.setState({
      value: e.currentTarget.textContent,
      id: id
    });
  };

  render() {
    return (
      <div>
        <h1>DATA DICTIONARY</h1>
        <Row>
          <Col md="2">
            <Dropdown
              style={{
                display: "flex",
                justifyContent: "flex-start",
                marginTop: "1%"
              }}
              onChange={this.onChangeDrop.bind(this)}
            >
              <DropdownToggle
                variant="success"
                value={this.state.value}
                style={{ fontSize: "26px" }}
              >
                {this.state.value === "" ? "เลือก Database" : this.state.value}
              </DropdownToggle>
              <Dropdown.Menu
                style={{ overflowY: "auto", height: "200px" }}
              >
                {this.state.DatabaseDrop.map(item => {
                  return (
                    <Dropdown.Item>
                      <div onClick={this.onChangeDrop(item.dataid)}>
                        {item.dbname}
                      </div>
                    </Dropdown.Item>
                  );
                })}
              </Dropdown.Menu>
            </Dropdown>
          </Col>
        </Row>

        <Row>
          {this.state.data.map(item => {
            var groupBy = function(xs, res) {
              return item.reduce(function(rv, x) {
                (rv[x[res]] = rv[x[res]] || []).push(x);
                return rv;
              }, {});
            };
            let a = groupBy([], "docno");
            {
              let table = Object.values(a).map((item, index) => {
                return (
                  <table width="100%" style={{ marginTop: "2%" }}>
                    <thead>
                      <label>
                        ตารางที่ {item[index].num} : {item[index].tablename}{" "}
                        (SQL)
                      </label>
                      <br />
                      <label>ชื่อตาราง : {item[index].tablenameth}</label>
                      <br />
                      <label>คำอธิบาย : {item[index].descriptiontable}</label>
                      <tr>
                        <th id="tb" className="text-center table-active">
                          Attribute
                        </th>
                        <th id="tb" className="text-center table-active">
                          Description
                        </th>
                        <th id="tb" className="text-center table-active">
                          Data Type
                        </th>
                        <th id="tb" className="text-center table-active">
                          Length
                        </th>
                        <th id="tb" className="text-center table-active">
                          Key
                        </th>
                        <th id="tb" className="text-center table-active">
                          Reference
                        </th>
                        <th id="tb" className="text-center table-active">
                          Original_Position
                        </th>
                      </tr>
                    </thead>
                    {Object.values(a)[index].map(item => {
                      return (
                        <tbody>
                          <tr>
                            <td id="tb" className="text-center" width="20%">
                              {item.attribute}
                            </td>
                            <td id="tb" className="text-center" width="30%">
                              {item.description}
                            </td>
                            <td id="tb" className="text-center">
                              {item.datatype}
                            </td>
                            <td id="tb" className="text-center">
                              {item.length}
                            </td>
                            <td id="tb" className="text-center">
                              {item.key}
                            </td>
                            <td id="tb" className="text-center">
                              {item.ref}
                            </td>
                            <td id="tb" className="text-center">
                              {item.original_position}
                            </td>
                          </tr>
                        </tbody>
                      );
                    })}
                  </table>
                );
              });
              return table;
            }

            // let count = 0;
            // for (var x = 0; x < Object.values(a).length; x++) {
            //   if (count === x) {
            //     count++;
            //     var table = Object.values(a).map(item => {
            //       return (
            //         <table width="100%" style={{ marginTop: "2%" }}>
            //           <thead className="text-center table-active">
            //             <label>Table : {item[x].tablename}</label>
            //             <tr>
            //               <th id="tb">Attribute</th>
            //               <th id="tb">Description</th>
            //               <th id="tb">Data Type</th>
            //               <th id="tb">Length</th>
            //               <th id="tb">Key</th>
            //               <th id="tb">Reference</th>
            //               <th id="tb">Original_Position</th>
            //             </tr>
            //           </thead>
            //           <tbody>
            //             {Object.values(a)[x].map(item => {
            //               console.log(item)
            //               return (
            //                 <tr>
            //                   <td id="tb" className="text-center">
            //                     {item.attribute}
            //                   </td>
            //                   <td id="tb" className="text-center">
            //                     {item.description}
            //                   </td>
            //                   <td id="tb" className="text-center">
            //                     {item.datatype}
            //                   </td>
            //                   <td id="tb" className="text-center">
            //                     {item.length}
            //                   </td>
            //                   <td id="tb" className="text-center">
            //                     {item.key}
            //                   </td>
            //                   <td id="tb" className="text-center">
            //                     {item.ref}
            //                   </td>
            //                   <td id="tb" className="text-center">
            //                     {item.original_position}
            //                   </td>
            //                 </tr>
            //               );
            //             })}
            //           </tbody>
            //         </table>
            //       );
            //     });
            //     return table;
            //   }
            // }
          })}
        </Row>
      </div>
    );
  }
}

export default DataDictionary;
