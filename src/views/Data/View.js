import React from "react";
import { Row, Col } from "react-bootstrap";
// import cookie from "js-cookie";
import axios from "axios";
import { REQUEST_URL } from "../../config";

class View extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      value: "",
      valueth: "",
      descriptiontb: "",
      description: ""
    };
  }

  componentDidMount = () => {
    this.loadTable();
  };

  loadTable = () => {
    axios
      .post(
        REQUEST_URL + "/loaddataedit",
        {
          id: this.props.match.params.table
        }
        // ,
        // {
        //   headers: { authorization: cookie.get("webcomputer") }
        // }
      )
      .then(response => {
        if (response.data.status) {
          this.setState({
            value: response.data.data[0].tablename,
            valueth: response.data.data[0].tablenameth,
            descriptiontb: response.data.data[0].descriptiontable,
            data: response.data.data
          });
        } else {
          this.props.history.push("/login");
        }
      });
  };

  render() {
    return (
      <div>
        <h1>DATABASE : {this.props.match.params.database}</h1>
        <h1>TABLE : {this.state.value}</h1>
        <Row>
          <Col md="12" style={{ marginTop: "2%" }}>
            <label>ชื่อตาราง (ภาษาไทย) : {this.state.valueth}</label>
          </Col>
          <Col md="12" style={{ marginTop: "1%" }}>
            <label>คำอธิบาย (Description) : {this.state.descriptiontb} </label>
          </Col>
        </Row>
        <table width="100%" style={{ marginTop: "2%" }}>
          <thead className="text-center table-active">
            <tr>
              <th id="tb">Attribute</th>
              <th id="tb">Description</th>
              <th id="tb">Data Type</th>
              <th id="tb">Length</th>
              <th id="tb">Key</th>
              <th id="tb">Reference</th>
              <th id="tb">Original_Position</th>
            </tr>
          </thead>

          <tbody>
            {this.state.data.map((item, index) => {
              return (
                <tr>
                  <td id="tb" className="text-center">
                    {item.attribute}
                  </td>
                  <td id="tb">{item.description}</td>
                  <td id="tb" className="text-center">
                    {item.datatype}
                  </td>
                  <td id="tb" className="text-center">
                    {item.length}
                  </td>
                  <td id="tb" className="text-center">
                    {item.key}
                  </td>
                  <td id="tb">{item.ref}</td>
                  <td id="tb" className="text-center">
                    {item.original_position}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default View;
