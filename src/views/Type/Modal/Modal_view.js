import React from "react";
import { Modal, Row, Col, Button } from "react-bootstrap";
import { REQUEST_URL } from "../../../config";
import Cookies from "js-cookie";
import Axios from "axios";
import Swal from "sweetalert2";
import moment from "moment";

class ModalEdit extends React.Component {
  state = {
    typeID: "",
    typeName: ""
  };

  componentDidUpdate = prevProps => {
    if (prevProps.typeID !== this.props.typeID) {
      if (this.props.typeID !== "") {
        Axios.post(
          REQUEST_URL + "/type/datatypeedit",
          {
            typeID: this.props.typeID
          },
          { headers: { authorization: Cookies.get("webcomputer") } }
        ).then(response => {
          if (response.data.status) {
            this.setState({
              typeID: response.data.data[0].typeID,
              typeName: response.data.data[0].typename,
              preby: response.data.data[0].preby,
              predate: response.data.data[0].predate
            });
          } else {
            this.props.history.push("/login");
          }
        });
      }
    }
  };

  modalClose = bool => e => {
    if (!bool) {
      this.setState({
        inputsearchx: ""
      });
    }
    this.props.set(bool);
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  render() {
    const { typeID, typeName } = this.state;
    return (
      <div>
        <Modal
          size="lg"
          show={this.props.Modal}
          onHide={this.modalClose(false)}
        >
          <Modal.Header closeButton id="fontthai" style={{ fontSize: "22px" }}>
            มุมมองข้อมูล
          </Modal.Header>
          <Modal.Body>
            <div style={{ display: "flex", justifyContent: "flex-start" }}>
              <Row>
                <Col md="12">
                  <label style={{ width: "100px" }}>รหัสประเภท : </label>
                  <input
                    name="typeID"
                    value={this.state.typeID}
                    values={typeID}
                    onChange={this.handleChange}
                    disabled
                  />
                </Col>
                <Col md="12" style={{ marginTop: "2%" }}>
                  <label style={{ width: "100px" }}>ชื่อประเภท : </label>
                  <input
                    name="typeName"
                    value={this.state.typeName}
                    values={typeName}
                    onChange={this.handleChange}
                    style={{ width: "80%" }}
                    disabled
                  />
                </Col>
              </Row>
            </div>
          </Modal.Body>
          <Modal.Footer style={{ display: "flex", justifyContent: "center" }}>
            <Button
              variant="secondary"
              onClick={this.modalClose(false)}
              id="btnthai"
            >
              ปิดหน้านี้
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default ModalEdit;
