import React from "react";
import { withRouter } from "react-router-dom";
import {
  Card,
  CardBody,
  Col,
  Row,
  Button,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon
} from "reactstrap";
import Spinner from "react-spinkit";
import Axios from "axios";
import Cookies from "js-cookie";
import { REQUEST_URL } from "../../config";
import moment from "moment";
import Swal from "sweetalert2";
import "../../index.css";
import numeral from "numeral";
import ModalAdd from "./Modal/Modal_add";
import ModalEdit from "./Modal/Modal_edit";
import ModalView from "./Modal/Modal_view";

class MainType extends React.Component {
  state = {
    data: [],
    filterData: [],
    modal: false,
    LoadData: false,
    inputsearch: "",
    selection: ""
  };

  componentDidMount() {
    this.showData();
    // this.chkPermission();
  }

  chkPermission = async () => {
    await Axios.post(
      REQUEST_URL + "/users/chkpermission",
      {
        user: Cookies.get("webcomputer"),
        file: "F02"
      },
      {
        headers: { authorization: Cookies.get("webcomputer") }
      }
    ).then(res => {
      if (res.data.success) {
        this.setState({
          act_open: res.data.data[0].act_open,
          act_add: res.data.data[0].act_add,
          act_edit: res.data.data[0].act_edit,
          act_view: res.data.data[0].act_view,
          act_print: res.data.data[0].act_print
        });
      } else {
        this.props.history.push("/login");
      }
    });
  };

  showData = async () => {
    await Axios.post(
      REQUEST_URL + "/type/datatype",
      {},
      {
        headers: { authorization: Cookies.get("webcomputer") }
      }
    ).then(response => {
      if (response.data.status) {
        // console.log(response.data.data);
        this.setState({
          filterData: response.data.data
        });
      } else {
        this.props.history.push("/login");
      }
    });
  };

  searchHandle = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
    this.searchNoButton(e.target.value);
  };

  searchNoButton = txt => {
    let ggez = txt;
    var filtered = this.state.filterData.filter(function(x) {
      return (
        x.typeID.match(new RegExp("^" + ggez === null ? "" : ggez, "i")) ||
        x.typename.match(new RegExp("^" + ggez === null ? "" : ggez, "i"))
      );
    });
    this.setState({ searchx: filtered.slice(0, 200) });
  };

  modalAdd = bool => {
    if (!bool) {
      this.showData();
    }
    this.setState({
      modaladd: bool
    });
  };

  modalEdit = bool => {
    if (!bool) {
      this.showData();
    }
    this.setState({
      modaledit: bool
    });
  };

  modalView = bool => {
    if (!bool) {
      this.showData();
    }
    this.setState({
      modalview: bool
    });
  };

  selectTypeID = typeid => {
    if (typeid === this.state.selection) {
      this.setState({
        selection: ""
      });
    } else {
      this.setState({
        selection: typeid
      });
    }
  };

  deleteData = () => {
    Swal.fire({
      title: "คุณต้องการลบข้อมูลใช่ไหม ?",
      text: this.state.selection,
      type: "warning",
      showCancelButton: true,
      confirmButtonText: "ใช่",
      cancelButtonText: "ไม่ใช่"
    }).then(result => {
      if (result.value) {
        Axios.post(
          REQUEST_URL + "/type/deletedatatype",
          { typeID: this.state.selection },
          {
            headers: { authorization: Cookies.get("webcomputer") }
          }
        ).then(response => {
          if (response.data.status) {
            Swal.fire({
              type: "success",
              title: "ลบข้อมูลสำเร็จ...",
              timer: 1200,
              showConfirmButton: false
            });
            this.showData();
            this.setState({
              inputsearch: ""
            });
          } else {
            Swal.fire({
              type: "error",
              title: "ลบข้อมูลไม่สำเร็จ...",
              timer: 1200,
              showConfirmButton: false
            });
          }
        });
      } else if (result.dismiss === Swal.DismissReason.cancel) {
        Swal.fire({
          type: "error",
          title: "ยกเลิกการลบข้อมูล",
          timer: 1200,
          showConfirmButton: false
        });
      }
    });
  };

  render() {
    const { inputsearch } = this.state;
    return this.state.filterData.length === 0 ? (
      <div className="animated fadeIn pt-5 text-center">
        <Spinner name="three-bounce" />{" "}
      </div>
    ) : (
      <div className="animated fadeIn">
        <Row>
          <Col xs={12}>
            <Card>
              <CardBody>
                <FormGroup row>
                  <Col md="3">
                    <InputGroup>
                      <Input
                        type="text"
                        name="inputsearch"
                        values={inputsearch}
                        placeholder="ค้นหา"
                        onChange={this.searchHandle}
                      />
                    </InputGroup>
                  </Col>
                  <div
                    style={{ marginRight: "1%" }}
                    onClick={() =>
                      this.setState({
                        modaladd: true
                      })
                    }
                  >
                    <Button color="success">เพิ่มข้อมูล</Button>
                  </div>
                  <div
                    style={{ marginRight: "1%" }}
                    onClick={() =>
                      this.state.selection === ""
                        ? Swal.fire({
                            type: "error",
                            title: "เลือกอย่างน้อย 1 รายการ"
                          })
                        : this.setState({
                            modaledit: true
                          })
                    }
                  >
                    <Button color="warning">แก้ไขข้อมูล</Button>
                  </div>
                  <div
                    style={{ marginRight: "1%" }}
                    onClick={() =>
                      this.state.selection === ""
                        ? Swal.fire({
                            type: "error",
                            title: "เลือกอย่างน้อย 1 รายการ",
                            timer: 1200
                          })
                        : this.setState({
                            modalview: true
                          })
                    }
                  >
                    <Button color="info">มุมมอง</Button>
                  </div>
                  <div style={{ marginRight: "1%" }}>
                    <Button
                      color="danger"
                      onClick={() =>
                        this.state.selection === ""
                          ? Swal.fire({
                              type: "error",
                              title: "เลือกอย่างน้อย 1 รายการ",
                              timer: 1200
                            })
                          : this.deleteData()
                      }
                    >
                      ลบข้อมูล
                    </Button>
                  </div>
                </FormGroup>
                {}
                <table>
                  <thead>
                    <tr style={{ textAlign: "center" }}>
                      <th id="tb" width="100px">
                        ประเภท
                      </th>
                      <th id="tb" width="300px">
                        รายการประเภท
                      </th>
                      <th id="tb" width="150px">
                        จำนวน
                      </th>
                      <th id="tb" width="150px">
                        รวมมูลค่า (บาท)
                      </th>
                      <th id="tb" width="150px">
                        ผู้บันทึก
                      </th>
                      <th id="tb" width="150px">
                        วันที่บันทึก
                      </th>
                      <th id="tb" width="150px">
                        ผู้แก้ไข
                      </th>
                      <th id="tb" width="150px">
                        วันที่แก้ไข
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.inputsearch === ""
                      ? this.state.filterData.map((item, index) => {
                          return (
                            <tr
                              className={
                                this.state.selection === item.typeID
                                  ? "table-primary text-center"
                                  : ""
                              }
                              key={index}
                              onClick={() => this.selectTypeID(item.typeID)}
                            >
                              <td id="tb" style={{ textAlign: "center" }}>
                                {item.typeID}
                              </td>
                              <td id="tb">{item.typename}</td>
                              <td id="tb" style={{ textAlign: "center" }}>
                                {item.quantity}
                              </td>
                              <td id="tb" style={{ textAlign: "right" }}>
                                {item.priceall !== null
                                  ? numeral(item.priceall).format("0,0.00")
                                  : ""}
                              </td>
                              <td id="tb" style={{ textAlign: "center" }}>
                                {item.preby}
                              </td>
                              <td id="tb" style={{ textAlign: "center" }}>
                                {moment(item.predate).format("DD/MM/YYYY")}
                              </td>
                              <td id="tb" style={{ textAlign: "center" }}>
                                {item.lastby}
                              </td>
                              <td id="tb" style={{ textAlign: "center" }}>
                                {item.lastdate !== null
                                  ? moment(item.lastdate).format("DD/MM/YYYY")
                                  : ""}
                              </td>
                            </tr>
                          );
                        })
                      : this.state.searchx.map((item, index) => {
                          return (
                            <tr
                              className={
                                this.state.selection === item.typeID
                                  ? "table-primary text-center"
                                  : ""
                              }
                              key={index}
                              onClick={() => this.selectTypeID(item.typeID)}
                            >
                              <td id="tb" style={{ textAlign: "center" }}>
                                {item.typeID}
                              </td>
                              <td id="tb">{item.typename}</td>
                              <td id="tb" style={{ textAlign: "center" }}>
                                {item.quantity}
                              </td>
                              <td id="tb" style={{ textAlign: "right" }}>
                                {item.priceall !== null
                                  ? numeral(item.priceall).format("0,0.00")
                                  : ""}
                              </td>
                              <td id="tb" style={{ textAlign: "center" }}>
                                {item.preby}
                              </td>
                              <td id="tb" style={{ textAlign: "center" }}>
                                {moment(item.predate).format("DD/MM/YYYY")}
                              </td>
                              <td id="tb" style={{ textAlign: "center" }}>
                                {item.lastby}
                              </td>
                              <td id="tb" style={{ textAlign: "center" }}>
                                {item.lastdate !== null
                                  ? moment(item.lastdate).format("DD/MM/YYYY")
                                  : ""}
                              </td>
                            </tr>
                          );
                        })}
                  </tbody>
                </table>
              </CardBody>
            </Card>
          </Col>
        </Row>
        <ModalAdd Modal={this.state.modaladd} set={this.modalAdd} />
        <ModalEdit
          Modal={this.state.modaledit}
          set={this.modalEdit}
          typeID={this.state.selection}
        />
        <ModalView
          Modal={this.state.modalview}
          set={this.modalView}
          typeID={this.state.selection}
        />
      </div>
    );
  }
}

export default withRouter(MainType);
