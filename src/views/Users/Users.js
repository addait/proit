import React from "react";
import { withRouter } from "react-router-dom";
import {
  Card,
  CardBody,
  Col,
  Row,
  Table,
  Button,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon
} from "reactstrap";
import Axios from "axios";
import { REQUEST_URL } from "../../config";
import moment from "moment";
import Cookies from "js-cookie";
import Swal from "sweetalert2";
// import { dateFormat } from "../../Module";

class Users extends React.Component {
  state = {
    data: [],
    filterData: []
  };

  componentDidMount() {
    this.showData();
  }

  onChangeSearch = e => {
    let input = e.target.value;
    let temp = this.state.data.filter(item => {
      return (
        item.userID.toUpperCase().match(input.toUpperCase()) ||
        item.sname.toUpperCase().match(input.toUpperCase())
      );
    });
    this.setState({ filterData: temp });
  };

  showData = async () => {
    await Axios.get(REQUEST_URL + "/users", {
      headers: { authorization: Cookies.get("webcomputer") }
    })
      .then(res => {
        if (res.data.status) {
          this.setState({ data: res.data.data, filterData: res.data.data });
        } else {
          this.props.history.push("/login");
        }
      })
      .catch(err => console.log(err));
  };

  deleteData = async userID => {
    Swal.fire({
      title: "คุณต้องการลบ?",
      text: "UserID: " + userID + " ใช่หรือไม่!",
      type: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes"
    }).then(async result => {
      if (result.value) {
        await Axios.get(REQUEST_URL + "/users/delete/" + userID.toUpperCase(), {
          headers: { authorization: Cookies.get("webcomputer") }
        })
          .then(res => {
            if (res.data.status) {
              Swal.fire("ลบข้อมูลเรียบร้อย!", "", "success");
              this.showData();
            } else {
              // this.props.history.push("/login");
              Swal.fire("ลบข้อมูลไม่สำเร็จ!", "", "error");
            }
          })
          .catch(err => console.log(err));
      }
    });
  };

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs={12}>
            <Card>
              <CardBody>
                <FormGroup row>
                  <Col md="3">
                    <InputGroup>
                      <Input
                        type="text"
                        name="txtSearch"
                        placeholder="ค้นหา"
                        onChange={this.onChangeSearch}
                        autoFocus
                      />
                      <InputGroupAddon addonType="append">
                        <Button type="button" block outline color="secondary">
                          <i className="fa fa-search"></i>
                        </Button>
                      </InputGroupAddon>
                    </InputGroup>
                  </Col>

                  <Col md="2">
                    <Button
                      color="success"
                      onClick={() => this.props.history.push("/users/new")}
                    >
                      <i className="fa fa-plus" /> เพิ่ม
                    </Button>
                  </Col>
                </FormGroup>

                <Table hover bordered responsive size="sm">
                  <thead className="thead-light">
                    <tr align="center">
                      <th scope="col" colSpan="2">
                        Level
                      </th>
                      <th scope="col">User ID</th>
                      <th scope="col">ชื่อ-นามสกุล</th>
                      <th scope="col">ตำแหน่ง</th>
                      <th scope="col">แผนก</th>
                      <th scope="col" colSpan="2">
                        สิทธิเข้าใช้
                      </th>
                      <th scope="col">เข้าใช้ล่าสุด</th>
                      <th scope="col">IP Address</th>
                      <th scope="col">วันที่บันทึก</th>
                      <th scope="col">ผู้บันทึก</th>
                      <th scope="col">วันที่แก้ไข</th>
                      <th scope="col">ผู้แก้ไข</th>
                      <th scope="col" />
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.filterData.length ? (
                      this.state.filterData.map((value, i) => {
                        return (
                          <tr key={i} style={{ whiteSpace: "nowrap" }}>
                            <td align="center">
                              {value.userLevel === "A" ? (
                                <img
                                  src={`${process.env.PUBLIC_URL}/img/admin.png`}
                                  className="img-fluid"
                                  alt="Admin"
                                />
                              ) : (
                                <img
                                  src={`${process.env.PUBLIC_URL}/img/users.png`}
                                  className="img-fluid"
                                  alt="User"
                                />
                              )}{" "}
                            </td>
                            <td>
                              {value.userLevel === "A" ? "Admin" : "User"}
                            </td>
                            <th scope="row">{value.userID}</th>
                            <td>{value.sname}</td>
                            <td>{value.post}</td>
                            <td>{value.depname}</td>
                            <td align="center">
                              {value.sta_usr ? (
                                <i className="fa fa-ban" />
                              ) : (
                                <i className="fa fa-check" />
                              )}
                            </td>
                            <td>{value.sta_usr ? "ปฏิเสธ" : "อนุญาติ"}</td>
                            <td align="center">
                              {value.lastlogin !== null
                                ? moment(value.lastlogin)
                                    .utc(+7)
                                    .format("DD/MM/YYYY HH:mm:ss")
                                : ""}
                            </td>
                            <td>{value.ip_login}</td>
                            <td align="center">
                              {moment(value.predate)
                                .utc(+7)
                                .format("DD/MM/YYYY HH:mm:ss")}
                            </td>
                            <td>{value.preby}</td>
                            <td align="center">
                              {value.lastdate !== null
                                ? moment(value.lastdate)
                                    .utc(+7)
                                    .format("DD/MM/YYYY HH:mm:ss")
                                : ""}
                            </td>
                            <td>{value.lastby}</td>
                            <td align="center">
                              <Button
                                className="mr-1"
                                color="warning"
                                size="sm"
                                onClick={() =>
                                  this.props.history.push(
                                    "/users/" + value.userID
                                  )
                                }
                              >
                                <i className="fa fa-edit" />
                              </Button>
                              <Button
                                color="danger"
                                size="sm"
                                onClick={() => this.deleteData(value.userID)}
                              >
                                <i className="fa fa-trash" />
                              </Button>
                            </td>
                          </tr>
                        );
                      })
                    ) : (
                      <tr>
                        <td colSpan="13" align="center">
                          ไม่พบข้อมูล
                        </td>
                      </tr>
                    )}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default withRouter(Users);
