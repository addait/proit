import React from "react";
import Axios from "axios";
import {
  Card,
  CardBody,
  Col,
  Row,
  Table,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
  FormFeedback
} from "reactstrap";
import Swal from "sweetalert2";
import { REQUEST_URL } from "../../config";
import Cookies from "js-cookie";

class AeUsers extends React.Component {
  state = {
    data: [],
    txtName: "",
    txtUserName: "",
    txtPasswd: "",
    txtDepname: "",
    txtPost: "",
    rdlevel: "U",
    isLoading: false,
    valid: false,
    invalid: false,
    addby: Cookies.get("webcomputer").split(":")
  };

  componentDidMount = async () => {
    await Axios.get(REQUEST_URL + "/files", {
      headers: { authorization: Cookies.get("webcomputer") }
    })
      .then(res => {
        if (res.data.status) {
          const newArray = res.data.data.map(value => {
            return {
              ...value,
              act_open: false,
              act_view: false,
              act_add: false,
              act_edit: false,
              act_delete: false,
              act_copy: false,
              act_print: false,
              act_other: false
            };
          });
          this.setState({ data: newArray });
        } else {
          this.props.history.push("/login");
        }
      })
      .catch(err => console.log(err));
  };

  showData = () => {
    let unlock = `${process.env.PUBLIC_URL}/img/unlock_green.png`;
    let lock = `${process.env.PUBLIC_URL}/img/lock_red.png`;
    let rows = Object.values(this.state.data).map((row, i) => (
      <tr key={i}>
        <th scope="row" align="center">
          {i + 1}
        </th>
        <td>{row.fileID}</td>
        <td>{row.filename}</td>
        <td align="right">0</td>
        <td />
        <td align="center">
          <img
            src={row.act_open ? unlock : lock}
            alt="Open"
            className="img-fluid"
            onClick={this.setPermiss("open", row.act_open, i)}
          />
        </td>
        <td align="center">
          <img
            src={row.act_view ? unlock : lock}
            alt="View"
            className="img-fluid"
            onClick={this.setPermiss("view", row.act_view, i)}
          />
        </td>
        <td align="center">
          <img
            src={row.act_add ? unlock : lock}
            alt="Add"
            className="img-fluid"
            onClick={this.setPermiss("add", row.act_add, i)}
          />
        </td>
        <td align="center">
          <img
            src={row.act_edit ? unlock : lock}
            alt="Edit"
            className="img-fluid"
            onClick={this.setPermiss("edit", row.act_edit, i)}
          />
        </td>
        <td align="center">
          <img
            src={row.act_delete ? unlock : lock}
            alt="Delete"
            className="img-fluid"
            onClick={this.setPermiss("delete", row.act_delete, i)}
          />
        </td>
        <td align="center">
          <img
            src={row.act_copy ? unlock : lock}
            alt="Copy"
            className="img-fluid"
            onClick={this.setPermiss("copy", row.act_copy, i)}
          />
        </td>
        <td align="center">
          <img
            src={row.act_print ? unlock : lock}
            alt="Print"
            className="img-fluid"
            onClick={this.setPermiss("print", row.act_print, i)}
          />
        </td>
        <td align="center">
          <img
            src={row.act_other ? unlock : lock}
            alt="other"
            className="img-fluid"
            onClick={this.setPermiss("other", row.act_other, i)}
          />
        </td>
      </tr>
    ));
    return rows;
  };

  setPermiss = (field, bln, index) => e => {
    const newArray = this.state.data[index];

    switch (field) {
      case "open":
        newArray.act_open = !bln;
        break;

      case "view":
        newArray.act_view = !bln;
        break;

      case "add":
        newArray.act_add = !bln;
        break;

      case "edit":
        newArray.act_edit = !bln;
        break;

      case "delete":
        newArray.act_delete = !bln;
        break;

      case "copy":
        newArray.act_copy = !bln;
        break;

      case "print":
        newArray.act_print = !bln;
        break;

      case "other":
        newArray.act_other = !bln;
        break;

      default:
    }

    const arr = [...this.state.data];
    arr.splice(index, 1, newArray);
    this.setState({ data: arr });
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });

    if (e.target.name === "txtUserName") {
      this.checkUserID(e.target.value);
    }
  };

  checkUserID = async userID => {
    if (userID === "") {
      return this.setState({ valid: false, invalid: false });
    }

    await Axios.get(REQUEST_URL + "/users/check/" + userID.toUpperCase(), {
      headers: { authorization: Cookies.get("webcomputer") }
    })
      .then(res => {
        if (res.data.status) {
          if (res.data.data.length === 0) {
            this.setState({ valid: true, invalid: false });
          } else {
            this.setState({ valid: false, invalid: true });
          }
        } else {
          this.props.history.push("/login");
        }
      })
      .catch(err => console.log(err));
  };

  onSubmit = e => {
    this.setState({ isLoading: true });
    e.preventDefault();
    if (this.state.valid === false) {
      this.setState({ isLoading: false });
      return Swal.fire(
        "มี UserID: " + this.state.txtUserName.toUpperCase() + " แล้วในระบบ",
        "โปรดระบบุ UserID ใหม่",
        "error"
      );
    }
    Swal.fire({
      title: "คุณต้องการ?",
      text: "บันทึกข้อมูลใช่หรือไม่",
      type: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes"
    }).then(async result => {
      if (result.value) {
        this.saveData();
      }
    });
    this.setState({ isLoading: false });
  };

  saveData = async () => {
    await Axios.post(
      REQUEST_URL + "/users/add",
      {
        Name: this.state.txtName,
        UserID: this.state.txtUserName,
        Passwd: this.state.txtPasswd,
        DepName: this.state.txtDepname,
        Post: this.state.txtPost,
        Level: this.state.rdlevel,
        detail: this.state.data,
        addby: this.state.addby[0]
      },
      { headers: { authorization: Cookies.get("webcomputer") } }
    )

      .then(res => {
        console.log(res.data);
        if (res.data.status) {
          Swal.fire("บันทึกข้อมูลสำเร็จ", "", "success");
          this.props.history.push("/users");
        } else {
          // this.props.history.push("/login");
          Swal.fire("บันทึกข้อมูลไม่สำเร็จ", "", "error");
        }
      })
      .catch(err => {
        console.log(err);
      });
  };

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col lg={12}>
            <Form onSubmit={e => this.onSubmit(e)} className="form-horizontal">
              <Card>
                <CardBody>
                  <FormGroup row>
                    <Col md="1">
                      <Label>Level</Label>
                    </Col>

                    <Col md="3">
                      <FormGroup check inline>
                        <Input
                          className="form-check-input"
                          type="radio"
                          id="rduser"
                          name="rdlevel"
                          value="U"
                          onChange={e => this.handleChange(e)}
                          defaultChecked={true}
                        />
                        <Label
                          className="form-check-label"
                          check
                          htmlFor="rduser"
                        >
                          User
                        </Label>
                      </FormGroup>
                      <FormGroup check inline>
                        <Input
                          className="form-check-input"
                          type="radio"
                          id="rdadmin"
                          name="rdlevel"
                          value="A"
                          onChange={e => this.handleChange(e)}
                        />
                        <Label
                          className="form-check-label"
                          check
                          htmlFor="rdadmin"
                        >
                          Administrator
                        </Label>
                      </FormGroup>
                    </Col>

                    <Col md="1">
                      <Label>ชื่อ-นามสกุล</Label>
                    </Col>

                    <Col xs="12" md="4">
                      <Input
                        type="text"
                        id="txtName"
                        name="txtName"
                        onChange={e => this.handleChange(e)}
                        required
                      />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="1">
                      <Label>User Name</Label>
                    </Col>
                    <Col xs="12" md="3">
                      <Input
                        type="text"
                        valid={this.state.valid}
                        invalid={this.state.invalid}
                        style={{ textTransform: "uppercase" }}
                        id="txtUserName"
                        name="txtUserName"
                        onChange={e => this.handleChange(e)}
                        required
                      />
                      <FormFeedback>
                        มี UserID: {this.state.txtUserName.toUpperCase()}{" "}
                        แล้วในระบบ
                      </FormFeedback>
                    </Col>
                    <Col md="1">
                      <Label>แผนก</Label>
                    </Col>
                    {/* <Col  md="1">
                                    <Input type="text" id="txtDepcode" name="txtDepcode" 
                                     />
                                </Col> */}
                    <Col xs="12" md="4">
                      <Input
                        type="text"
                        id="txtDepname"
                        name="txtDepname"
                        onChange={e => this.handleChange(e)}
                      />
                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="1">
                      <Label for="text">Password</Label>
                    </Col>
                    <Col xs="12" md="3">
                      <Input
                        type="password"
                        id="txtPasswd"
                        name="txtPasswd"
                        autoComplete="new-password"
                        onChange={e => this.handleChange(e)}
                        required
                      />
                    </Col>

                    <Col md="1">
                      <Label for="text">ตำแหน่ง</Label>
                    </Col>

                    <Col xs="12" md="4">
                      <Input
                        type="text"
                        id="txtPost"
                        name="txtPost"
                        onChange={e => this.handleChange(e)}
                      />
                    </Col>
                  </FormGroup>

                  <Table size="sm" hover bordered responsive>
                    <thead className="thead-light">
                      <tr align="center">
                        <th align="center">#</th>
                        <th>รหัสแฟ้ม</th>
                        <th>ชื่อแฟ้ม</th>
                        <th>เปิดใช้</th>
                        <th>เข้าใช้ลาสุด</th>
                        <th>เปิด</th>
                        <th>มุมมอง</th>
                        <th>เพิ่ม</th>
                        <th>แก้ไข</th>
                        <th>ลบ</th>
                        <th>คัดลอก</th>
                        <th>พิมพ์</th>
                        <th>อื่น ๆ</th>
                      </tr>
                    </thead>
                    <tbody>{this.showData()}</tbody>
                  </Table>

                  <Button
                    type="submit"
                    className="mr-1"
                    color="primary"
                    size="sm"
                    //   disabled={this.state.isLoading}
                  >
                    <i className="fa fa-save" /> บันทึก
                  </Button>
                  <Button type="reset" color="danger" size="sm">
                    <i className="fa fa-ban" /> ยกเลิก
                  </Button>
                </CardBody>
              </Card>
            </Form>
          </Col>
        </Row>
      </div>
    );
  }
}

export default AeUsers;
