import React from "react";
import { withRouter } from "react-router-dom";
import {
  Card,
  CardBody,
  Col,
  Row,
  Table,
  Button,
  FormGroup,
  Input,
  InputGroup,
  InputGroupAddon
} from "reactstrap";
import Axios from "axios";
import { REQUEST_URL } from "../../config";

import Cookies from "js-cookie";
import Swal from "sweetalert2";
import { dateFormat } from "../../Module";

// import BootstrapTable from 'react-bootstrap-table-next';
// import paginationFactory from 'react-bootstrap-table2-paginator';

class Files extends React.Component {
  state = {
    data: [],
    filterData: []
  };

  componentDidMount() {
    this.showData();
  }

  onChangeSearch = e => {
    let input = e.target.value;
    let temp = this.state.data.filter(item => {
      return (
        item.fileID.toUpperCase().match(input.toUpperCase()) ||
        item.filename.toUpperCase().match(input.toUpperCase())
      );
    });

    this.setState({ filterData: temp });
  };

  showData = async () => {
    await Axios.get(REQUEST_URL + "/files", {
      headers: { authorization: Cookies.get("webcomputer") }
    })
      .then(res => {
        if (res.data.status) {
          this.setState({ data: res.data.data, filterData: res.data.data });
        } else {
          this.props.history.push("/login");
        }
      })
      .catch(err => console.log(err));
  };

  deleteData = async (fileID, fileName) => {
    Swal.fire({
      title: "คุณต้องการลบ?",
      text: fileName + " ใช่หรือไม่!",
      type: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes"
    }).then(async result => {
      if (result.value) {
        await Axios.get(REQUEST_URL + "/files/delete/" + fileID.toUpperCase(), {
          headers: { authorization: Cookies.get("webcomputer") }
        })
          .then(res => {
            if (res.data.status) {
              Swal.fire("ลบข้อมูลเรียบร้อย!", "", "success");
              this.showData();
            } else {
              Swal.fire("ลบข้อมูลไม่สำเร็จ!", "", "error");
            }
          })
          .catch(err => console.log(err));
      }
    });
  };

  render() {
    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs={12}>
            <Card>
              <CardBody>
                <FormGroup row>
                  <Col md="3">
                    <InputGroup>
                      <Input
                        type="text"
                        name="txtSearch"
                        placeholder="ค้นหา"
                        onChange={this.onChangeSearch}
                        autoFocus
                      />
                      <InputGroupAddon addonType="append">
                        <Button type="button" block outline color="secondary">
                          <i className="fa fa-search"></i>
                        </Button>
                      </InputGroupAddon>
                    </InputGroup>
                  </Col>

                  <Col md="2">
                    <Button
                      color="success"
                      onClick={() => this.props.history.push("/files/new")}
                    >
                      <i className="fa fa-plus" /> เพิ่ม
                    </Button>
                  </Col>
                </FormGroup>
                <Table hover bordered responsive size="sm">
                  <thead className="thead-light">
                    <tr align="center">
                      <th scope="col">รหัสแฟ้ม</th>
                      <th scope="col">ชื่อแฟ้มระบบงาน</th>
                      <th scope="col">วันที่บันทึก</th>
                      <th scope="col">ผู้บันทึก</th>
                      <th scope="col">วันที่แก้ไข</th>
                      <th scope="col">ผู้แก้ไข</th>
                      <th scope="col" />
                    </tr>
                  </thead>
                  <tbody>
                    {this.state.filterData.length ? (
                      this.state.filterData.map((value, i) => {
                        return (
                          <tr key={i}>
                            <th scope="row">{value.fileID}</th>
                            <td>{value.filename}</td>
                            <td align="center">{dateFormat(value.predate)}</td>
                            <td>{value.preby}</td>
                            <td align="center">{dateFormat(value.lastdate)}</td>
                            <td>{value.lastby}</td>
                            <td align="center">
                              <Button
                                className="mr-1"
                                color="warning"
                                size="sm"
                                onClick={() =>
                                  this.props.history.push(
                                    "/files/" + value.fileID
                                  )
                                }
                              >
                                <i className="fa fa-edit" />
                              </Button>
                              <Button
                                color="danger"
                                size="sm"
                                onClick={() =>
                                  this.deleteData(value.fileID, value.filename)
                                }
                              >
                                <i className="fa fa-trash" />
                              </Button>
                            </td>
                          </tr>
                        );
                      })
                    ) : (
                      <tr>
                        <td colSpan="7" align="center">
                          ไม่พบข้อมูล
                        </td>
                      </tr>
                    )}
                  </tbody>
                </Table>
              </CardBody>
            </Card>
          </Col>
        </Row>
      </div>
    );
  }
}

export default withRouter(Files);
