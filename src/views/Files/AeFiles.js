import React from "react";
import Axios from "axios";
import {
  Card,
  CardBody,
  Col,
  Row,
  Table,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
  FormFeedback
} from "reactstrap";

import { withRouter } from "react-router-dom";
import Swal from "sweetalert2";
import { REQUEST_URL } from "../../config";
import Cookies from "js-cookie";

class AeFile extends React.Component {
  state = {
    data: [],
    fileID: "",
    fileName: "",
    isLoading: false,
    valid: false,
    invalid: false,
    addby: Cookies.get("webcomputer").split(":")
  };

  componentDidMount = async () => {
    if (!this.props.match.params.id) {
      return this.showAdd();
    } else {
      return this.showEdit();
    }
  };

  showAdd = async () => {
    await Axios.get(REQUEST_URL + "/users", {
      headers: { authorization: Cookies.get("webcomputer") }
    })
      .then(res => {
        if (res.data.status) {
          const newArray = res.data.data.map(user => {
            return {
              ...user,
              act_open: false,
              act_view: false,
              act_add: false,
              act_edit: false,
              act_delete: false,
              act_copy: false,
              act_print: false,
              act_other: false
            };
          });
          this.setState({ data: newArray });
        } else {
          this.props.history.push("/login");
        }
      })
      .catch(err => console.log(err));
  };

  showEdit = async () => {
    await Axios.get(
      REQUEST_URL + "/users/permission/files/" + this.props.match.params.id,
      {
        headers: { authorization: Cookies.get("webcomputer") }
      }
    )
      .then(res => {
        if (res.data.status) {
          this.setState({
            fileID: res.data.data[0].fileID,
            fileName: res.data.data[0].filename,
            data: res.data.data
          });
        } else {
          this.props.history.push("/login");
        }
      })
      .catch(error => {
        console.log(error);
      });
  };

  ShowUserDefault = () => {
    let unlock = `${process.env.PUBLIC_URL}/img/unlock_green.png`;
    let lock = `${process.env.PUBLIC_URL}/img/lock_red.png`;

    let rows = Object.values(this.state.data).map((row, i) => (
      <tr key={i}>
        <th scope="row" align="center">
          {i + 1}
        </th>
        <td align="center">
          {row.userLevel === "A" ? (
            <img
              src={`${process.env.PUBLIC_URL}/img/admin.png`}
              className="img-fluid"
              alt="Admin"
            />
          ) : (
            <img
              src={`${process.env.PUBLIC_URL}/img/users.png`}
              className="img-fluid"
              alt="User"
            />
          )}{" "}
        </td>
        <td>{row.userID}</td>
        <td>{row.sname}</td>
        <td align="right">0</td>
        <td />
        <td align="center">
          <img
            src={row.act_open ? unlock : lock}
            alt="Open"
            className="img-fluid"
            onClick={this.setPermiss("open", row.act_open, i)}
          />
        </td>
        <td align="center">
          <img
            src={row.act_view ? unlock : lock}
            alt="View"
            className="img-fluid"
            onClick={this.setPermiss("view", row.act_view, i)}
          />
        </td>
        <td align="center">
          <img
            src={row.act_add ? unlock : lock}
            alt="Add"
            className="img-fluid"
            onClick={this.setPermiss("add", row.act_add, i)}
          />
        </td>
        <td align="center">
          <img
            src={row.act_edit ? unlock : lock}
            alt="Edit"
            className="img-fluid"
            onClick={this.setPermiss("edit", row.act_edit, i)}
          />
        </td>
        <td align="center">
          <img
            src={row.act_delete ? unlock : lock}
            alt="Delete"
            className="img-fluid"
            onClick={this.setPermiss("delete", row.act_delete, i)}
          />
        </td>
        <td align="center">
          <img
            src={row.act_copy ? unlock : lock}
            alt="Copy"
            className="img-fluid"
            onClick={this.setPermiss("copy", row.act_copy, i)}
          />
        </td>
        <td align="center">
          <img
            src={row.act_print ? unlock : lock}
            alt="Print"
            className="img-fluid"
            onClick={this.setPermiss("print", row.act_print, i)}
          />
        </td>
        <td align="center">
          <img
            src={row.act_other ? unlock : lock}
            alt="other"
            className="img-fluid"
            onClick={this.setPermiss("other", row.act_other, i)}
          />
        </td>
      </tr>
    ));

    return rows;
  };

  setPermiss = (field, bln, index) => e => {
    const newArray = this.state.data[index];

    switch (field) {
      case "open":
        newArray.act_open = !bln;
        break;

      case "view":
        newArray.act_view = !bln;
        break;

      case "add":
        newArray.act_add = !bln;
        break;

      case "edit":
        newArray.act_edit = !bln;
        break;

      case "delete":
        newArray.act_delete = !bln;
        break;

      case "copy":
        newArray.act_copy = !bln;
        break;

      case "print":
        newArray.act_print = !bln;
        break;

      case "other":
        newArray.act_other = !bln;
        break;

      default:
    }

    const arr = [...this.state.data];
    arr.splice(index, 1, newArray);
    this.setState({ data: arr });
  };

  onSubmit = e => {
    this.setState({ isLoading: true });
    e.preventDefault();
    if (!this.props.match.params.id) {
      if (this.state.valid === false) {
        this.setState({ isLoading: false });
        return Swal.fire(
          "มีรหัสแฟ้ม " + this.state.fileID.toUpperCase() + " แล้วในระบบ",
          "โปรดระบุรหัสแฟ้มใหม่",
          "error"
        );
      }
    }

    Swal.fire({
      title: "คุณต้องการ?",
      text: !this.props.match.params.id
        ? "บันทึกข้อมูลใช่หรือไม่"
        : "แก้ไขข้อมูลใช่หรือไม่",
      type: "question",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes"
    }).then(async result => {
      if (result.value) {
        if (!this.props.match.params.id) {
          return this.saveData("add");
        } else {
          return this.saveData("edit");
        }
      }
    });
    this.setState({ isLoading: false });
  };

  saveData = async type => {
    await Axios.post(
      REQUEST_URL + "/files/" + type,
      {
        fileID: this.state.fileID.toUpperCase(),
        fileName: this.state.fileName,
        detail: this.state.data,
        addby: this.state.addby[0]
      },
      { headers: { authorization: Cookies.get("webcomputer") } }
    )
      .then(res => {
        if (res.data.status) {
          Swal.fire("บันทึกข้อมูลสำเร็จ", "", "success");
          this.props.history.push("/files");
          this.setState({ fileID: "", fileName: "" });
        } else {
          // this.props.history.push("/login");
          Swal.fire("บันทึกข้อมูลไม่สำเร็จ", "", "error");
        }
      })
      .catch(err => console.error(err));
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });

    if (e.target.name === "fileID") {
      this.checkFileID(e.target.value);
    }
  };

  checkFileID = async fileID => {
    if (fileID === "") {
      return this.setState({ valid: false, invalid: false });
    }
    await Axios.get(REQUEST_URL + "/files/check/" + fileID.toUpperCase(), {
      headers: { authorization: Cookies.get("webcomputer") }
    })
      .then(res => {
        if (res.data.status) {
          if (res.data.data.length === 0) {
            this.setState({ valid: true, invalid: false });
          } else {
            this.setState({ valid: false, invalid: true });
          }
        } else {
          this.props.history.push("/login");
        }
      })
      .catch(err => console.log(err));
  };

  render() {
    return this.state.data ? (
      <div className="animated fadeIn">
        <Row>
          <Col lg={12}>
            <Form onSubmit={e => this.onSubmit(e)} className="form-horizontal">
              <Card>
                <CardBody>
                  <FormGroup row inline>
                    <Label for="fileID" sm={1}>
                      รหัสแฟ้ม
                    </Label>
                    <Col sm={2}>
                      <Input
                        type="text"
                        valid={this.state.valid}
                        invalid={this.state.invalid}
                        style={{ textTransform: "uppercase" }}
                        name="fileID"
                        id="fileID"
                        placeholder="F01"
                        required
                        onChange={e => this.handleChange(e)}
                        value={this.state.fileID}
                        maxLength="3"
                        readOnly={!this.props.match.params.id ? false : true}
                      />
                      <FormFeedback>
                        มีรหัสแฟ้ม {this.state.fileID.toUpperCase()} แล้วในระบบ
                      </FormFeedback>
                    </Col>
                  </FormGroup>

                  <FormGroup row inline>
                    <Label for="fileName" sm={1}>
                      ชื่อแฟ้ม
                    </Label>
                    <Col sm={11}>
                      <Input
                        type="text"
                        name="fileName"
                        id="fileName"
                        required
                        onChange={e => this.handleChange(e)}
                        value={this.state.fileName}
                      />
                    </Col>
                  </FormGroup>

                  <Table size="sm" hover bordered responsive>
                    <thead className="thead-light">
                      <tr align="center">
                        <th align="center">#</th>
                        <th>Level</th>
                        <th>User LogIn</th>
                        <th>ชื่อ-นามสกุล</th>
                        <th>เปิดใช้</th>
                        <th>เข้าใช้ลาสุด</th>
                        <th>เปิด</th>
                        <th>มุมมอง</th>
                        <th>เพิ่ม</th>
                        <th>แก้ไข</th>
                        <th>ลบ</th>
                        <th>คัดลอก</th>
                        <th>พิมพ์</th>
                        <th>อื่น ๆ</th>
                      </tr>
                    </thead>
                    <tbody>{this.ShowUserDefault()}</tbody>
                  </Table>

                  <Button
                    type="submit"
                    className="mr-1"
                    color="primary"
                    size="sm"
                    disabled={this.state.isLoading}
                  >
                    <i className="fa fa-save" /> บันทึก
                  </Button>
                  <Button type="reset" color="danger" size="sm">
                    <i className="fa fa-ban" /> ยกเลิก
                  </Button>
                </CardBody>
              </Card>
            </Form>
          </Col>
        </Row>
      </div>
    ) : (
      "Loading"
    );
  }
}

export default withRouter(AeFile);
