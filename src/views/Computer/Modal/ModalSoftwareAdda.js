import React from "react";
import {
  Modal,
  Container,
  Row,
  Col,
  Form,
  Button,
  Table,
  Tab,
  Tabs
} from "react-bootstrap";
import axios from "axios";
import { REQUEST_URL } from "../../../config";
import Cookies from "js-cookie";
import Swal from "sweetalert2";

class ModalSoftwareAdda extends React.Component {
  state = {
    datasoftwareadda: [],
    inputsearchx: ""
  };

  componentDidMount = async () => {
    await this.SoftwareAdda();
  };

  SoftwareAdda = async () => {
    await axios
      .post(
        REQUEST_URL + "/computer/softwareadda",
        {},
        {
          headers: { authorization: Cookies.get("webcomputer") }
        }
      )
      .then(response => {
        if (response.data.status) {
          this.setState({
            datasoftwareadda: response.data.data
          });
        } else {
          this.props.history.push("/login");
        }
      });
  };

  modalClose = bool => e => {
    if (!bool) {
      this.setState({
        inputsearchx: ""
      });
    }
    this.props.set(bool);
  };

  searchHandler = e => {
    this.setState({ [e.target.name]: e.target.value });
    this.searchNoButton(e.target.value);
  };

  searchNoButton = txt => {
    let ggez = txt;
    var filtered = this.state.datasoftwareadda.filter(function(x) {
      return (
        x.appID.match(new RegExp("^" + ggez === null ? "" : ggez, "i")) ||
        x.appName.match(new RegExp("^" + ggez === null ? "" : ggez, "i")) ||
        x.appNameEng.match(new RegExp("^" + ggez === null ? "" : ggez, "i"))
      );
    });
    this.setState({ searchx: filtered.slice(0, 200) });
  };

  toMain = (appID, appName, appNameEng, reMark) => e => {
    let tf;
    for (let x = 0; x < this.props.dataexist.length; x++) {
      if (this.props.dataexist[x].appID === appID) {
        tf = true;
      } else {
        tf = false;
      }
    }
    if (tf) {
      Swal.fire({
        type: "error",
        title: "ห้ามเพิ่มข้อมูลช้ำ"
      });
    } else {
      this.props.getSoftwareAdda(appID, appName, appNameEng, reMark);
      this.setState({
        inputsearchx: ""
      });
      this.props.set(false);
    }
  };

  render() {
    const { inputsearchx } = this.state;
    return (
      <div>
        <Modal
          size="lg"
          show={this.props.Modal}
          onHide={this.modalClose(false)}
        >
          <Modal.Header
            closeButton
            id="fontthai"
            style={{ fontSize: "22px" }}
          ></Modal.Header>
          <Modal.Body>
            <label>ค้นหา : </label>
            <input
              type="text"
              name="inputsearchx"
              values={inputsearchx}
              onChange={this.searchHandler}
              placeholder="รหัสบาร์โค้ดหรือชื่อซอฟต์แวร์เพื่อค้นหา"
              style={{ width: "500px", marginBottom: "2%" }}
            />
            <div style={{ display: "flex", justifyContent: "center" }}>
              <table
                //   width="100%"
                style={{
                  display: "block",
                  height: "350px",
                  overflowY: "scroll",
                  alignItems: "center"
                }}
              >
                <thead className="text-center table-active">
                  <tr>
                    <th id="tb" width="10%">
                      รหัสโปรแกรม
                    </th>
                    <th id="tb" width="30%">
                      โปรแกรม
                    </th>
                    <th id="tb" width="30%">
                      ภาษาอังกฤษ
                    </th>
                    <th id="tb" width="10%">
                      สถานะ
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.inputsearchx === ""
                    ? this.state.datasoftwareadda.map((item, index) => {
                        return (
                          <tr key={index}>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.appID}
                            </td>
                            <td id="tb">{item.appName}</td>
                            <td id="tb">{item.appNameEng}</td>
                            <td
                              id="tb"
                              style={{
                                textAlign: "center",
                                cursor: "pointer",
                                color: "red"
                              }}
                              onClick={this.toMain(
                                item.appID,
                                item.appName,
                                item.appNameEng,
                                item.reMark
                              )}
                            >
                              เลือก
                            </td>
                          </tr>
                        );
                      })
                    : this.state.searchx.map((item, index) => {
                        return (
                          <tr key={index}>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.appID}
                            </td>
                            <td id="tb">{item.appName}</td>
                            <td id="tb">{item.appNameEng}</td>
                            <td
                              id="tb"
                              style={{
                                textAlign: "center",
                                cursor: "pointer",
                                color: "red"
                              }}
                              onClick={this.toMain(
                                item.appID,
                                item.appName,
                                item.appNameEng,
                                item.reMark
                              )}
                            >
                              เลือก
                            </td>
                          </tr>
                        );
                      })}
                </tbody>
              </table>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={this.modalClose(false)}
              id="btnthai"
            >
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default ModalSoftwareAdda;
