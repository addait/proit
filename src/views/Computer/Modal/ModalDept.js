import React from "react";
import {
  Modal,
  Container,
  Row,
  Col,
  Form,
  Button,
  Table,
  Tab,
  Tabs
} from "react-bootstrap";
import axios from "axios";
import { REQUEST_URL } from "../../../config";
import Cookies from "js-cookie";

class ModalDept extends React.Component {
  state = {
    datadept: [],
    inputsearchx: ""
  };

  componentDidMount = async () => {
    await this.Dept();
  };

  Dept = async () => {
    await axios
      .post(
        REQUEST_URL + "/computer/depthrm",
        {},
        {
          headers: { authorization: Cookies.get("webcomputer") }
        }
      )
      .then(response => {
        if (response.data.status) {
          this.setState({
            datadept: response.data.data
          });
        } else {
          this.props.history.push("/login");
        }
      });
  };

  modalClose = bool => e => {
    if (!bool) {
      this.setState({
        inputsearchx: ""
      });
    }
    this.props.set(bool);
  };

  searchHandler = e => {
    this.setState({ [e.target.name]: e.target.value });
    this.searchNoButton(e.target.value);
  };

  searchNoButton = txt => {
    let ggez = txt;
    var filtered = this.state.datadept.filter(function(x) {
      return (
        x.depcode.match(new RegExp("^" + ggez === null ? "" : ggez, "i")) ||
        x.desc.match(new RegExp("^" + ggez === null ? "" : ggez, "i"))
      );
    });
    this.setState({ searchx: filtered.slice(0, 200) });
  };

  toMain = (depcode, desc) => e => {
    if (this.props.REQ === "Computer") {
      this.props.getDept(depcode, desc);
    } else if (this.props.REQ === "Software") {
      this.props.getSoftwareDept(depcode, desc);
    }
    this.setState({
      inputsearchx: ""
    });
    this.props.set(false);
  };

  render() {
    const { inputsearchx } = this.state;
    return (
      <div>
        <Modal
          size="lg"
          show={this.props.Modal}
          onHide={this.modalClose(false)}
        >
          <Modal.Header
            closeButton
            id="fontthai"
            style={{ fontSize: "22px" }}
          ></Modal.Header>
          <Modal.Body>
            <label>ค้นหา : </label>
            <input
              type="text"
              name="inputsearchx"
              values={inputsearchx}
              onChange={this.searchHandler}
              placeholder="รหัสแผนกหรือชื่อแผนกเพื่อค้นหา"
              style={{ width: "500px", marginBottom: "2%" }}
            />
            <div style={{ display: "flex", justifyContent: "center" }}>
              <table
                //   width="100%"
                style={{
                  display: "block",
                  height: "350px",
                  overflowY: "scroll",
                  alignItems: "center"
                }}
              >
                <thead className="text-center table-active">
                  <tr>
                    <th id="tb">รหัสแผนก</th>
                    <th id="tb">ชื่อแผนก</th>
                    <th id="tb">สถานะ</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.inputsearchx === ""
                    ? this.state.datadept.map((item, index) => {
                        return (
                          <tr key={index}>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.depcode}
                            </td>
                            <td id="tb">{item.desc}</td>
                            <td
                              id="tb"
                              style={{
                                textAlign: "center",
                                cursor: "pointer",
                                color: "red"
                              }}
                              onClick={this.toMain(item.depcode, item.desc)}
                            >
                              เลือก
                            </td>
                          </tr>
                        );
                      })
                    : this.state.searchx.map((item, index) => {
                        return (
                          <tr key={index}>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.depcode}
                            </td>
                            <td id="tb">{item.desc}</td>
                            <td
                              id="tb"
                              style={{
                                textAlign: "center",
                                cursor: "pointer",
                                color: "red"
                              }}
                              onClick={this.toMain(item.depcode, item.desc)}
                            >
                              เลือก
                            </td>
                          </tr>
                        );
                      })}
                </tbody>
              </table>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={this.modalClose(false)}
              id="btnthai"
            >
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default ModalDept;
