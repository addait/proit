import React from "react";
import {
  Modal,
  Container,
  Row,
  Col,
  Form,
  Button,
  Table,
  Tab,
  Tabs
} from "react-bootstrap";
import axios from "axios";
import { REQUEST_URL } from "../../../config";
import Cookies from "js-cookie";
import Swal from "sweetalert2";

class ModalOS extends React.Component {
  state = {
    dataos: [],
    inputsearchx: ""
  };

  componentDidMount = async () => {
    await this.OS();
  };

  OS = async () => {
    await axios
      .post(
        REQUEST_URL + "/computer/os",
        {},
        {
          headers: { authorization: Cookies.get("webcomputer") }
        }
      )
      .then(response => {
        if (response.data.status) {
          this.setState({
            dataos: response.data.data
          });
        } else {
          this.props.history.push("/login");
        }
      });
  };

  modalClose = bool => e => {
    if (!bool) {
      this.setState({
        inputsearchx: ""
      });
    }
    this.props.set(bool);
  };

  searchHandler = e => {
    this.setState({ [e.target.name]: e.target.value });
    this.searchNoButton(e.target.value);
  };

  searchNoButton = txt => {
    let ggez = txt;
    var filtered = this.state.dataos.filter(function(x) {
      return (
        x.barcode.match(new RegExp("^" + ggez === null ? "" : ggez, "i")) ||
        x.name.match(new RegExp("^" + ggez === null ? "" : ggez, "i"))
      );
    });
    this.setState({ searchx: filtered.slice(0, 200) });
  };

  toMain = (barcode, name, quantity, cdkey) => e => {
    if (quantity === 0) {
      Swal.fire({
        type: "error",
        title: "รุ่นนี้หมดแล้ว"
      });
    } else {
      this.props.getOS(barcode, name, cdkey);
      this.setState({
        inputsearchx: ""
      });
      this.props.set(false);
    }
  };

  render() {
    const { inputsearchx } = this.state;
    return (
      <div>
        <Modal
          size="xl"
          show={this.props.Modal}
          onHide={this.modalClose(false)}
        >
          <Modal.Header
            closeButton
            id="fontthai"
            style={{ fontSize: "22px" }}
          ></Modal.Header>
          <Modal.Body>
            <label>ค้นหา : </label>
            <input
              type="text"
              name="inputsearchx"
              values={inputsearchx}
              onChange={this.searchHandler}
              placeholder="รหัสบาร์โค้ดหรือชื่อ OS เพื่อค้นหา"
              style={{ width: "500px", marginBottom: "2%" }}
            />
            <div style={{ display: "flex", justifyContent: "center" }}>
              <table
                //   width="100%"
                style={{
                  display: "block",
                  height: "350px",
                  overflowY: "scroll",
                  alignItems: "center"
                }}
              >
                <thead className="text-center table-active">
                  <tr>
                    <th id="tb" width="10%">
                      Barcode
                    </th>
                    <th id="tb" width="10%">
                      ยี่ห้อ
                    </th>
                    <th id="tb" width="8%">
                      Part-Number
                    </th>
                    <th id="tb" width="47%">
                      รุ่นระบบปฎิบัติการ
                    </th>
                    {/* <th id="tb">Product-Key</th> */}
                    <th id="tb" width="5%">
                      License
                    </th>
                    <th id="tb" width="5%">
                      จำนวนที่ใช้
                    </th>
                    <th id="tb" width="5%">
                      คงเหลือ
                    </th>
                    <th id="tb" width="10%">
                      สถานะ
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.inputsearchx === ""
                    ? this.state.dataos.map((item, index) => {
                        return (
                          <tr key={index}>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.barcode}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.brandName}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.partnumber}
                            </td>
                            <td id="tb">{item.name}</td>
                            {/* <td id="tb">{item.cdkey}</td> */}
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.volumelicense}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.countb}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {parseInt(item.volumelicense) -
                                parseInt(item.countb) ===
                              0
                                ? "หมด"
                                : parseInt(item.volumelicense) -
                                  parseInt(item.countb)}
                            </td>
                            <td
                              id="tb"
                              style={{
                                textAlign: "center",
                                cursor: "pointer",
                                color: "red"
                              }}
                              onClick={this.toMain(
                                item.barcode,
                                item.name,
                                parseInt(item.volumelicense) -
                                  parseInt(item.countb),
                                item.cdkey
                              )}
                            >
                              เลือก
                            </td>
                          </tr>
                        );
                      })
                    : this.state.searchx.map((item, index) => {
                        return (
                          <tr key={index}>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.barcode}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.brandName}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.partnumber}
                            </td>
                            <td id="tb">{item.name}</td>
                            {/* <td id="tb">{item.cdkey}</td> */}
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.volumelicense}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {item.countb}
                            </td>
                            <td id="tb" style={{ textAlign: "center" }}>
                              {parseInt(item.volumelicense) -
                                parseInt(item.countb) ===
                              0
                                ? "หมด"
                                : parseInt(item.volumelicense) -
                                  parseInt(item.countb)}
                            </td>
                            <td
                              id="tb"
                              style={{
                                textAlign: "center",
                                cursor: "pointer",
                                color: "red"
                              }}
                              onClick={this.toMain(
                                item.barcode,
                                item.name,
                                parseInt(item.volumelicense) -
                                  parseInt(item.countb),
                                item.cdkey
                              )}
                            >
                              เลือก
                            </td>
                          </tr>
                        );
                      })}
                </tbody>
              </table>
            </div>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="secondary"
              onClick={this.modalClose(false)}
              id="btnthai"
            >
              Close
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default ModalOS;
