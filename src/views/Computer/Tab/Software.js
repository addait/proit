import React from "react";
import { Col, Row, Button } from "reactstrap";
import { Tabs, Tab, Collapse, Card } from "react-bootstrap";
import Swal from "sweetalert2";
import { REQUEST_URL } from "../../../config";
import Cookies from "js-cookie";
import moment from "moment";
import ModalSoftware from "../Modal/ModalSoftware";
import Axios from "axios";

class Software extends React.Component {
  state = {
    datasoftware: []
  };

  componentDidMount = () => {
    this.loadSoftwareEdit();
  };

  loadSoftwareEdit = async () => {
    Axios.post(
      REQUEST_URL + "/computer/loadsoftwareedit",
      {
        id: this.props.ID
      },
      { headers: { authorization: Cookies.get("webcomputer") } }
    ).then(response => {
      if (response.data.status) {
        for (let x = 0; x < response.data.data.length; x++) {
          let SW = this.state.datasoftware;
          this.state.datasoftware.splice(this.state.datasoftware.length, 0, {
            barcode: response.data.data[x].barcode,
            brandname: response.data.data[x].brandName,
            name: response.data.data[x].name,
            cdkey: response.data.data[x].cdkey,
            price: response.data.data[x].price
          });
          this.setState({
            datasoftware: SW
          });
        }
      } else {
        this.props.history.push("/login");
      }
    });
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (prevState !== this.state) {
      this.props.getSoftware(this.state.datasoftware);
    }
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  modalSoftware = bool => {
    this.setState({
      modalsoftware: bool
    });
  };

  Software = (barcode, brandname, name, cdkey, price) => {
    // console.log(barcode, brandname, name, cdkey, price);
    this.state.datasoftware.splice(this.state.datasoftware.length, 0, {
      barcode: barcode,
      brandname: brandname,
      name: name,
      cdkey: cdkey,
      price: price
    });
  };

  deleteSoftware = index => e => {
    let a = this.state.datasoftware;
    a.splice(index, 1);
    this.setState({
      datasoftware: a
    });
  };

  render() {
    return (
      <div className="animated fadeIn">
        <Button
          color="success"
          style={{ marginBottom: "2%" }}
          onClick={() => this.setState({ modalsoftware: true })}
        >
          เพิ่มซอฟต์แวร์
        </Button>
        <Card style={{ height: "400px" }}>
          <table width="100%" style={{ marginBottom: "2%" }}>
            <thead className="text-center table-active">
              <tr>
                <th id="tb" width="5%">
                  ลำดับที่
                </th>
                <th id="tb" width="10%">
                  Barcode
                </th>
                <th id="tb" width="10%">
                  ยี่ห้อ
                </th>
                <th id="tb" width="30%">
                  รายละเอียด
                </th>
                <th id="tb" width="30%">
                  Product Key
                </th>
                <th id="tb" width="5%">
                  ราคา
                </th>
                <th id="tb" width="4%">
                  #
                </th>
              </tr>
            </thead>
            <tbody>
              {this.state.datasoftware.map((item, index) => {
                return (
                  <tr key={index}>
                    <td id="tb" style={{ textAlign: "center" }}>
                      {index + 1}
                    </td>
                    <td id="tb" style={{ textAlign: "center" }}>
                      {item.barcode}
                    </td>
                    <td id="tb" style={{ textAlign: "center" }}>
                      {item.brandname}
                    </td>
                    <td id="tb">{item.name}</td>
                    <td id="tb">{item.cdkey}</td>
                    <td id="tb" style={{ textAlign: "center" }}>
                      {item.price}
                    </td>
                    <td
                      id="tb"
                      style={{
                        textAlign: "center",
                        cursor: "pointer",
                        color: "red"
                      }}
                      onClick={this.deleteSoftware(index)}
                    >
                      ลบ
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </Card>
        <ModalSoftware
          Modal={this.state.modalsoftware}
          set={this.modalSoftware}
          getSoftware={this.Software}
          dataexist={this.state.datasoftware}
        />
      </div>
    );
  }
}

export default Software;
