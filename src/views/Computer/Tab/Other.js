import React from "react";
import { Col, Row, Button } from "reactstrap";
import { Tabs, Tab, Collapse } from "react-bootstrap";
import Swal from "sweetalert2";
import { REQUEST_URL } from "../../../config";
import Cookies from "js-cookie";
import DatePicker from "react-date-picker";
import moment from "moment";
import pic1 from "../../../img/placeholder.png";
import ModalUser from ".././Modal/ModalUser";
import ModalDept from ".././Modal/ModalDept";
import ModalOS from ".././Modal/ModalOS";
import Axios from "axios";

class Other extends React.Component {
  state = {
    value: "PC",
    bit: "32",
    Date: new Date(),
    DateCPU: new Date(),
    DateMB: new Date(),
    DateRAM: new Date(),
    DateHDD: new Date(),
    DateMN: new Date(),
    DateVGA: new Date(),
    DateLAN: new Date(),
    DatePT: new Date(),
    DateOT: new Date(),
    tabCPU: false,
    tabMB: false,
    tabRAM: false,
    tabHDD: false,
    tabMN: false,
    tabVGA: false,
    tabLAN: false,
    tabPT: false,
    tabOT: false,
    dataCPU: [],
    dataMB: [],
    dataRAM: [],
    dataHDD: [],
    dataMN: [],
    dataVGA: [],
    dataLAN: [],
    dataPT: [],
    dataOT: [],
    pfs_id: "",
    name: "",
    depcode: "",
    depname: "",
    barcode: "",
    nameos: "",
    productkey: "",
    preViewimg: "",
    type: "",
    statusitemCPU: "1",
    statusitemMB: "1",
    statusitemRAM: "1",
    statusitemHDD: "1",
    statusitemMN: "1",
    statusitemVGA: "1",
    statusitemLAN: "1",
    statusitemPT: "1",
    statusitemOT: "1"
  };

  componentDidMount = () => {
    if (this.props.ID !== undefined) {
      this.loadDataEdit();
    }
  };

  loadDataEdit = async () => {
    Axios.post(
      REQUEST_URL + "/computer/loaddataedit",
      {
        id: this.props.ID
      },
      { headers: { authorization: Cookies.get("webcomputer") } }
    ).then(response => {
      if (response.data.status) {
        let a = response.data.data[0].comID.substr(0, 3);
        if (a === "COM") {
          a = "PC";
        } else if (a === "TIN") {
          a = "THIN";
        } else if (a === "NTE") {
          a = "NOTEBOOK";
        } else if (a === "SRV") {
          a = "SERVER";
        } else if (a === "MAC") {
          a = "I-MAC";
        } else if (a === "I-MAC") {
          a = "IMC";
        }
        let o;
        if (response.data.data[0].osbit) {
          o = "64";
        } else {
          o = "32";
        }
        this.setState({
          value: a,
          Date: response.data.data[0].startDate,
          tel: response.data.data[0].Tel,
          pfs_id: response.data.data[0].pfs_id,
          name: response.data.data[0].nameuse,
          depcode: response.data.data[0].depcode,
          depname: response.data.data[0].desc,
          os: response.data.data[0].barcode,
          nameos: response.data.data[0].name,
          productkey: response.data.data[0].cdkey,
          bit: o,
          pcname: response.data.data[0].comname,
          ippc: response.data.data[0].ipaddress,
          mainboard: response.data.data[0].m_b,
          cpu: response.data.data[0].cpu,
          speed: response.data.data[0].cpuSpeed,
          vga: response.data.data[0].vga,
          harddisk: response.data.data[0].hdd,
          sizehd: response.data.data[0].hddSize,
          monitor: response.data.data[0].monitor,
          ram: response.data.data[0].ram,
          sizeram: response.data.data[0].ramSize,
          ups: response.data.data[0].ups,
          lan: response.data.data[0].lancard,
          printer: response.data.data[0].printer,
          printersn: response.data.data[0].printersn,
          // price: response.data.data[0].price,
          priceall: 9999,
          note: response.data.data[0].Remark
        });
        for (let x = 0; x < response.data.data.length; x++) {
          let status;
          if (response.data.data[x].status) {
            status = 1;
          } else {
            status = 0;
          }
          if (response.data.data[x].type === "0") {
            let CPU = this.state.dataCPU;
            this.state.dataCPU.splice(this.state.dataCPU.length, 0, {
              codeitem: response.data.data[x].ascode,
              nameitem: response.data.data[x].asdesc,
              priceitem: response.data.data[x].price,
              dateitem: moment(response.data.data[x].bstartdate)
                .utc()
                .format("YYYY-MM-DD HH:mm:ss"),
              statusitem: status,
              noteitem: response.data.data[x].bremark,
              type: "CPU",
              datetimestart: moment(this.state.Date)
                .utc()
                .format("DD/MM/YYYY")
            });
            this.setState({
              dataCPU: CPU
            });
          } else if (response.data.data[x].type === "1") {
            let MB = this.state.dataMB;
            this.state.dataMB.splice(this.state.dataMB.length, 0, {
              codeitem: response.data.data[x].ascode,
              nameitem: response.data.data[x].asdesc,
              priceitem: response.data.data[x].price,
              dateitem: moment(response.data.data[x].bstartdate)
                .utc()
                .format("YYYY-MM-DD HH:mm:ss"),
              statusitem: status,
              noteitem: response.data.data[x].bremark,
              type: "MB",
              datetimestart: moment(this.state.Date)
                .utc()
                .format("DD/MM/YYYY")
            });
            this.setState({
              dataMB: MB
            });
          } else if (response.data.data[x].type === "2") {
            let RAM = this.state.dataRAM;
            this.state.dataRAM.splice(this.state.dataRAM.length, 0, {
              codeitem: response.data.data[x].ascode,
              nameitem: response.data.data[x].asdesc,
              priceitem: response.data.data[x].price,
              dateitem: moment(response.data.data[x].bstartdate)
                .utc()
                .format("YYYY-MM-DD HH:mm:ss"),
              statusitem: status,
              noteitem: response.data.data[x].bremark,
              type: "RAM",
              datetimestart: moment(this.state.Date)
                .utc()
                .format("DD/MM/YYYY")
            });
            this.setState({
              dataRAM: RAM
            });
          } else if (response.data.data[x].type === "3") {
            let HDD = this.state.dataHDD;
            this.state.dataHDD.splice(this.state.dataHDD.length, 0, {
              codeitem: response.data.data[x].ascode,
              nameitem: response.data.data[x].asdesc,
              priceitem: response.data.data[x].price,
              dateitem: moment(response.data.data[x].bstartdate)
                .utc()
                .format("YYYY-MM-DD HH:mm:ss"),
              statusitem: status,
              noteitem: response.data.data[x].bremark,
              type: "HDD",
              datetimestart: moment(this.state.Date)
                .utc()
                .format("DD/MM/YYYY")
            });
            this.setState({
              dataHDD: HDD
            });
          } else if (response.data.data[x].type === "4") {
            let MN = this.state.dataMN;
            this.state.dataMN.splice(this.state.dataMN.length, 0, {
              codeitem: response.data.data[x].ascode,
              nameitem: response.data.data[x].asdesc,
              priceitem: response.data.data[x].price,
              dateitem: moment(response.data.data[x].bstartdate)
                .utc()
                .format("YYYY-MM-DD HH:mm:ss"),
              statusitem: status,
              noteitem: response.data.data[x].bremark,
              type: "MN",
              datetimestart: moment(this.state.Date)
                .utc()
                .format("DD/MM/YYYY")
            });
            this.setState({
              dataMN: MN
            });
          } else if (response.data.data[x].type === "5") {
            let VGA = this.state.dataVGA;
            this.state.dataVGA.splice(this.state.dataVGA.length, 0, {
              codeitem: response.data.data[x].ascode,
              nameitem: response.data.data[x].asdesc,
              priceitem: response.data.data[x].price,
              dateitem: moment(response.data.data[x].bstartdate)
                .utc()
                .format("YYYY-MM-DD HH:mm:ss"),
              statusitem: status,
              noteitem: response.data.data[x].bremark,
              type: "VGA",
              datetimestart: moment(this.state.Date)
                .utc()
                .format("DD/MM/YYYY")
            });
            this.setState({
              dataVGA: VGA
            });
          } else if (response.data.data[x].type === "6") {
            let LAN = this.state.dataLAN;
            this.state.dataLAN.splice(this.state.dataLAN.length, 0, {
              codeitem: response.data.data[x].ascode,
              nameitem: response.data.data[x].asdesc,
              priceitem: response.data.data[x].price,
              dateitem: moment(response.data.data[x].bstartdate)
                .utc()
                .format("YYYY-MM-DD HH:mm:ss"),
              statusitem: status,
              noteitem: response.data.data[x].bremark,
              type: "LAN",
              datetimestart: moment(this.state.Date)
                .utc()
                .format("DD/MM/YYYY")
            });
            this.setState({
              dataLAN: LAN
            });
          } else if (response.data.data[x].type === "7") {
            let PT = this.state.dataPT;
            this.state.dataPT.splice(this.state.dataPT.length, 0, {
              codeitem: response.data.data[x].ascode,
              nameitem: response.data.data[x].asdesc,
              priceitem: response.data.data[x].price,
              dateitem: moment(response.data.data[x].bstartdate)
                .utc()
                .format("YYYY-MM-DD HH:mm:ss"),
              statusitem: status,
              noteitem: response.data.data[x].bremark,
              type: "PT",
              datetimestart: moment(this.state.Date)
                .utc()
                .format("DD/MM/YYYY")
            });
            this.setState({
              dataPT: PT
            });
          } else if (response.data.data[x].type === "8") {
            let OT = this.state.dataOT;
            this.state.dataOT.splice(this.state.dataOT.length, 0, {
              codeitem: response.data.data[x].ascode,
              nameitem: response.data.data[x].asdesc,
              priceitem: response.data.data[x].price,
              dateitem: moment(response.data.data[x].bstartdate)
                .utc()
                .format("YYYY-MM-DD HH:mm:ss"),
              statusitem: status,
              noteitem: response.data.data[x].bremark,
              type: "OT",
              datetimestart: moment(this.state.Date)
                .utc()
                .format("DD/MM/YYYY")
            });
            this.setState({
              dataOT: OT
            });
          }
        }
      } else {
        this.props.history.push("/login");
      }
    });
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (prevState !== this.state) {
      this.props.getOther(
        this.state.value,
        moment(this.state.Date).format("DD/MM/YYYY HH:mm:ss"),
        this.state.tel,
        this.state.pfs_id,
        this.state.name,
        this.state.depcode,
        this.state.depname,
        this.state.barcode,
        this.state.nameos,
        this.state.productkey,
        this.state.bit,
        this.state.pcname,
        this.state.ippc,
        this.state.mainboard,
        this.state.cpu,
        this.state.speed,
        this.state.vga,
        this.state.harddisk,
        this.state.sizehd,
        this.state.monitor,
        this.state.ram,
        this.state.sizeram,
        this.state.ups,
        this.state.lan,
        this.state.printer,
        this.state.printersn,
        this.state.price,
        this.state.priceall,
        this.state.note,
        this.state.dataCPU,
        this.state.dataMB,
        this.state.dataRAM,
        this.state.dataHDD,
        this.state.dataMN,
        this.state.dataVGA,
        this.state.dataLAN,
        this.state.dataPT,
        this.state.dataOT
      );
    }
  };

  // onChange = e => {
  //   if (e.target.files.length !== 0) {
  //     this.setState({
  //       file: e.target.files[0],
  //       fileName: e.target.files[0].name,
  //       nameType: e.target.files[0].type,
  //       nameShow: e.target.files[0].name,
  //       preViewimg: URL.createObjectURL(e.target.files[0])
  //     });
  //   } else {
  //     this.setState({
  //       fileName: "",
  //       nameType: "",
  //       nameShow: "",
  //       preViewimg: ""
  //     });
  //   }
  // };

  onChangeDrop = e => {
    this.setState({
      value: e.target.value
    });
  };

  handleChangeDate = name => date => {
    this.setState({
      [name]: date
    });
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  ToTable = tab => e => {
    e.preventDefault();
    if (tab === "CPU") {
      this.state.dataCPU.splice(this.state.dataCPU.length, 0, {
        codeitem: this.state.codeitemCPU,
        nameitem: this.state.nameitemCPU,
        priceitem: this.state.priceitemCPU,
        dateitem: moment(this.state.DateCPU).format("YYYY-MM-DD HH:mm:ss"),
        statusitem: this.state.statusitemCPU,
        noteitem: this.state.noteitemCPU,
        type: "CPU",
        datetimestart: moment(this.state.Date)
          .utc()
          .format("DD/MM/YYYY")
      });
      this.setState({
        codeitemCPU: "",
        nameitemCPU: "",
        priceitemCPU: "",
        DateCPU: new Date(),
        statusitemCPU: "",
        noteitemCPU: ""
      });
    } else if (tab === "MB") {
      this.state.dataMB.splice(this.state.dataMB.length, 0, {
        codeitem: this.state.codeitemMB,
        nameitem: this.state.nameitemMB,
        priceitem: this.state.priceitemMB,
        dateitem: moment(this.state.DateMB).format("YYYY-MM-DD HH:mm:ss"),
        statusitem: this.state.statusitemMB,
        noteitem: this.state.noteitemMB,
        type: "MB",
        datetimestart: moment(this.state.Date)
          .utc()
          .format("DD/MM/YYYY")
      });
      this.setState({
        codeitemMB: "",
        nameitemMB: "",
        priceitemMB: "",
        DateMB: new Date(),
        statusitemMB: "",
        noteitemMB: ""
      });
    } else if (tab === "RAM") {
      this.state.dataRAM.splice(this.state.dataRAM.length, 0, {
        codeitem: this.state.codeitemRAM,
        nameitem: this.state.nameitemRAM,
        priceitem: this.state.priceitemRAM,
        dateitem: moment(this.state.DateRAM).format("YYYY-MM-DD HH:mm:ss"),
        statusitem: this.state.statusitemRAM,
        noteitem: this.state.noteitemRAM,
        type: "RAM",
        datetimestart: moment(this.state.Date)
          .utc()
          .format("DD/MM/YYYY")
      });
      this.setState({
        codeitemRAM: "",
        nameitemRAM: "",
        priceitemRAM: "",
        DateRAM: new Date(),
        statusitemRAM: "",
        noteitemRAM: ""
      });
    } else if (tab === "HDD") {
      this.state.dataHDD.splice(this.state.dataHDD.length, 0, {
        codeitem: this.state.codeitemHDD,
        nameitem: this.state.nameitemHDD,
        priceitem: this.state.priceitemHDD,
        dateitem: moment(this.state.DateHDD).format("YYYY-MM-DD HH:mm:ss"),
        statusitem: this.state.statusitemHDD,
        noteitem: this.state.noteitemHDD,
        type: "HDD",
        datetimestart: moment(this.state.Date)
          .utc()
          .format("DD/MM/YYYY")
      });
      this.setState({
        codeitemHDD: "",
        nameitemHDD: "",
        priceitemHDD: "",
        DateHDD: new Date(),
        statusitemHDD: "",
        noteitemHDD: ""
      });
    } else if (tab === "MN") {
      this.state.dataMN.splice(this.state.dataMN.length, 0, {
        codeitem: this.state.codeitemMN,
        nameitem: this.state.nameitemMN,
        priceitem: this.state.priceitemMN,
        dateitem: moment(this.state.DateMN).format("YYYY-MM-DD HH:mm:ss"),
        statusitem: this.state.statusitemMN,
        noteitem: this.state.noteitemMN,
        type: "MN",
        datetimestart: moment(this.state.Date)
          .utc()
          .format("DD/MM/YYYY")
      });
      this.setState({
        codeitemMN: "",
        nameitemMN: "",
        priceitemMN: "",
        DateMN: new Date(),
        statusitemMN: "",
        noteitemMN: ""
      });
    } else if (tab === "VGA") {
      this.state.dataVGA.splice(this.state.dataVGA.length, 0, {
        codeitem: this.state.codeitemVGA,
        nameitem: this.state.nameitemVGA,
        priceitem: this.state.priceitemVGA,
        dateitem: moment(this.state.DateVGA).format("YYYY-MM-DD HH:mm:ss"),
        statusitem: this.state.statusitemVGA,
        noteitem: this.state.noteitemVGA,
        type: "VGA",
        datetimestart: moment(this.state.Date)
          .utc()
          .format("DD/MM/YYYY")
      });
      this.setState({
        codeitemVGA: "",
        nameitemVGA: "",
        priceitemVGA: "",
        DateVGA: new Date(),
        statusitemVGA: "",
        noteitemVGA: ""
      });
    } else if (tab === "LAN") {
      this.state.dataLAN.splice(this.state.dataLAN.length, 0, {
        codeitem: this.state.codeitemLAN,
        nameitem: this.state.nameitemLAN,
        priceitem: this.state.priceitemLAN,
        dateitem: moment(this.state.DateLAN).format("YYYY-MM-DD HH:mm:ss"),
        statusitem: this.state.statusitemLAN,
        noteitem: this.state.noteitemLAN,
        type: "LAN",
        datetimestart: moment(this.state.Date)
          .utc()
          .format("DD/MM/YYYY")
      });
      this.setState({
        codeitemLAN: "",
        nameitemLAN: "",
        priceitemLAN: "",
        DateLAN: new Date(),
        statusitemLAN: "",
        noteitemLAN: ""
      });
    } else if (tab === "PT") {
      this.state.dataPT.splice(this.state.dataPT.length, 0, {
        codeitem: this.state.codeitemPT,
        nameitem: this.state.nameitemPT,
        priceitem: this.state.priceitemPT,
        dateitem: moment(this.state.DatePT).format("YYYY-MM-DD HH:mm:ss"),
        statusitem: this.state.statusitemPT,
        noteitem: this.state.noteitemPT,
        type: "PT",
        datetimestart: moment(this.state.Date)
          .utc()
          .format("DD/MM/YYYY")
      });
      this.setState({
        codeitemPT: "",
        nameitemPT: "",
        priceitemPT: "",
        DatePT: new Date(),
        statusitemPT: "",
        noteitemPT: ""
      });
    } else if (tab === "OT") {
      this.state.dataOT.splice(this.state.dataOT.length, 0, {
        codeitem: this.state.codeitemOT,
        nameitem: this.state.nameitemOT,
        priceitem: this.state.priceitemOT,
        dateitem: moment(this.state.DateOT).format("YYYY-MM-DD HH:mm:ss"),
        statusitem: this.state.statusitemOT,
        noteitem: this.state.noteitemOT,
        type: "OT",
        datetimestart: moment(this.state.Date)
          .utc()
          .format("DD/MM/YYYY")
      });
      this.setState({
        codeitemOT: "",
        nameitemOT: "",
        priceitemOT: "",
        DateOT: new Date(),
        statusitemOT: "",
        noteitemOT: ""
      });
    }
  };

  deleteToTable = (index, tab) => e => {
    e.preventDefault();
    if (tab === "CPU") {
      let a = this.state.dataCPU;
      a.splice(index, 1);
      this.setState({
        dataCPU: a
      });
    } else if (tab === "MB") {
      let a = this.state.dataMB;
      a.splice(index, 1);
      this.setState({
        dataMB: a
      });
    } else if (tab === "RAM") {
      let a = this.state.dataRAM;
      a.splice(index, 1);
      this.setState({
        dataRAM: a
      });
    } else if (tab === "HDD") {
      let a = this.state.dataHDD;
      a.splice(index, 1);
      this.setState({
        dataHDD: a
      });
    } else if (tab === "MN") {
      let a = this.state.dataMN;
      a.splice(index, 1);
      this.setState({
        dataMN: a
      });
    } else if (tab === "VGA") {
      let a = this.state.dataVGA;
      a.splice(index, 1);
      this.setState({
        dataVGA: a
      });
    } else if (tab === "LAN") {
      let a = this.state.dataLAN;
      a.splice(index, 1);
      this.setState({
        dataLAN: a
      });
    } else if (tab === "PT") {
      let a = this.state.dataPT;
      a.splice(index, 1);
      this.setState({
        dataPT: a
      });
    } else if (tab === "OT") {
      let a = this.state.dataOT;
      a.splice(index, 1);
      this.setState({
        dataOT: a
      });
    }
  };

  modalUser = bool => {
    this.setState({
      modaluser: bool
    });
  };

  modalDept = bool => {
    this.setState({
      modaldept: bool
    });
  };

  modalOS = bool => {
    this.setState({
      modalos: bool
    });
  };

  User = (pfs_id, title, name, last_name, division, desc) => {
    this.setState({
      pfs_id: pfs_id,
      name: title + name + " " + last_name,
      depcode: division,
      depname: desc
    });
  };

  Dept = (depcode, desc) => {
    this.setState({
      depcode: depcode,
      depname: desc
    });
  };

  OS = (barcode, name, productkey) => {
    this.setState({
      barcode: barcode,
      nameos: name,
      productkey: productkey
    });
  };

  render() {
    const {
      bit,
      tel,
      pcname,
      ippc,
      productkey,
      mainboard,
      cpu,
      speed,
      vga,
      harddisk,
      sizehd,
      monitor,
      ram,
      sizeram,
      ups,
      lan,
      printer,
      printersn,
      price,
      priceall,
      note,
      codeitemCPU,
      nameitemCPU,
      priceitemCPU,
      statusitemCPU,
      noteitemCPU,
      codeitemMB,
      nameitemMB,
      priceitemMB,
      statusitemMB,
      noteitemMB,
      codeitemRAM,
      nameitemRAM,
      priceitemRAM,
      statusitemRAM,
      noteitemRAM,
      codeitemHDD,
      nameitemHDD,
      priceitemHDD,
      statusitemHDD,
      noteitemHDD,
      codeitemMN,
      nameitemMN,
      priceitemMN,
      statusitemMN,
      noteitemMN,
      codeitemVGA,
      nameitemVGA,
      priceitemVGA,
      statusitemVGA,
      noteitemVGA,
      codeitemLAN,
      nameitemLAN,
      priceitemLAN,
      statusitemLAN,
      noteitemLAN,
      codeitemPT,
      nameitemPT,
      priceitemPT,
      statusitemPT,
      noteitemPT,
      codeitemOT,
      nameitemOT,
      priceitemOT,
      statusitemOT,
      noteitemOT
    } = this.state;
    return (
      <div className="animated fadeIn">
        รายละเอียด
        <Row>
          <Col md="8">
            <Row style={{ margin: "1%" }}>
              <Col md="auto">ประเภท :</Col>
              <Col md="auto">
                {this.props.ID === undefined ? (
                  <select
                    onChange={this.onChangeDrop.bind(this)}
                    value={this.state.value}
                  >
                    <option value="PC">PC</option>
                    <option value="THIN">THIN</option>
                    <option value="NOTEBOOK">NOTEBOOK</option>
                    <option value="SERVER">SERVER</option>
                    <option value="MACBOOK">MACBOOK</option>
                    <option value="I-MAC">I-MAC</option>
                  </select>
                ) : (
                  <input value={this.state.value} disabled />
                )}
              </Col>
              <Col md="auto">วันที่เริ่มใช้งาน :</Col>

              <Col md="auto">
                {this.props.ID === undefined ? (
                  <DatePicker
                    value={this.state.Date}
                    onChange={this.handleChangeDate("Date")}
                    format="dd/MM/y"
                  />
                ) : (
                  <input
                    type="text"
                    value={moment(this.state.Date).format("DD/MM/YYYY")}
                    disabled
                  />
                )}
              </Col>
              <Col md="auto">โทร :</Col>
              <Col md="auto">
                <input
                  type="text"
                  name="tel"
                  values={tel}
                  value={this.state.tel}
                  onChange={this.handleChange}
                  style={{ width: "80px" }}
                />
              </Col>
            </Row>
            <Row style={{ margin: "2%" }}>
              <Col md="12">
                <label style={{ width: "80px" }}>ผู้ใช้งาน :</label>
                <input
                  type="text"
                  value={this.state.pfs_id}
                  disabled
                  style={{ width: "120px" }}
                />
                <button
                  type="button"
                  block
                  outline
                  color="secondary"
                  onClick={() => this.setState({ modaluser: true })}
                >
                  <i className="fa fa-search"></i>
                </button>
                <input
                  type="text"
                  value={this.state.name}
                  disabled
                  style={{ width: "500px" }}
                />
              </Col>
            </Row>
            <Row style={{ margin: "2%" }}>
              <Col md="12">
                <label style={{ width: "80px" }}>แผนก :</label>
                <input
                  type="text"
                  value={this.state.depcode}
                  disabled
                  style={{ width: "120px" }}
                />
                <button
                  type="button"
                  block
                  outline
                  color="secondary"
                  onClick={() => this.setState({ modaldept: true })}
                >
                  <i className="fa fa-search"></i>
                </button>
                <input
                  type="text"
                  value={this.state.depname}
                  disabled
                  style={{ width: "500px" }}
                />
              </Col>
            </Row>
            <Row style={{ margin: "2%" }}>
              <Col md="12">
                <label style={{ width: "80px" }}>OS :</label>
                <input
                  type="text"
                  value={this.state.barcode}
                  disabled
                  style={{ width: "120px" }}
                />
                <button
                  type="button"
                  block
                  outline
                  color="secondary"
                  onClick={() => this.setState({ modalos: true })}
                >
                  <i className="fa fa-search"></i>
                </button>
                <input
                  type="text"
                  value={this.state.nameos}
                  disabled
                  style={{ width: "500px" }}
                />
              </Col>
            </Row>
            <Row style={{ margin: "2%" }}>
              <Col md="auto">Product Key :</Col>
              <Col md="auto">
                <input
                  type="text"
                  name="productkey"
                  values={productkey}
                  value={this.state.productkey}
                  onChange={this.handleChange}
                  style={{ width: "400px" }}
                />
              </Col>
              <Col md="auto">
                <form>
                  <label style={{ marginRight: "10px" }}>
                    <input
                      type="radio"
                      name="bit"
                      value="32"
                      checked={this.state.bit === "32"}
                      onChange={this.handleChange}
                      style={{ margin: "5px" }}
                      values={bit}
                    />
                    32 BIT
                  </label>
                  <label>
                    <input
                      type="radio"
                      name="bit"
                      value="64"
                      checked={this.state.bit === "64"}
                      onChange={this.handleChange}
                      style={{ margin: "5px" }}
                      values={bit}
                    />
                    64 BIT
                  </label>
                </form>
              </Col>
            </Row>
            <Row style={{ margin: "2%" }}>
              <Col md="auto">ชื่อเครื่อง :</Col>
              <Col md="auto">
                <input
                  type="text"
                  name="pcname"
                  values={pcname}
                  value={this.state.pcname}
                  onChange={this.handleChange}
                  style={{ width: "200px" }}
                />
              </Col>
              <Col md="auto">IP ADDRESS :</Col>
              <Col md="auto">
                <input
                  type="text"
                  name="ippc"
                  values={ippc}
                  value={this.state.ippc}
                  onChange={this.handleChange}
                  style={{ width: "200px" }}
                />
              </Col>
            </Row>
          </Col>
          <Col md="4">
            <Row style={{ marginBottom: "5%" }}>
              <Col
                md="10"
                style={{ display: "flex", justifyContent: "center" }}
              >
                <img
                  src={
                    this.state.pfs_id === ""
                      ? pic1
                      : `${process.env.PUBLIC_URL}/employee/${this.state.pfs_id}.jpg`
                  }
                  width="80%"
                  alt="noimage"
                />
                {/* <input
                  type="file"
                  name="customFile"
                  accept="image/,"
                  onChange={this.onChange}
                  style={{ width: "100%", marginTop: "1%" }}
                /> */}
              </Col>
              <Col md="2"></Col>
            </Row>
          </Col>
        </Row>
        <Tabs defaultActiveKey="spec" id="uncontrolled-tab-example">
          <Tab eventKey="spec" title="SPEC">
            <Row>
              <Col md="12">
                <label style={{ width: "100px", textAlign: "right" }}>
                  Mainboard :
                </label>
                <input
                  type="text"
                  name="mainboard"
                  values={mainboard}
                  value={this.state.mainboard}
                  onChange={this.handleChange}
                  style={{ width: "300px", marginLeft: "1%" }}
                />
                <label
                  style={{
                    width: "100px",
                    marginLeft: "1%",
                    textAlign: "right"
                  }}
                >
                  CPU :
                </label>
                <input
                  type="text"
                  name="cpu"
                  values={cpu}
                  value={this.state.cpu}
                  onChange={this.handleChange}
                  style={{ width: "300px", marginLeft: "1%" }}
                />
                <label
                  style={{
                    width: "80px",
                    // marginLeft: "%",
                    textAlign: "right"
                  }}
                >
                  Speed :
                </label>
                <input
                  type="text"
                  name="speed"
                  values={speed}
                  value={this.state.speed}
                  onChange={this.handleChange}
                  style={{ width: "100px", marginLeft: "1%" }}
                />
                <label> Ghz</label>
              </Col>
              <Col md="12">
                <label style={{ width: "100px", textAlign: "right" }}>
                  VGA :
                </label>
                <input
                  type="text"
                  name="vga"
                  values={vga}
                  value={this.state.vga}
                  onChange={this.handleChange}
                  style={{ width: "300px", marginLeft: "1%" }}
                />
                <label
                  style={{
                    width: "100px",
                    marginLeft: "1%",
                    textAlign: "right"
                  }}
                >
                  HardDisk :
                </label>
                <input
                  type="text"
                  name="harddisk"
                  values={harddisk}
                  value={this.state.harddisk}
                  onChange={this.handleChange}
                  style={{ width: "300px", marginLeft: "1%" }}
                />
                <label
                  style={{
                    width: "80px",
                    // marginLeft: "%",
                    textAlign: "right"
                  }}
                >
                  Size :
                </label>
                <input
                  type="text"
                  name="sizehd"
                  values={sizehd}
                  value={this.state.sizehd}
                  onChange={this.handleChange}
                  style={{ width: "100px", marginLeft: "1%" }}
                />
                <label> GB</label>
              </Col>
              <Col md="12">
                <label style={{ width: "100px", textAlign: "right" }}>
                  Monitor :
                </label>
                <input
                  type="text"
                  name="monitor"
                  values={monitor}
                  value={this.state.monitor}
                  onChange={this.handleChange}
                  style={{ width: "300px", marginLeft: "1%" }}
                />
                <label
                  style={{
                    width: "100px",
                    marginLeft: "1%",
                    textAlign: "right"
                  }}
                >
                  RAM :
                </label>
                <input
                  type="text"
                  name="ram"
                  values={ram}
                  value={this.state.ram}
                  onChange={this.handleChange}
                  style={{ width: "300px", marginLeft: "1%" }}
                />
                <label
                  style={{
                    width: "80px",
                    // marginLeft: "%",
                    textAlign: "right"
                  }}
                >
                  Size :
                </label>
                <input
                  type="text"
                  name="sizeram"
                  values={sizeram}
                  value={this.state.sizeram}
                  onChange={this.handleChange}
                  style={{ width: "100px", marginLeft: "1%" }}
                />
                <label> MB</label>
              </Col>
              <Col md="12">
                <label style={{ width: "100px", textAlign: "right" }}>
                  Ups :
                </label>
                <input
                  type="text"
                  name="ups"
                  values={ups}
                  value={this.state.ups}
                  onChange={this.handleChange}
                  style={{ width: "300px", marginLeft: "1%" }}
                />
                <label
                  style={{
                    width: "100px",
                    marginLeft: "1%",
                    textAlign: "right"
                  }}
                >
                  LAN Card :
                </label>
                <input
                  type="text"
                  name="lan"
                  values={lan}
                  value={this.state.lan}
                  onChange={this.handleChange}
                  style={{ width: "300px", marginLeft: "1%" }}
                />
              </Col>
              <Col md="12">
                <label style={{ width: "100px", textAlign: "right" }}>
                  Printer :
                </label>
                <input
                  type="text"
                  name="printer"
                  values={printer}
                  value={this.state.printer}
                  onChange={this.handleChange}
                  style={{ width: "300px", marginLeft: "1%" }}
                />
                <label
                  style={{
                    width: "100px",
                    marginLeft: "1%",
                    textAlign: "right"
                  }}
                >
                  Printer S/N :
                </label>
                <input
                  type="text"
                  name="printersn"
                  values={printersn}
                  value={this.state.printersn}
                  onChange={this.handleChange}
                  style={{ width: "300px", marginLeft: "1%" }}
                />
              </Col>
              <Col md="12">
                <label style={{ width: "100px", textAlign: "right" }}>
                  ราคา :
                </label>
                <input
                  type="text"
                  name="price"
                  values={price}
                  value={this.state.price}
                  onChange={this.handleChange}
                  style={{ width: "300px", marginLeft: "1%" }}
                />
                <label
                  style={{
                    width: "100px",
                    marginLeft: "1%",
                    textAlign: "right"
                  }}
                >
                  รวมมูลค่า
                </label>
                <input
                  type="text"
                  name="priceall"
                  values={priceall}
                  value={this.state.priceall}
                  onChange={this.handleChange}
                  style={{
                    width: "300px",
                    backgroundColor: "black",
                    color: "white",
                    marginLeft: "1%"
                  }}
                />
              </Col>
              <Col md="12">
                <label style={{ width: "100px", textAlign: "right" }}>
                  หมายเหตุ :
                </label>
                <textarea
                  type="text"
                  name="note"
                  values={note}
                  value={this.state.note}
                  onChange={this.handleChange}
                  rows="2"
                  style={{ verticalAlign: "top", width: "725px", marginLeft: "1%" }}
                />
              </Col>
            </Row>
          </Tab>
          <Tab eventKey="cpu" title="CPU">
            <table width="100%" style={{ marginBottom: "2%" }}>
              <thead className="text-center table-active">
                <tr>
                  <th id="tb">ลำดับที่</th>
                  <th id="tb">รหัสทรัพย์สิน</th>
                  <th id="tb">ชื่อทรัพย์สิน</th>
                  <th id="tb">มูลค่าทรัพย์สิน</th>
                  <th id="tb">วันที่ครอบครอง</th>
                  <th id="tb">สถานะ</th>
                  <th id="tb">หมายเหตุ</th>
                  <th id="tb">#</th>
                </tr>
              </thead>
              <tbody>
                {this.state.dataCPU.map((item, index) => {
                  return (
                    <tr key={index}>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {index + 1}
                      </td>
                      <td id="tb">{item.codeitem}</td>
                      <td id="tb">{item.nameitem}</td>
                      <td id="tb" style={{ textAlign: "right" }}>
                        {item.priceitem}
                      </td>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {item.datetimestart}
                      </td>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {item.statusitem === 1 ? "ใช้งานอยู่" : "ไม่ใช้งานแล้ว"}
                      </td>
                      <td id="tb">{item.noteitem}</td>
                      <td
                        id="tb"
                        style={{ textAlign: "center", cursor: "pointer" }}
                        onClick={this.deleteToTable(index, "CPU")}
                      >
                        ลบ
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <Button
              color="info"
              onClick={() => this.setState({ tabCPU: !this.state.tabCPU })}
              style={{ marginBottom: "1%" }}
            >
              {this.state.tabCPU ? "ปิดหน้าแก้ไขข้อมูล" : "เปิดหน้าแก้ไขข้อมูล"}
            </Button>
            <Collapse in={this.state.tabCPU}>
              <Row>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    รหัสทรัพย์สิน :
                  </label>
                  <input
                    name="codeitemCPU"
                    values={codeitemCPU}
                    onChange={this.handleChange}
                    value={this.state.codeitemCPU}
                    type="text"
                    style={{ width: "300px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    ชื่อทรัพย์สิน :
                  </label>
                  <input
                    name="nameitemCPU"
                    values={nameitemCPU}
                    onChange={this.handleChange}
                    value={this.state.nameitemCPU}
                    type="text"
                    style={{ width: "700px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    มูลค่าทรัพย์สิน :
                  </label>
                  <input
                    name="priceitemCPU"
                    values={priceitemCPU}
                    onChange={this.handleChange}
                    value={this.state.priceitemCPU}
                    type="text"
                    style={{ width: "300px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    วันที่ครอบครอง :
                  </label>
                  <span style={{ marginLeft: "1%" }}>
                    <DatePicker
                      value={this.state.DateCPU}
                      onChange={this.handleChangeDate("DateCPU")}
                      format="dd/MM/y"
                    />
                  </span>
                  <label style={{ marginLeft: "1%" }}>สถานะ</label>
                  <label style={{ marginRight: "10px", color: "green" }}>
                    <input
                      type="radio"
                      name="statusitemCPU"
                      value="1"
                      checked={this.state.statusitemCPU === "1"}
                      onChange={this.handleChange}
                      style={{ margin: "5px" }}
                      values={statusitemCPU}
                    />
                    ใช้งานอยู่
                  </label>
                  <label style={{ color: "red" }}>
                    <input
                      type="radio"
                      name="statusitemCPU"
                      value="0"
                      checked={this.state.statusitemCPU === "0"}
                      onChange={this.handleChange}
                      style={{ margin: "5px" }}
                      values={statusitemCPU}
                    />
                    ไม่ใช้งานแล้ว
                  </label>
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    หมายเหตุ :
                  </label>
                  <textarea
                    name="noteitemCPU"
                    values={noteitemCPU}
                    onChange={this.handleChange}
                    value={this.state.noteitemCPU}
                    type="text"
                    rows="2"
                    style={{ verticalAlign: "top", width: "700px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <Button
                    color="success"
                    onClick={this.ToTable("CPU")}
                    style={{ margin: "1%" }}
                  >
                    เพิ่มข้อมูล
                  </Button>
                  <Button color="danger" style={{ margin: "1%" }}>
                    เคลียร์ข้อมูล
                  </Button>
                </Col>
              </Row>
            </Collapse>
          </Tab>
          <Tab eventKey="mainboard" title="MAINBOARD">
            <table width="100%" style={{ marginBottom: "2%" }}>
              <thead className="text-center table-active">
                <tr>
                  <th id="tb">ลำดับที่</th>
                  <th id="tb">รหัสทรัพย์สิน</th>
                  <th id="tb">ชื่อทรัพย์สิน</th>
                  <th id="tb">มูลค่าทรัพย์สิน</th>
                  <th id="tb">วันที่ครอบครอง</th>
                  <th id="tb">สถานะ</th>
                  <th id="tb">หมายเหตุ</th>
                  <th id="tb">#</th>
                </tr>
              </thead>
              <tbody>
                {this.state.dataMB.map((item, index) => {
                  return (
                    <tr key={index}>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {index + 1}
                      </td>
                      <td id="tb">{item.codeitem}</td>
                      <td id="tb">{item.nameitem}</td>
                      <td id="tb" style={{ textAlign: "right" }}>
                        {item.priceitem}
                      </td>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {item.datetimestart}
                      </td>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {item.statusitem === 1 ? "ใช้งานอยู่" : "ไม่ใช้งานแล้ว"}
                      </td>
                      <td id="tb">{item.noteitem}</td>
                      <td
                        id="tb"
                        style={{ textAlign: "center", cursor: "pointer" }}
                        onClick={this.deleteToTable(index, "MB")}
                      >
                        ลบ
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <Button
              color="info"
              onClick={() => this.setState({ tabMB: !this.state.tabMB })}
              style={{ marginBottom: "1%" }}
            >
              {this.state.tabMB ? "ปิดหน้าแก้ไขข้อมูล" : "เปิดหน้าแก้ไขข้อมูล"}
            </Button>
            <Collapse in={this.state.tabMB}>
              <Row>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    รหัสทรัพย์สิน :
                  </label>
                  <input
                    name="codeitemMB"
                    values={codeitemMB}
                    onChange={this.handleChange}
                    value={this.state.codeitemMB}
                    type="text"
                    style={{ width: "300px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    ชื่อทรัพย์สิน :
                  </label>
                  <input
                    name="nameitemMB"
                    values={nameitemMB}
                    onChange={this.handleChange}
                    value={this.state.nameitemMB}
                    type="text"
                    style={{ width: "700px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    มูลค่าทรัพย์สิน :
                  </label>
                  <input
                    name="priceitemMB"
                    values={priceitemMB}
                    onChange={this.handleChange}
                    value={this.state.priceitemMB}
                    type="text"
                    style={{ width: "300px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    วันที่ครอบครอง :
                  </label>
                  <span style={{ marginLeft: "1%" }}>
                    <DatePicker
                      value={this.state.DateMB}
                      onChange={this.handleChangeDate("DateMB")}
                      format="dd/MM/y"
                    />
                  </span>
                  <label style={{ marginLeft: "1%" }}>สถานะ</label>
                  <label style={{ marginRight: "10px", color: "green" }}>
                    <input
                      type="radio"
                      name="statusitemMB"
                      value="1"
                      checked={this.state.statusitemMB === "1"}
                      onChange={this.handleChange}
                      style={{ margin: "5px" }}
                      values={statusitemMB}
                    />
                    ใช้งานอยู่
                  </label>
                  <label style={{ color: "red" }}>
                    <input
                      type="radio"
                      name="statusitemMB"
                      value="0"
                      checked={this.state.statusitemMB === "0"}
                      onChange={this.handleChange}
                      style={{ margin: "5px" }}
                      values={statusitemMB}
                    />
                    ไม่ใช้งานแล้ว
                  </label>
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    หมายเหตุ :
                  </label>
                  <textarea
                    name="noteitemMB"
                    values={noteitemMB}
                    onChange={this.handleChange}
                    value={this.state.noteitemMB}
                    type="text"
                    rows="2"
                    style={{ verticalAlign: "top", width: "700px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <Button
                    color="success"
                    onClick={this.ToTable("MB")}
                    style={{ margin: "1%" }}
                  >
                    เพิ่มข้อมูล
                  </Button>
                  <Button color="danger" style={{ margin: "1%" }}>
                    เคลียร์ข้อมูล
                  </Button>
                </Col>
              </Row>
            </Collapse>
          </Tab>
          <Tab eventKey="ram" title="RAM">
            <table width="100%" style={{ marginBottom: "2%" }}>
              <thead className="text-center table-active">
                <tr>
                  <th id="tb">ลำดับที่</th>
                  <th id="tb">รหัสทรัพย์สิน</th>
                  <th id="tb">ชื่อทรัพย์สิน</th>
                  <th id="tb">มูลค่าทรัพย์สิน</th>
                  <th id="tb">วันที่ครอบครอง</th>
                  <th id="tb">สถานะ</th>
                  <th id="tb">หมายเหตุ</th>
                  <th id="tb">#</th>
                </tr>
              </thead>
              <tbody>
                {this.state.dataRAM.map((item, index) => {
                  return (
                    <tr key={index}>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {index + 1}
                      </td>
                      <td id="tb">{item.codeitem}</td>
                      <td id="tb">{item.nameitem}</td>
                      <td id="tb" style={{ textAlign: "right" }}>
                        {item.priceitem}
                      </td>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {item.datetimestart}
                      </td>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {item.statusitem === 1 ? "ใช้งานอยู่" : "ไม่ใช้งานแล้ว"}
                      </td>
                      <td id="tb">{item.noteitem}</td>
                      <td
                        id="tb"
                        style={{ textAlign: "center", cursor: "pointer" }}
                        onClick={this.deleteToTable(index, "RAM")}
                      >
                        ลบ
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <Button
              color="info"
              onClick={() => this.setState({ tabRAM: !this.state.tabRAM })}
              style={{ marginBottom: "1%" }}
            >
              {this.state.tabRAM ? "ปิดหน้าแก้ไขข้อมูล" : "เปิดหน้าแก้ไขข้อมูล"}
            </Button>
            <Collapse in={this.state.tabRAM}>
              <Row>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    รหัสทรัพย์สิน :
                  </label>
                  <input
                    name="codeitemRAM"
                    values={codeitemRAM}
                    onChange={this.handleChange}
                    value={this.state.codeitemRAM}
                    type="text"
                    style={{ width: "300px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    ชื่อทรัพย์สิน :
                  </label>
                  <input
                    name="nameitemRAM"
                    values={nameitemRAM}
                    onChange={this.handleChange}
                    value={this.state.nameitemRAM}
                    type="text"
                    style={{ width: "700px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    มูลค่าทรัพย์สิน :
                  </label>
                  <input
                    name="priceitemRAM"
                    values={priceitemRAM}
                    onChange={this.handleChange}
                    value={this.state.priceitemRAM}
                    type="text"
                    style={{ width: "300px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    วันที่ครอบครอง :
                  </label>
                  <span style={{ marginLeft: "1%" }}>
                    <DatePicker
                      value={this.state.DateRAM}
                      onChange={this.handleChangeDate("DateRAM")}
                      format="dd/MM/y"
                    />
                  </span>
                  <label style={{ marginLeft: "1%" }}>สถานะ</label>
                  <label style={{ marginRight: "10px", color: "green" }}>
                    <input
                      type="radio"
                      name="statusitemRAM"
                      value="1"
                      checked={this.state.statusitemRAM === "1"}
                      onChange={this.handleChange}
                      style={{ margin: "5px" }}
                      values={statusitemRAM}
                    />
                    ใช้งานอยู่
                  </label>
                  <label style={{ color: "red" }}>
                    <input
                      type="radio"
                      name="statusitemRAM"
                      value="0"
                      checked={this.state.statusitemRAM === "0"}
                      onChange={this.handleChange}
                      style={{ margin: "5px" }}
                      values={statusitemRAM}
                    />
                    ไม่ใช้งานแล้ว
                  </label>
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    หมายเหตุ :
                  </label>
                  <textarea
                    name="noteitemRAM"
                    values={noteitemRAM}
                    onChange={this.handleChange}
                    value={this.state.noteitemRAM}
                    type="text"
                    rows="2"
                    style={{ verticalAlign: "top", width: "700px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <Button
                    color="success"
                    onClick={this.ToTable("RAM")}
                    style={{ margin: "1%" }}
                  >
                    เพิ่มข้อมูล
                  </Button>
                  <Button color="danger" style={{ margin: "1%" }}>
                    เคลียร์ข้อมูล
                  </Button>
                </Col>
              </Row>
            </Collapse>
          </Tab>
          <Tab eventKey="hdd" title="HDD">
            <table width="100%" style={{ marginBottom: "2%" }}>
              <thead className="text-center table-active">
                <tr>
                  <th id="tb">ลำดับที่</th>
                  <th id="tb">รหัสทรัพย์สิน</th>
                  <th id="tb">ชื่อทรัพย์สิน</th>
                  <th id="tb">มูลค่าทรัพย์สิน</th>
                  <th id="tb">วันที่ครอบครอง</th>
                  <th id="tb">สถานะ</th>
                  <th id="tb">หมายเหตุ</th>
                  <th id="tb">#</th>
                </tr>
              </thead>
              <tbody>
                {this.state.dataHDD.map((item, index) => {
                  return (
                    <tr key={index}>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {index + 1}
                      </td>
                      <td id="tb">{item.codeitem}</td>
                      <td id="tb">{item.nameitem}</td>
                      <td id="tb" style={{ textAlign: "right" }}>
                        {item.priceitem}
                      </td>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {item.datetimestart}
                      </td>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {item.statusitem === 1 ? "ใช้งานอยู่" : "ไม่ใช้งานแล้ว"}
                      </td>
                      <td id="tb">{item.noteitem}</td>
                      <td
                        id="tb"
                        style={{ textAlign: "center", cursor: "pointer" }}
                        onClick={this.deleteToTable(index, "HDD")}
                      >
                        ลบ
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <Button
              color="info"
              onClick={() => this.setState({ tabHDD: !this.state.tabHDD })}
              style={{ marginBottom: "1%" }}
            >
              {this.state.tabHDD ? "ปิดหน้าแก้ไขข้อมูล" : "เปิดหน้าแก้ไขข้อมูล"}
            </Button>
            <Collapse in={this.state.tabHDD}>
              <Row>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    รหัสทรัพย์สิน :
                  </label>
                  <input
                    name="codeitemHDD"
                    values={codeitemHDD}
                    onChange={this.handleChange}
                    value={this.state.codeitemHDD}
                    type="text"
                    style={{ width: "300px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    ชื่อทรัพย์สิน :
                  </label>
                  <input
                    name="nameitemHDD"
                    values={nameitemHDD}
                    onChange={this.handleChange}
                    value={this.state.nameitemHDD}
                    type="text"
                    style={{ width: "700px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    มูลค่าทรัพย์สิน :
                  </label>
                  <input
                    name="priceitemHDD"
                    values={priceitemHDD}
                    onChange={this.handleChange}
                    value={this.state.priceitemHDD}
                    type="text"
                    style={{ width: "300px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    วันที่ครอบครอง :
                  </label>
                  <span style={{ marginLeft: "1%" }}>
                    <DatePicker
                      value={this.state.DateHDD}
                      onChange={this.handleChangeDate("DateHDD")}
                      format="dd/MM/y"
                    />
                  </span>
                  <label style={{ marginLeft: "1%" }}>สถานะ</label>
                  <label style={{ marginRight: "10px", color: "green" }}>
                    <input
                      type="radio"
                      name="statusitemHDD"
                      value="1"
                      checked={this.state.statusitemHDD === "1"}
                      onChange={this.handleChange}
                      style={{ margin: "5px" }}
                      values={statusitemHDD}
                    />
                    ใช้งานอยู่
                  </label>
                  <label style={{ color: "red" }}>
                    <input
                      type="radio"
                      name="statusitemHDD"
                      value="0"
                      checked={this.state.statusitemHDD === "0"}
                      onChange={this.handleChange}
                      style={{ margin: "5px" }}
                      values={statusitemHDD}
                    />
                    ไม่ใช้งานแล้ว
                  </label>
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    หมายเหตุ :
                  </label>
                  <textarea
                    name="noteitemHDD"
                    values={noteitemHDD}
                    onChange={this.handleChange}
                    value={this.state.noteitemHDD}
                    type="text"
                    rows="2"
                    style={{ verticalAlign: "top", width: "700px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <Button
                    color="success"
                    onClick={this.ToTable("HDD")}
                    style={{ margin: "1%" }}
                  >
                    เพิ่มข้อมูล
                  </Button>
                  <Button color="danger" style={{ margin: "1%" }}>
                    เคลียร์ข้อมูล
                  </Button>
                </Col>
              </Row>
            </Collapse>
          </Tab>
          <Tab eventKey="monitor" title="Monitor">
            <table width="100%" style={{ marginBottom: "2%" }}>
              <thead className="text-center table-active">
                <tr>
                  <th id="tb">ลำดับที่</th>
                  <th id="tb">รหัสทรัพย์สิน</th>
                  <th id="tb">ชื่อทรัพย์สิน</th>
                  <th id="tb">มูลค่าทรัพย์สิน</th>
                  <th id="tb">วันที่ครอบครอง</th>
                  <th id="tb">สถานะ</th>
                  <th id="tb">หมายเหตุ</th>
                  <th id="tb">#</th>
                </tr>
              </thead>
              <tbody>
                {this.state.dataMN.map((item, index) => {
                  return (
                    <tr key={index}>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {index + 1}
                      </td>
                      <td id="tb">{item.codeitem}</td>
                      <td id="tb">{item.nameitem}</td>
                      <td id="tb" style={{ textAlign: "right" }}>
                        {item.priceitem}
                      </td>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {item.datetimestart}
                      </td>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {item.statusitem === 1 ? "ใช้งานอยู่" : "ไม่ใช้งานแล้ว"}
                      </td>
                      <td id="tb">{item.noteitem}</td>
                      <td
                        id="tb"
                        style={{ textAlign: "center", cursor: "pointer" }}
                        onClick={this.deleteToTable(index, "MN")}
                      >
                        ลบ
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <Button
              color="info"
              onClick={() => this.setState({ tabMN: !this.state.tabMN })}
              style={{ marginBottom: "1%" }}
            >
              {this.state.tabMN ? "ปิดหน้าแก้ไขข้อมูล" : "เปิดหน้าแก้ไขข้อมูล"}
            </Button>
            <Collapse in={this.state.tabMN}>
              <Row>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    รหัสทรัพย์สิน :
                  </label>
                  <input
                    name="codeitemMN"
                    values={codeitemMN}
                    onChange={this.handleChange}
                    value={this.state.codeitemMN}
                    type="text"
                    style={{ width: "300px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    ชื่อทรัพย์สิน :
                  </label>
                  <input
                    name="nameitemMN"
                    values={nameitemMN}
                    onChange={this.handleChange}
                    value={this.state.nameitemMN}
                    type="text"
                    style={{ width: "700px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    มูลค่าทรัพย์สิน :
                  </label>
                  <input
                    name="priceitemMN"
                    values={priceitemMN}
                    onChange={this.handleChange}
                    value={this.state.priceitemMN}
                    type="text"
                    style={{ width: "300px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    วันที่ครอบครอง :
                  </label>
                  <span style={{ marginLeft: "1%" }}>
                    <DatePicker
                      value={this.state.DateMN}
                      onChange={this.handleChangeDate("DateMN")}
                      format="dd/MM/y"
                    />
                  </span>
                  <label style={{ marginLeft: "1%" }}>สถานะ</label>
                  <label style={{ marginRight: "10px", color: "green" }}>
                    <input
                      type="radio"
                      name="statusitemMN"
                      value="1"
                      checked={this.state.statusitemMN === "1"}
                      onChange={this.handleChange}
                      style={{ margin: "5px" }}
                      values={statusitemMN}
                    />
                    ใช้งานอยู่
                  </label>
                  <label style={{ color: "red" }}>
                    <input
                      type="radio"
                      name="statusitemMN"
                      value="0"
                      checked={this.state.statusitemMN === "0"}
                      onChange={this.handleChange}
                      style={{ margin: "5px" }}
                      values={statusitemMN}
                    />
                    ไม่ใช้งานแล้ว
                  </label>
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    หมายเหตุ :
                  </label>
                  <textarea
                    name="noteitemMN"
                    values={noteitemMN}
                    onChange={this.handleChange}
                    value={this.state.noteitemMN}
                    type="text"
                    rows="2"
                    style={{ verticalAlign: "top", width: "700px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <Button
                    color="success"
                    onClick={this.ToTable("MN")}
                    style={{ margin: "1%" }}
                  >
                    เพิ่มข้อมูล
                  </Button>
                  <Button color="danger" style={{ margin: "1%" }}>
                    เคลียร์ข้อมูล
                  </Button>
                </Col>
              </Row>
            </Collapse>
          </Tab>
          <Tab eventKey="vga" title="VGA Card">
            <table width="100%" style={{ marginBottom: "2%" }}>
              <thead className="text-center table-active">
                <tr>
                  <th id="tb">ลำดับที่</th>
                  <th id="tb">รหัสทรัพย์สิน</th>
                  <th id="tb">ชื่อทรัพย์สิน</th>
                  <th id="tb">มูลค่าทรัพย์สิน</th>
                  <th id="tb">วันที่ครอบครอง</th>
                  <th id="tb">สถานะ</th>
                  <th id="tb">หมายเหตุ</th>
                  <th id="tb">#</th>
                </tr>
              </thead>
              <tbody>
                {this.state.dataVGA.map((item, index) => {
                  return (
                    <tr key={index}>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {index + 1}
                      </td>
                      <td id="tb">{item.codeitem}</td>
                      <td id="tb">{item.nameitem}</td>
                      <td id="tb" style={{ textAlign: "right" }}>
                        {item.priceitem}
                      </td>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {item.datetimestart}
                      </td>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {item.statusitem === 1 ? "ใช้งานอยู่" : "ไม่ใช้งานแล้ว"}
                      </td>
                      <td id="tb">{item.noteitem}</td>
                      <td
                        id="tb"
                        style={{ textAlign: "center", cursor: "pointer" }}
                        onClick={this.deleteToTable(index, "VGA")}
                      >
                        ลบ
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <Button
              color="info"
              onClick={() => this.setState({ tabVGA: !this.state.tabVGA })}
              style={{ marginBottom: "1%" }}
            >
              {this.state.tabVGA ? "ปิดหน้าแก้ไขข้อมูล" : "เปิดหน้าแก้ไขข้อมูล"}
            </Button>
            <Collapse in={this.state.tabVGA}>
              <Row>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    รหัสทรัพย์สิน :
                  </label>
                  <input
                    name="codeitemVGA"
                    values={codeitemVGA}
                    onChange={this.handleChange}
                    value={this.state.codeitemVGA}
                    type="text"
                    style={{ width: "300px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    ชื่อทรัพย์สิน :
                  </label>
                  <input
                    name="nameitemVGA"
                    values={nameitemVGA}
                    onChange={this.handleChange}
                    value={this.state.nameitemVGA}
                    type="text"
                    style={{ width: "700px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    มูลค่าทรัพย์สิน :
                  </label>
                  <input
                    name="priceitemVGA"
                    values={priceitemVGA}
                    onChange={this.handleChange}
                    value={this.state.priceitemVGA}
                    type="text"
                    style={{ width: "300px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    วันที่ครอบครอง :
                  </label>
                  <span style={{ marginLeft: "1%" }}>
                    <DatePicker
                      value={this.state.DateVGA}
                      onChange={this.handleChangeDate("DateVGA")}
                      format="dd/MM/y"
                    />
                  </span>
                  <label style={{ marginLeft: "1%" }}>สถานะ</label>
                  <label style={{ marginRight: "10px", color: "green" }}>
                    <input
                      type="radio"
                      name="statusitemVGA"
                      value="1"
                      checked={this.state.statusitemVGA === "1"}
                      onChange={this.handleChange}
                      style={{ margin: "5px" }}
                      values={statusitemVGA}
                    />
                    ใช้งานอยู่
                  </label>
                  <label style={{ color: "red" }}>
                    <input
                      type="radio"
                      name="statusitemVGA"
                      value="0"
                      checked={this.state.statusitemVGA === "0"}
                      onChange={this.handleChange}
                      style={{ margin: "5px" }}
                      values={statusitemVGA}
                    />
                    ไม่ใช้งานแล้ว
                  </label>
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    หมายเหตุ :
                  </label>
                  <textarea
                    name="noteitemVGA"
                    values={noteitemVGA}
                    onChange={this.handleChange}
                    value={this.state.noteitemVGA}
                    type="text"
                    rows="2"
                    style={{ verticalAlign: "top", width: "700px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <Button
                    color="success"
                    onClick={this.ToTable("VGA")}
                    style={{ margin: "1%" }}
                  >
                    เพิ่มข้อมูล
                  </Button>
                  <Button color="danger" style={{ margin: "1%" }}>
                    เคลียร์ข้อมูล
                  </Button>
                </Col>
              </Row>
            </Collapse>
          </Tab>
          <Tab eventKey="lan" title="LAN Card">
            <table width="100%" style={{ marginBottom: "2%" }}>
              <thead className="text-center table-active">
                <tr>
                  <th id="tb">ลำดับที่</th>
                  <th id="tb">รหัสทรัพย์สิน</th>
                  <th id="tb">ชื่อทรัพย์สิน</th>
                  <th id="tb">มูลค่าทรัพย์สิน</th>
                  <th id="tb">วันที่ครอบครอง</th>
                  <th id="tb">สถานะ</th>
                  <th id="tb">หมายเหตุ</th>
                  <th id="tb">#</th>
                </tr>
              </thead>
              <tbody>
                {this.state.dataLAN.map((item, index) => {
                  return (
                    <tr key={index}>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {index + 1}
                      </td>
                      <td id="tb">{item.codeitem}</td>
                      <td id="tb">{item.nameitem}</td>
                      <td id="tb" style={{ textAlign: "right" }}>
                        {item.priceitem}
                      </td>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {item.datetimestart}
                      </td>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {item.statusitem === 1 ? "ใช้งานอยู่" : "ไม่ใช้งานแล้ว"}
                      </td>
                      <td id="tb">{item.noteitem}</td>
                      <td
                        id="tb"
                        style={{ textAlign: "center", cursor: "pointer" }}
                        onClick={this.deleteToTable(index, "LAN")}
                      >
                        ลบ
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <Button
              color="info"
              onClick={() => this.setState({ tabLAN: !this.state.tabLAN })}
              style={{ marginBottom: "1%" }}
            >
              {this.state.tabLAN ? "ปิดหน้าแก้ไขข้อมูล" : "เปิดหน้าแก้ไขข้อมูล"}
            </Button>
            <Collapse in={this.state.tabLAN}>
              <Row>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    รหัสทรัพย์สิน :
                  </label>
                  <input
                    name="codeitemLAN"
                    values={codeitemLAN}
                    onChange={this.handleChange}
                    value={this.state.codeitemLAN}
                    type="text"
                    style={{ width: "300px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    ชื่อทรัพย์สิน :
                  </label>
                  <input
                    name="nameitemLAN"
                    values={nameitemLAN}
                    onChange={this.handleChange}
                    value={this.state.nameitemLAN}
                    type="text"
                    style={{ width: "700px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    มูลค่าทรัพย์สิน :
                  </label>
                  <input
                    name="priceitemLAN"
                    values={priceitemLAN}
                    onChange={this.handleChange}
                    value={this.state.priceitemLAN}
                    type="text"
                    style={{ width: "300px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    วันที่ครอบครอง :
                  </label>
                  <span style={{ marginLeft: "1%" }}>
                    <DatePicker
                      value={this.state.DateLAN}
                      onChange={this.handleChangeDate("DateLAN")}
                      format="dd/MM/y"
                    />
                  </span>
                  <label style={{ marginLeft: "1%" }}>สถานะ</label>
                  <label style={{ marginRight: "10px", color: "green" }}>
                    <input
                      type="radio"
                      name="statusitemLAN"
                      value="1"
                      checked={this.state.statusitemLAN === "1"}
                      onChange={this.handleChange}
                      style={{ margin: "5px" }}
                      values={statusitemLAN}
                    />
                    ใช้งานอยู่
                  </label>
                  <label style={{ color: "red" }}>
                    <input
                      type="radio"
                      name="statusitemLAN"
                      value="0"
                      checked={this.state.statusitemLAN === "0"}
                      onChange={this.handleChange}
                      style={{ margin: "5px" }}
                      values={statusitemLAN}
                    />
                    ไม่ใช้งานแล้ว
                  </label>
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    หมายเหตุ :
                  </label>
                  <textarea
                    name="noteitemLAN"
                    values={noteitemLAN}
                    onChange={this.handleChange}
                    value={this.state.noteitemLAN}
                    type="text"
                    rows="2"
                    style={{ verticalAlign: "top", width: "700px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <Button
                    color="success"
                    onClick={this.ToTable("LAN")}
                    style={{ margin: "1%" }}
                  >
                    เพิ่มข้อมูล
                  </Button>
                  <Button color="danger" style={{ margin: "1%" }}>
                    เคลียร์ข้อมูล
                  </Button>
                </Col>
              </Row>
            </Collapse>
          </Tab>
          <Tab eventKey="printer" title="เครื่องพิมพ์">
            <table width="100%" style={{ marginBottom: "2%" }}>
              <thead className="text-center table-active">
                <tr>
                  <th id="tb">ลำดับที่</th>
                  <th id="tb">รหัสทรัพย์สิน</th>
                  <th id="tb">ชื่อทรัพย์สิน</th>
                  <th id="tb">มูลค่าทรัพย์สิน</th>
                  <th id="tb">วันที่ครอบครอง</th>
                  <th id="tb">สถานะ</th>
                  <th id="tb">หมายเหตุ</th>
                  <th id="tb">#</th>
                </tr>
              </thead>
              <tbody>
                {this.state.dataPT.map((item, index) => {
                  return (
                    <tr key={index}>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {index + 1}
                      </td>
                      <td id="tb">{item.codeitem}</td>
                      <td id="tb">{item.nameitem}</td>
                      <td id="tb" style={{ textAlign: "right" }}>
                        {item.priceitem}
                      </td>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {item.datetimestart}
                      </td>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {item.statusitem === 1 ? "ใช้งานอยู่" : "ไม่ใช้งานแล้ว"}
                      </td>
                      <td id="tb">{item.noteitem}</td>
                      <td
                        id="tb"
                        style={{ textAlign: "center", cursor: "pointer" }}
                        onClick={this.deleteToTable(index, "PT")}
                      >
                        ลบ
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <Button
              color="info"
              onClick={() => this.setState({ tabPT: !this.state.tabPT })}
              style={{ marginBottom: "1%" }}
            >
              {this.state.tabPT ? "ปิดหน้าแก้ไขข้อมูล" : "เปิดหน้าแก้ไขข้อมูล"}
            </Button>
            <Collapse in={this.state.tabPT}>
              <Row>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    รหัสทรัพย์สิน :
                  </label>
                  <input
                    name="codeitemPT"
                    values={codeitemPT}
                    onChange={this.handleChange}
                    value={this.state.codeitemPT}
                    type="text"
                    style={{ width: "300px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    ชื่อทรัพย์สิน :
                  </label>
                  <input
                    name="nameitemPT"
                    values={nameitemPT}
                    onChange={this.handleChange}
                    value={this.state.nameitemPT}
                    type="text"
                    style={{ width: "700px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    มูลค่าทรัพย์สิน :
                  </label>
                  <input
                    name="priceitemPT"
                    values={priceitemPT}
                    onChange={this.handleChange}
                    value={this.state.priceitemPT}
                    type="text"
                    style={{ width: "300px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    วันที่ครอบครอง :
                  </label>
                  <span style={{ marginLeft: "1%" }}>
                    <DatePicker
                      value={this.state.DatePT}
                      onChange={this.handleChangeDate("DatePT")}
                      format="dd/MM/y"
                    />
                  </span>
                  <label style={{ marginLeft: "1%" }}>สถานะ</label>
                  <label style={{ marginRight: "10px", color: "green" }}>
                    <input
                      type="radio"
                      name="statusitemPT"
                      value="1"
                      checked={this.state.statusitemPT === "1"}
                      onChange={this.handleChange}
                      style={{ margin: "5px" }}
                      values={statusitemPT}
                    />
                    ใช้งานอยู่
                  </label>
                  <label style={{ color: "red" }}>
                    <input
                      type="radio"
                      name="statusitemPT"
                      value="0"
                      checked={this.state.statusitemPT === "0"}
                      onChange={this.handleChange}
                      style={{ margin: "5px" }}
                      values={statusitemPT}
                    />
                    ไม่ใช้งานแล้ว
                  </label>
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    หมายเหตุ :
                  </label>
                  <textarea
                    name="noteitemPT"
                    values={noteitemPT}
                    onChange={this.handleChange}
                    value={this.state.noteitemPT}
                    type="text"
                    rows="2"
                    style={{ verticalAlign: "top", width: "700px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <Button
                    color="success"
                    onClick={this.ToTable("PT")}
                    style={{ margin: "1%" }}
                  >
                    เพิ่มข้อมูล
                  </Button>
                  <Button color="danger" style={{ margin: "1%" }}>
                    เคลียร์ข้อมูล
                  </Button>
                </Col>
              </Row>
            </Collapse>
          </Tab>
          <Tab eventKey="other" title="ทรัพย์สินอื่น ๆ">
            <table width="100%" style={{ marginBottom: "2%" }}>
              <thead className="text-center table-active">
                <tr>
                  <th id="tb">ลำดับที่</th>
                  <th id="tb">รหัสทรัพย์สิน</th>
                  <th id="tb">ชื่อทรัพย์สิน</th>
                  <th id="tb">มูลค่าทรัพย์สิน</th>
                  <th id="tb">วันที่ครอบครอง</th>
                  <th id="tb">สถานะ</th>
                  <th id="tb">หมายเหตุ</th>
                  <th id="tb">#</th>
                </tr>
              </thead>
              <tbody>
                {this.state.dataOT.map((item, index) => {
                  return (
                    <tr key={index}>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {index + 1}
                      </td>
                      <td id="tb">{item.codeitem}</td>
                      <td id="tb">{item.nameitem}</td>
                      <td id="tb" style={{ textAlign: "right" }}>
                        {item.priceitem}
                      </td>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {item.datetimestart}
                      </td>
                      <td id="tb" style={{ textAlign: "center" }}>
                        {item.statusitem === 1 ? "ใช้งานอยู่" : "ไม่ใช้งานแล้ว"}
                      </td>
                      <td id="tb">{item.noteitem}</td>
                      <td
                        id="tb"
                        style={{ textAlign: "center", cursor: "pointer" }}
                        onClick={this.deleteToTable(index, "OT")}
                      >
                        ลบ
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <Button
              color="info"
              onClick={() => this.setState({ tabOT: !this.state.tabOT })}
              style={{ marginBottom: "1%" }}
            >
              {this.state.tabOT ? "ปิดหน้าแก้ไขข้อมูล" : "เปิดหน้าแก้ไขข้อมูล"}
            </Button>
            <Collapse in={this.state.tabOT}>
              <Row>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    รหัสทรัพย์สิน :
                  </label>
                  <input
                    name="codeitemOT"
                    values={codeitemOT}
                    onChange={this.handleChange}
                    value={this.state.codeitemOT}
                    type="text"
                    style={{ width: "300px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    ชื่อทรัพย์สิน :
                  </label>
                  <input
                    name="nameitemOT"
                    values={nameitemOT}
                    onChange={this.handleChange}
                    value={this.state.nameitemOT}
                    type="text"
                    style={{ width: "700px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    มูลค่าทรัพย์สิน :
                  </label>
                  <input
                    name="priceitemOT"
                    values={priceitemOT}
                    onChange={this.handleChange}
                    value={this.state.priceitemOT}
                    type="text"
                    style={{ width: "300px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    วันที่ครอบครอง :
                  </label>
                  <span style={{ marginLeft: "1%" }}>
                    <DatePicker
                      value={this.state.DateOT}
                      onChange={this.handleChangeDate("DateOT")}
                      format="dd/MM/y"
                    />
                  </span>
                  <label style={{ marginLeft: "1%" }}>สถานะ</label>
                  <label style={{ marginRight: "10px", color: "green" }}>
                    <input
                      type="radio"
                      name="statusitemOT"
                      value="1"
                      checked={this.state.statusitemOT === "1"}
                      onChange={this.handleChange}
                      style={{ margin: "5px" }}
                      values={statusitemOT}
                    />
                    ใช้งานอยู่
                  </label>
                  <label style={{ color: "red" }}>
                    <input
                      type="radio"
                      name="statusitemOT"
                      value="0"
                      checked={this.state.statusitemOT === "0"}
                      onChange={this.handleChange}
                      style={{ margin: "5px" }}
                      values={statusitemOT}
                    />
                    ไม่ใช้งานแล้ว
                  </label>
                </Col>
                <Col md="12">
                  <label style={{ width: "120px", textAlign: "right" }}>
                    หมายเหตุ :
                  </label>
                  <textarea
                    name="noteitemOT"
                    values={noteitemOT}
                    onChange={this.handleChange}
                    value={this.state.noteitemOT}
                    type="text"
                    rows="2"
                    style={{ verticalAlign: "top", width: "700px", marginLeft: "1%" }}
                  />
                </Col>
                <Col md="12">
                  <Button
                    color="success"
                    onClick={this.ToTable("OT")}
                    style={{ margin: "1%" }}
                  >
                    เพิ่มข้อมูล
                  </Button>
                  <Button color="danger" style={{ margin: "1%" }}>
                    เคลียร์ข้อมูล
                  </Button>
                </Col>
              </Row>
            </Collapse>
          </Tab>
        </Tabs>
        <ModalUser
          Modal={this.state.modaluser}
          set={this.modalUser}
          getUser={this.User}
          REQ="Computer"
        />
        <ModalDept
          Modal={this.state.modaldept}
          set={this.modalDept}
          getDept={this.Dept}
          REQ="Computer"
        />
        <ModalOS
          Modal={this.state.modalos}
          set={this.modalOS}
          getOS={this.OS}
        />
      </div>
    );
  }
}

export default Other;
