import React from "react";
import { Col, Row, Button } from "reactstrap";
import { Tabs, Tab, Collapse, Card } from "react-bootstrap";
import Swal from "sweetalert2";
import { REQUEST_URL } from "../../../config";
import Cookies from "js-cookie";
import moment from "moment";
import Axios from "axios";
import ModalSoftwareAdda from "../Modal/ModalSoftwareAdda";

class SoftwareAdda extends React.Component {
  state = {
    datasoftwareadda: []
  };

  componentDidMount = () => {
    this.loadSoftwareAddaEdit();
  };

  loadSoftwareAddaEdit = async () => {
    Axios.post(
      REQUEST_URL + "/computer/loadsoftwareaddaedit",
      {
        id: this.props.ID
      },
      { headers: { authorization: Cookies.get("webcomputer") } }
    ).then(response => {
      if (response.data.status) {
        for (let x = 0; x < response.data.data.length; x++) {
          let SWADDA = this.state.datasoftwareadda;
          this.state.datasoftwareadda.splice(this.state.datasoftwareadda.length, 0, {
            appID: response.data.data[x].appID,
            appName: response.data.data[x].appName,
            appNameEng: response.data.data[x].appNameEng,
            reMark: response.data.data[x].reMark,
          });
          this.setState({
            datasoftwareadda: SWADDA
          });
        }
      } else {
        this.props.history.push("/login");
      }
    });
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (prevState !== this.state) {
      this.props.getSoftwareAdda(this.state.datasoftwareadda);
    }
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  modalSoftwareAdda = bool => {
    this.setState({
      modalsoftwareadda: bool
    });
  };

  SoftwareAdda = (appID, appName, appNameEng, reMark) => {
    this.state.datasoftwareadda.splice(this.state.datasoftwareadda.length, 0, {
      appID: appID,
      appName: appName,
      appNameEng: appNameEng,
      reMark: reMark
    });
  };

  deleteSoftwareAdda = index => e => {
    let a = this.state.datasoftwareadda;
    a.splice(index, 1);
    this.setState({
      datasoftwareadda: a
    });
  };

  render() {
    return (
      <div className="animated fadeIn">
        <Button
          color="success"
          style={{ marginBottom: "2%" }}
          onClick={() => this.setState({ modalsoftwareadda: true })}
        >
          เพิ่มซอฟต์แวร์แอ๊ดด้า
        </Button>
        <Card style={{ height: "400px" }}>
          <table width="100%" style={{ marginBottom: "2%" }}>
            <thead className="text-center table-active">
              <tr>
                <th id="tb" width="5%">
                  ลำดับที่
                </th>
                <th id="tb" width="10%">
                  AppID
                </th>
                <th id="tb" width="30%">
                  ชื่อโปรแกรม
                </th>
                <th id="tb" width="30%">
                  ชื่อภาษาอังกฤษ
                </th>
                <th id="tb" width="20%">
                  หมายเหตุ
                </th>
                <th id="tb" width="5%">
                  #
                </th>
              </tr>
            </thead>
            <tbody>
              {this.state.datasoftwareadda.map((item, index) => {
                return (
                  <tr key={index}>
                    <td id="tb" style={{ textAlign: "center" }}>
                      {index + 1}
                    </td>
                    <td id="tb" style={{ textAlign: "center" }}>
                      {item.appID}
                    </td>
                    <td id="tb">{item.appName}</td>
                    <td id="tb">{item.appNameEng}</td>
                    <td id="tb">{item.reMark}</td>
                    <td
                      id="tb"
                      style={{
                        textAlign: "center",
                        cursor: "pointer",
                        color: "red"
                      }}
                      onClick={this.deleteSoftwareAdda(index)}
                    >
                      เลือก
                    </td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </Card>
        <ModalSoftwareAdda
          Modal={this.state.modalsoftwareadda}
          set={this.modalSoftwareAdda}
          getSoftwareAdda={this.SoftwareAdda}
          dataexist={this.state.datasoftwareadda}
        />
      </div>
    );
  }
}

export default SoftwareAdda;
