import React, { useState } from "react";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Card,
  Button,
  CardTitle,
  CardText,
  Row,
  Col
} from "reactstrap";
import classnames from "classnames";
import Other from "./Tab/Other";
import Software from "./Tab/Software";
import SoftwareAdda from "./Tab/SoftwareAdda";
import Swal from "sweetalert2";
import Axios from "axios";
import { REQUEST_URL } from "../../config";
import Cookies from "js-cookie";
import moment from "moment";

class EditMainComputer extends React.Component {
  state = {
    activeTab: "1"
  };

  toggle = tab => e => {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  };

  dataOther = (
    value,
    date,
    tel,
    pfs_id,
    name,
    depcode,
    depname,
    barcode,
    nameos,
    productkey,
    bit,
    pcname,
    ippc,
    mainboard,
    cpu,
    speed,
    vga,
    harddisk,
    sizehd,
    monitor,
    ram,
    sizeram,
    ups,
    lan,
    printer,
    printersn,
    price,
    priceall,
    note,
    datacpu,
    datamb,
    dataram,
    datahdd,
    datamn,
    datavga,
    datalan,
    datapt,
    dataot,
    file,
    fileName,
    nameType,
    nameShow
  ) => {
    this.setState({
      value: value,
      date: date,
      tel: tel,
      pfs_id: pfs_id,
      name: name,
      depcode: depcode,
      depname: depname,
      barcode: barcode,
      nameos: nameos,
      productkey: productkey,
      bit: bit,
      pcname: pcname,
      ippc: ippc,
      mainboard: mainboard,
      cpu: cpu,
      speed: speed,
      vga: vga,
      harddisk: harddisk,
      sizehd: sizehd,
      monitor: monitor,
      ram: ram,
      sizeram: sizeram,
      ups: ups,
      lan: lan,
      printer: printer,
      printersn: printersn,
      price: price,
      priceall: priceall,
      note: note,
      datacpu: datacpu,
      datamb: datamb,
      dataram: dataram,
      datahdd: datahdd,
      datamn: datamn,
      datavga: datavga,
      datalan: datalan,
      datapt: datapt,
      dataot: dataot,
      file: file,
      fileName: fileName,
      nameType: nameType,
      nameShow: nameShow
    });
  };

  dataSoftware = data => {
    this.setState({
      datasoftware: data
    });
  };

  dataSoftwareAdda = data => {
    this.setState({
      datasoftwareadda: data
    });
  };

  sendData = e => {
    e.preventDefault();
    Axios.post(
      REQUEST_URL + "/computer/editmain",
      {
        id: this.props.match.params.id,
        value: this.state.value,
        date: moment(this.state.date).format("YYYY-MM-DD HH:mm:ss"),
        tel: this.state.tel,
        pfs_id: this.state.pfs_id,
        name: this.state.name,
        depcode: this.state.depcode,
        depname: this.state.depname,
        barcode: this.state.barcode,
        nameos: this.state.nameos,
        productkey: this.state.productkey,
        bit: this.state.bit,
        pcname: this.state.pcname,
        ippc: this.state.ippc,
        mainboard: this.state.mainboard,
        cpu: this.state.cpu,
        speed: this.state.speed,
        vga: this.state.vga,
        harddisk: this.state.harddisk,
        sizehd: this.state.sizehd,
        monitor: this.state.monitor,
        ram: this.state.ram,
        sizeram: this.state.sizeram,
        ups: this.state.ups,
        lan: this.state.lan,
        printer: this.state.printer,
        printersn: this.state.printersn,
        price: this.state.price,
        priceall: this.state.priceall,
        note: this.state.note,
        datacpu: this.state.datacpu,
        datamb: this.state.datamb,
        dataram: this.state.dataram,
        datahdd: this.state.datahdd,
        datamn: this.state.datamn,
        datavga: this.state.datavga,
        datalan: this.state.datalan,
        datapt: this.state.datapt,
        dataot: this.state.dataot,
        datasoftware: this.state.datasoftware,
        datasoftwareadda: this.state.datasoftwareadda,
        lastby: sessionStorage.getItem("user_id")
      },
      {
        headers: { authorization: Cookies.get("webcomputer") }
      }
    ).then(response => {
      if (response.data.status) {
        Swal.fire({
          type: "success",
          title: "เพิ่มข้อมูลสำเร็จ..."
        });
      }
    });
  };

  render() {
    return (
      <div>
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === "1" })}
              onClick={this.toggle("1")}
            >
              ข้อมูลทั่วไป
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === "2" })}
              onClick={this.toggle("2")}
            >
              ซอฟต์แวร์
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === "3" })}
              onClick={this.toggle("3")}
            >
              ซอฟต์แวร์แอ๊ดด้า
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === "4" })}
              onClick={this.toggle("4")}
            >
              ประวัติการซ่อม
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === "5" })}
              onClick={this.toggle("5")}
            >
              ข้อมูลจากเครื่อง
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === "6" })}
              onClick={this.toggle("6")}
            >
              การครอบครอง
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === "7" })}
              onClick={this.toggle("7")}
            >
              ประวัติการโอนย้าย
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <Other ID={this.props.match.params.id} getOther={this.dataOther} />
            <div
              style={{
                display: "flex",
                justifyContent: "flex-end",
                marginTop: "3%"
              }}
            >
              <Button
                color="success"
                onClick={this.sendData}
                style={{ marginRight: "1%" }}
              >
                บันทึกข้อมูล
              </Button>
              <Button color="danger">ยกเลิกการบันทึก</Button>
            </div>
          </TabPane>
          <TabPane tabId="2">
            <Software
              ID={this.props.match.params.id}
              getSoftware={this.dataSoftware}
            />
          </TabPane>
          <TabPane tabId="3">
            <SoftwareAdda
              ID={this.props.match.params.id}
              getSoftwareAdda={this.dataSoftwareAdda}
            />
          </TabPane>
          <TabPane tabId="4"></TabPane>
          <TabPane tabId="5"></TabPane>
          <TabPane tabId="6"></TabPane>
          <TabPane tabId="7"></TabPane>
        </TabContent>
      </div>
    );
  }
}

export default EditMainComputer;
