import React from "react";
import {
  Button,
  Card,
  CardBody,
  CardGroup,
  Col,
  Form,
  Input,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Row,
  Alert
} from "reactstrap";
import axios from "axios";
import { REQUEST_URL } from "../../config";
import Cookie from "js-cookie";
import "../style.css";

class Login extends React.Component {
  state = {
    userID: "",
    pass: "",
    status: ""
  };

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value,
      status: ""
    });
  };

  onSubmit = e => {
    e.preventDefault();
    axios
      .post(REQUEST_URL + "/login", {
        userID: this.state.userID.toUpperCase(),
        password: this.state.pass.toUpperCase()
      })
      .then(response => {
        if (response.data.status) {
          Cookie.set(
            "webcomputer",
            response.data.user[0].userID + ":" + response.data.token
          );
          sessionStorage.setItem(
            "user_id",
            response.data.user[0].userID.toUpperCase()
          );
          sessionStorage.setItem("status", response.data.user[0].userLevel);
          // sessionStorage.setItem("level", response.data.user[0].level);
          this.props.history.push("/");
        } else {
          this.setState({ status: response.data.status });
          this.props.history.push("/login");
        }
      });
  };

  render() {
    return (
      <div style={{ width: "100%", height: "100%" }}>
        <div className="loginbackground" style={{ height: "100vh" }}>
          <Row style={{ margin: "0px" }}>
            <Col md="12"></Col>
          </Row>
          <Row
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              margin: "0px",
              height: "100vh"
            }}
          >
            <Col md="4">
              <CardGroup>
                <Card className="p-4" style={{ backgroundColor: "#e4e5e6" }}>
                  <CardBody>
                    <Form onSubmit={e => this.onSubmit(e)}>
                      <h1>เข้าสู่ระบบ</h1>
                      <p className="text-muted">Web Computer</p>

                      {this.state.status === false ? (
                        <Alert
                          className="alert alert-danger fade show"
                          variant="danger"
                        >
                          Username หรือ Password ไม่ถูกต้อง
                        </Alert>
                      ) : (
                        ""
                      )}

                      <InputGroup className="mb-3">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-user" />
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="text"
                          name="userID"
                          placeholder="Username"
                          autoComplete="username"
                          autoFocus
                          required
                          onChange={e => this.handleChange(e)}
                        />
                      </InputGroup>
                      <InputGroup className="mb-4">
                        <InputGroupAddon addonType="prepend">
                          <InputGroupText>
                            <i className="icon-lock"></i>
                          </InputGroupText>
                        </InputGroupAddon>
                        <Input
                          type="password"
                          name="pass"
                          placeholder="Password"
                          autoComplete="current-password"
                          required
                          onChange={e => this.handleChange(e)}
                        />
                      </InputGroup>
                      <Row>
                        <Col md="6" xs="6">
                          <Button color="primary" className="px-4">
                          เข้าสู่ระบบ
                          </Button>
                        </Col>
                        <Col
                          md="6"
                          xs="6"
                          style={{
                            display: "flex",
                            justifyContent: "flex-end"
                          }}
                        >
                          <p
                            onClick={() => this.props.history.push("/register")}
                            style={{
                              fontSize: "18px",
                              color: "black",
                              textDecoration: "underline",
                              cursor: "pointer"
                            }}
                          >
                            สมัครสมาชิก
                          </p>
                        </Col>
                      </Row>
                    </Form>
                  </CardBody>
                </Card>
              </CardGroup>
            </Col>
          </Row>
          <Row style={{ margin: "0px" }}>
            <Col md="12"></Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default Login;
