import React from "react";

// const User = React.lazy(() => import("./views/User/User"));
// const Database = React.lazy(() => import("./views/Data/Database"));
// const Table = React.lazy(() => import("./views/Data/Table"));
// const Edit = React.lazy(() => import("./views/Data/Edit"));
// const View = React.lazy(() => import("./views/Data/View"));
// const CreateDictionary = React.lazy(() =>
//   import("./views/Data/CreateDictionary")
// );
// const DataDictionary = React.lazy(() => import("./views/Data/DataDictionary"));

import files from "./views/Files/Files";
import filesAe from "./views/Files/AeFiles";
import users from "./views/Users/Users";
import usersAdd from "./views/Users/AddUsers";
import usersEdit from "./views/Users/EditUsers";
import MainComputer from "./views/Computer/Maincomputer";
import AddMainComputer from "./views/Computer/Main_add";
import EditMainComputer from "./views/Computer/Main_edit";
import ViewMainComputer from "./views/Computer/Main_view";
<<<<<<< HEAD
import MainSoftware from "./views/Software/MainSoftware";
import AddMainSoftware from "./views/Software/Main_add";
import EditMainSoftware from "./views/Software/Main_edit";
import ViewMainSoftware from "./views/Software/Main_view";
import MainApplication from "./views/Application/MainApplication";
import AddMainApplication from "./views/Application/Main_add";
import EditMainApplication from "./views/Application/Main_edit";
import ViewMainApplication from "./views/Application/Main_view";
import MainType from "./views/Type/MainType";
import MainBrand from "./views/Brand/MainBrand";
import MainSeller from "./views/Seller/MainSeller";
import MainGroupSupplies from "./views/GroupSupplies/MainGroupSupplies";
// import MainSupplies from "./views/Supplies/MainSupplies"
import MainSuppliesMRP from "./views/SuppliesMRP/MainSuppliesMRP";
=======
import CCTV from './views/Camera/Cameracctv'
import Camera from './views/Camera/Cameramain'
import Maindash from './views/Main/Main'
>>>>>>> 5ef5cfea0adbe9a438d3d6a5ddd128544d23f93f

const routes = [
  { path: "/", exact: true, name: "หน้าหลัก" , component: Maindash },
  { path: "/files", exact: true, name: "ข้อมูลระบบงาน", component: files },
  {
    path: "/files/new",
    exact: true,
    name: "เพิ่มข้อมูลระบบงาน",
    component: filesAe
  },
  {
    path: "/files/:id",
    exact: true,
    name: "แก้ไขข้อมูลระบบงาน",
    component: filesAe
  },
  { path: "/users", exact: true, name: "ข้อมูลผู้ใช้งาน", component: users },
  {
    path: "/users/new",
    exact: true,
    name: "เพิ่มผู้ใช้งาน",
    component: usersAdd
  },
  {
    path: "/users/:id",
    exact: true,
    name: "แก้ไขผู้ใช้งาน",
    component: usersEdit
  },
  {
    path: "/main/computer",
    exact: true,
    name: "ข้อมูลทะเบียนคอมพิวเตอร์",
    component: MainComputer
  },
  {
    path: "/main/computer/new",
    exact: true,
    name: "เพิ่มทะเบียนคอมพิวเตอร์",
    component: AddMainComputer
  },
  {
    path: "/main/computer/edit/:id",
    exact: true,
    name: "แก้ไขทะเบียนคอมพิวเตอร์",
    component: EditMainComputer
  },
  {
    path: "/main/computer/view/:id",
    exact: true,
    name: "มุมมองทะเบียนคอมพิวเตอร์",
    component: ViewMainComputer
  },
  {
<<<<<<< HEAD
    path: "/main/software",
    exact: true,
    name: "ข้อมูลทะเบียนซอฟต์แวร์",
    component: MainSoftware
  },
  {
    path: "/main/software/new",
    exact: true,
    name: "เพิ่มทะเบียนซอฟต์แวร์",
    component: AddMainSoftware
  },
  {
    path: "/main/software/edit/:id",
    exact: true,
    name: "แก้ไขทะเบียนซอฟต์แวร์",
    component: EditMainSoftware
  },
  {
    path: "/main/software/view/:id",
    exact: true,
    name: "มุมมองทะเบียนซอฟต์แวร์",
    component: ViewMainSoftware
  },
  {
    path: "/main/application",
    exact: true,
    name: "ข้อมูลทะเบียนแอพพลิเคชั่น",
    component: MainApplication
  },
  {
    path: "/main/application/new",
    exact: true,
    name: "เพิ่มทะเบียนแอพพลิเคชั่น",
    component: AddMainApplication
  },
  {
    path: "/main/application/edit/:id",
    exact: true,
    name: "แก้ไขทะเบียนแอพพลิเคชั่น",
    component: EditMainApplication
  },
  {
    path: "/main/application/view/:id",
    exact: true,
    name: "มุมมองทะเบียนแอพพลิเคชั่น",
    component: ViewMainApplication
  },
  {
    path: "/main/type",
    exact: true,
    name: "ข้อมูลประเภท",
    component: MainType
  },
  {
    path: "/main/brand",
    exact: true,
    name: "ข้อมูลแบรนด์",
    component: MainBrand
  },
  {
    path: "/main/seller",
    exact: true,
    name: "ข้อมูลผู้ขาย",
    component: MainSeller
  },
  {
    path: "/sup/groupsupplies",
    exact: true,
    name: "กลุ่มวัสดุสิ้นเปลือง",
    component: MainGroupSupplies
  },
  // {
  //   path: "/sup/supplies",
  //   exact: true,
  //   name: "วัสดุสิ้นเปลือง",
  //   component: MainSupplies
  // },
  {
    path: "/sup/suppliesmrp",
    exact: true,
    name: "วัสดุสิ้นเปลืองMRP",
    component: MainSuppliesMRP
  }
=======
    path: '/CCTV',
    exact: true,
    name: "กล้อง CCTV",
    component: CCTV
  },
  {
    path: '/Camera',
    exact: true,
    name: "กล้องวงจรปิด",
    component: Camera
  },
>>>>>>> 5ef5cfea0adbe9a438d3d6a5ddd128544d23f93f
  // {
  //   path: "/data/database",
  //   exact: true,
  //   name: "Database",
  //   component: Database
  // },
  // {
  //   path: "/data/database/table/:database/:id",
  //   exact: true,
  //   name: "Table",
  //   component: Table
  // },
  // {
  //   path: "/data/database/table/:database/:id/edit/:database/:table",
  //   exact: true,
  //   name: "Edit",
  //   component: Edit
  // },
  // {
  //   path: "/data/database/table/:database/:id/view/:database/:table",
  //   exact: true,
  //   name: "View",
  //   component: View
  // },
  // {
  //   path: "/data/database/table/:database/:id/createdictionary/:database/:id",
  //   exact: true,
  //   name: "CreateDictionary",
  //   component: CreateDictionary
  // },
  // {
  //   path: "/data/datadictionary",
  //   name: "DataDictionary",
  //   component: DataDictionary
  // }
];

export default routes;
